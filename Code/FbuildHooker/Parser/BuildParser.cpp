//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: BuildParser.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// BuildParser.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Parser.h"
#include "../Hooker/FBuildHooker.h"

// Constructor
//------------------------------------------------------------------------------
/*explicit*/ BuildParser::BuildParser() :
buildType(-1),
usePch(false),
genPch(false),
usePdbType(0)
{
}

/*explicit*/ BuildParser::BuildParser(const char *string) :
Parser(string),
buildType(-1),
usePch(false),
genPch(false),
usePdbType(0)
{
}

/*explicit*/ BuildParser::BuildParser(const std::string &string) :
Parser(string),
usePch(false),
genPch(false),
usePdbType(0)
{
};

// DeConstructor
//------------------------------------------------------------------------------
BuildParser::~BuildParser()
{
}

// GetToolChainType
//------------------------------------------------------------------------------
void BuildParser::InitToolChainType(const std::vector<std::string> &tokens, int &startIdx)
{
	DEBUG("Init toolchain type: %d\n", startIdx);

	std::string cmdName;
	cmd = tokens[startIdx];
	StrHelper::StrTrim(cmd, '"');
	FileDirHelper::FixupFilePath(cmd);
	FileDirHelper::GetFileNameFromPath(cmdName, cmd, false);
	if (StrHelper::StrCompareI(cmdName, "cl"))
	{
		toolchainType = ToolChain::TOOLCHAIN_TYPE::MSVC;
		buildType = ToolChain::BUILD_TYPE::COMPILE;
	}
	else if (StrHelper::StrCompareI(cmdName, "lib"))
	{
		toolchainType = ToolChain::TOOLCHAIN_TYPE::MSVC;
		buildType = ToolChain::BUILD_TYPE::STATIC_LIBRARY;
	}
	else if (StrHelper::StrCompareI(cmdName, "link"))
	{
		toolchainType = ToolChain::TOOLCHAIN_TYPE::MSVC;
		buildType = ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY;
	}
	else if (StrHelper::StrCompareI(cmdName, "gcc") ||
			StrHelper::StrCompareI(cmdName, "arm-linux-androideabi-gcc"))
	{
		// Compile c
		toolchainType = ToolChain::TOOLCHAIN_TYPE::GNU;
		buildType = ToolChain::BUILD_TYPE::COMPILE;
	}
	else if (StrHelper::StrCompareI(cmdName, "g++") ||
			StrHelper::StrCompareI(cmdName, "arm-linux-androideabi-g++"))
	{
		// Compile c++ and link
		toolchainType = ToolChain::TOOLCHAIN_TYPE::GNU;
	}
	else if (StrHelper::StrCompareI(cmdName, "ar") ||
			StrHelper::StrCompareI(cmdName, "arm-linux-androideabi-ar"))
	{
		toolchainType = ToolChain::TOOLCHAIN_TYPE::GNU;
		buildType = ToolChain::BUILD_TYPE::STATIC_LIBRARY;
	}
	else
	{
		FATALERROR("Not support toolset: %s - %s\n", tokens[startIdx].c_str(), cmd.c_str());
	}
	startIdx++;

	DEBUG("Toolchain type: %d\n", toolchainType);
	DEBUG("Build type: %d\n", buildType);
}

// ParserCustom
//------------------------------------------------------------------------------
void BuildParser::ParserCustom(int &startIdx)
{
	if (StrHelper::StrBeginWithI(tokens[startIdx], "/CURDIR:"))
	{
		// add custom dir variable		
		currentDir = tokens[startIdx].substr(8);
		StrHelper::StrTrim(currentDir, '"');
		DEBUG("Add custom dir: %s\n", currentDir.c_str());
		startIdx++;
	}
	else
	{
		INFO("Can't parse current directory!\n");
	}

	if (StrHelper::StrBeginWithI(tokens[startIdx], "/ENVLIBPATH:"))
	{
		std::string libPath = tokens[startIdx].substr(12);
		StrHelper::StrTrim(libPath, '"');

		DEBUG("EnvParser parse libpath %s\n", libPath.c_str());
		Parser parsePaths(libPath);
		parsePaths.Tokenize(';');
		envLibPaths = parsePaths.GetTokens();
		for (size_t i = 0; i < envLibPaths.size(); ++i)
		{
			FileDirHelper::TransToFullPath(envLibPaths[i], currentDir);
		}
		startIdx++;
	}
	else
	{
		INFO("Can't parse env lib path!\n");
	}

	if (StrHelper::StrBeginWithI(tokens[startIdx], "/ENV:"))
	{
		std::string envStr = tokens[startIdx].substr(5);
		StrHelper::StrTrim(envStr, '"');

		DEBUG("EnvParser parse envStr %s\n", envStr.c_str());
		StrHelper::StrSplit(envStr, '|', envs);
		startIdx++;
	}
	else
	{
		INFO("No env!\n");
	}
}

// ParserContent
//------------------------------------------------------------------------------
/*virtual*/ void BuildParser::ParserContent()
{
	DEBUG("BuildParse ParserContent token size %d\n", tokens.size());
	if (tokens.size() <= 0)
	{
		return;
	}

	int startIdx = 0;
	ParserCustom(startIdx);
	InitToolChainType(tokens, startIdx);

	if (toolchainType == ToolChain::TOOLCHAIN_TYPE::MSVC)
	{
		ParseMSVCArguments(startIdx);
	}
	else if (toolchainType == ToolChain::TOOLCHAIN_TYPE::GNU)
	{
		ParseGNUArguments(startIdx);
	}
}

// ParseMSVCArguments
//------------------------------------------------------------------------------
void BuildParser::ParseMSVCArguments(int &startIdx)
{
	DEBUG("Parse MSVC arguments: %d\n", startIdx);

	size_t i = startIdx;
	for (; i < tokens.size(); ++i)
	{
		if (StrHelper::StrBeginWithI(tokens[i], "/DEF:"))
		{
			std::string defFile = tokens[i].substr(5);
			StrHelper::StrTrim(defFile, '"');
			FileDirHelper::TransToFullPath(defFile, currentDir);
			buildOpts.push_back("/DEF:\"" + defFile + "\"");
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/D"))
		{
			if (tokens[i] == "/D")
			{
				preDefines.push_back(tokens[i] + tokens[i + 1]);
				buildOpts.push_back(tokens[i] + tokens[i + 1]);
				i++;
			}
			else
			{
				preDefines.push_back(tokens[i]);
				buildOpts.push_back(tokens[i]);
			}
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/I") && buildType == ToolChain::BUILD_TYPE::COMPILE)
		{
			std::string include = tokens[i].substr(2);
			StrHelper::StrTrim(include, '"');
			FileDirHelper::TransToFullPath(include, currentDir);
			buildOpts.push_back("/I\"" + include + "\"");
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/Z"))
		{
			if (StrHelper::StrBeginWithI(tokens[i], "/Z7"))
			{
				usePdbType = 2;
			}
			else
			{
				usePdbType = 1;
			}
			buildOpts.push_back(tokens[i]);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/Fd"))
		{
			pdbFile = tokens[i].substr(3);
			StrHelper::StrTrim(pdbFile, '"');
			FileDirHelper::TransToFullPath(pdbFile, currentDir);
			buildOpts.push_back("/Fd\"" + pdbFile + "\"");
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/Fo"))
		{
			std::string output = "";
			output = tokens[i].substr(3);
			StrHelper::StrTrim(output, '"');
			FileDirHelper::TransToFullPath(output, currentDir);
			if (StrHelper::StrEndWithI(output, ".obj"))
			{
				outputFile = output;
				FileDirHelper::GetDirFromPath(outputPath, outputFile);
			}
			else
			{
				outputPath = output;
			}
			DEBUG("Set output path: %s\n", outputPath.c_str());
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/OUT:"))
		{
			outputFile = tokens[i].substr(5);
			StrHelper::StrTrim(outputFile, '"');
			FileDirHelper::TransToFullPath(outputFile, currentDir);
			FileDirHelper::GetDirFromPath(outputPath, outputFile);
			DEBUG("Parse output file: %s\n", outputFile.c_str());
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/LIBPATH:"))
		{
			std::string libPath = tokens[i].substr(9);
			StrHelper::StrTrim(libPath, '"');
			FileDirHelper::TransToFullPath(libPath, currentDir);
			buildOpts.push_back("/LIBPATH:\"" + libPath + "\"");
			DEBUG("Build parser lib path: %s\n", libPath.c_str());
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/manifestinput:"))
		{
			std::string manifestInput = tokens[i].substr(15);
			StrHelper::StrTrim(manifestInput, '"');
			FileDirHelper::TransToFullPath(manifestInput, currentDir);
			buildOpts.push_back("/manifestinput:\"" + manifestInput + "\"");
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/manifestfile:"))
		{
			DEBUG("manifest file: %s\n", tokens[i].c_str());
			std::string manifestFile = tokens[i].substr(14);
			StrHelper::StrTrim(manifestFile, '"');
			FileDirHelper::TransToFullPath(manifestFile, currentDir);
			buildOpts.push_back("/manifestfile:\"" + manifestFile + "\"");
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/"))
		{
			if (StrHelper::StrBeginWithI(tokens[i], "/Yc"))
			{
				pchHeaderFile = tokens[i].substr(3);
				StrHelper::StrTrim(pchHeaderFile, '"');
				genPch = true;
			}
			else if (StrHelper::StrBeginWithI(tokens[i], "/Yu"))
			{
				pchInput = tokens[i].substr(3);
				StrHelper::StrTrim(pchInput, '"');
				usePch = true;
				buildOpts.push_back(tokens[i]);
			}
			else if (StrHelper::StrBeginWithI(tokens[i], "/Fp"))
			{
				pchOutput = tokens[i].substr(3);
				StrHelper::StrTrim(pchOutput, '"');
			}
			else if (StrHelper::StrBeginWithI(tokens[i], "/FI") || StrHelper::StrBeginWithI(tokens[i], "/Fm"))
			{
				// Other file path to transform
				buildOpts.push_back(tokens[i]);
			}
			else
			{
				buildOpts.push_back(tokens[i]);
			}
		}
		else if (StrHelper::StrEndWithI(tokens[i], ".cpp") || StrHelper::StrEndWithI(tokens[i], ".c") ||
			StrHelper::StrEndWithI(tokens[i], ".cc") || StrHelper::StrEndWithI(tokens[i], ".hpp") ||
			StrHelper::StrEndWithI(tokens[i], ".obj") || StrHelper::StrEndWithI(tokens[i], ".o"))
		{
			std::string srcPath = tokens[i];
			FileDirHelper::TransToFullPath(srcPath, currentDir);
			sourceFiles.insert(std::make_pair(srcPath, false));
		}
		else if (StrHelper::StrEndWithI(tokens[i], ".lib"))
		{
			std::string path = tokens[i];
			FileDirHelper::TransToFullPath(path, currentDir);
			if (FileDirHelper::FileExists(path))
			{
				dependency.push_back(path);
			}
			else
			{
				dependency.push_back(tokens[i]);
			}
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "\"") && StrHelper::StrEndWithI(tokens[i], "\""))
		{
			tokens[i] = tokens[i].substr(1, tokens[i].size() - 2);
			i--;
		}
		else
		{
			// Treat .res or other file depends as additionalInputs
			std::string maybe_path = tokens[i];
			StrHelper::StrTrim(maybe_path, '"');
			FileDirHelper::TransToFullPath(maybe_path, currentDir);

			if (FileDirHelper::FileExists(maybe_path))
			{
				FileDirHelper::TransToFullPath(maybe_path, currentDir);
				additionalInputs.push_back(maybe_path);
				INFO("Parser exist file: %s --> %s\n", tokens[i].c_str(), maybe_path.c_str());
			}
			else
			{
				WARNING("Other token %s\n", tokens[i].c_str());
				otherTokens.push_back(tokens[i]);
			}
		}
	}
	startIdx = i;
	DEBUG("Parse MSVC arguments finished: %d\n", startIdx);

	if (pchOutput != "")
	{
		FileDirHelper::TransToFullPath(pchOutput, currentDir);
		if (pchHeaderFile == "")
		{
			buildOpts.push_back("/Fp\"" + pchOutput + '"');
		}
	}
}

// ParseGCCArguments
//------------------------------------------------------------------------------
void BuildParser::ParseGNUArguments(int &startIdx)
{
	DEBUG("Parse GNU arguments: %d, build type: %d\n", startIdx, buildType);

	size_t i = startIdx;
	for (; i < tokens.size(); ++i)
	{
		DEBUG("Parse token: %s\n", tokens[i].c_str());
		if (StrHelper::StrCompareI(tokens[i], "-c") ||
			StrHelper::StrCompareI(tokens[i], "-E") || 
			StrHelper::StrCompareI(tokens[i], "-S"))
		{
			buildType = ToolChain::BUILD_TYPE::COMPILE;
			buildOpts.push_back(tokens[i]);
		}
		else if (StrHelper::StrCompareI(tokens[i], "-o"))
		{
			if (i >= tokens.size())
			{
				FATALERROR("Gnu toolchain %s specify -o but no output files found!\n", cmd.c_str());
				continue;
			}
			outputFile = tokens[++i];
			StrHelper::StrTrim(outputFile, '"');
			FileDirHelper::TransToFullPath(outputFile, currentDir);
			FileDirHelper::GetDirFromPath(outputPath, outputFile);
			INFO("Parse GNU output file: %s\n", outputFile.c_str());
			DEBUG("Parse GNU output path: %s\n", outputPath.c_str());
		}
		else if (StrHelper::StrEndWithI(tokens[i], ".cpp") || StrHelper::StrEndWithI(tokens[i], ".c") ||
			StrHelper::StrEndWithI(tokens[i], ".cxx") || StrHelper::StrEndWithI(tokens[i], ".cp") ||
			StrHelper::StrEndWithI(tokens[i], ".cc") || StrHelper::StrEndWithI(tokens[i], ".c++") ||
			StrHelper::StrEndWithI(tokens[i], ".o") || StrHelper::StrEndWithI(tokens[i], ".obj"))
		{
			std::string srcPath = tokens[i];
			FileDirHelper::TransToFullPath(srcPath, currentDir);
			sourceFiles.insert(std::make_pair(srcPath, false));
			INFO("Parse GNU source file: %s\n", srcPath.c_str());
		}
		else if (StrHelper::StrEndWithI(tokens[i], ".a"))
		{
			if (buildType == ToolChain::BUILD_TYPE::STATIC_LIBRARY)
			{
				outputFile = tokens[i];
				StrHelper::StrTrim(outputFile, '"');
				FileDirHelper::TransToFullPath(outputFile, currentDir);
				FileDirHelper::GetDirFromPath(outputPath, outputFile);
				DEBUG("Parse output file: %s\n", outputFile.c_str());
				DEBUG("Parse output dir: %s\n", outputPath.c_str());
			}
			else
			{
				DEBUG("Parse dependency: %s\n", tokens[i].c_str());
				std::string dependPath = tokens[i];
				StrHelper::StrTrim(dependPath, '"');
				FileDirHelper::TransToFullPath(dependPath, currentDir);
				dependency.push_back(dependPath);
			}
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "-MF"))
		{
			// TODO check no-space arg
			if (i >= tokens.size())
			{
				FATALERROR("Gnu toolchain %s specify -MF but no depend files found!\n", cmd.c_str());
				continue;
			}
			pdbFile = tokens[++i];
			StrHelper::StrTrim(pdbFile, '"');
			FileDirHelper::TransToFullPath(pdbFile, currentDir);
			FileDirHelper::GetDirFromPath(outputPath, pdbFile);

			INFO("Parse GNU depend file: %s\n", pdbFile.c_str());
			buildOpts.push_back("-MF\"" + pdbFile + "\"");
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "\"") && StrHelper::StrEndWithI(tokens[i], "\""))
		{
			tokens[i] = tokens[i].substr(1, tokens[i].size() - 2);
			i--;
		}
		else
		{
			DEBUG("Parse other build options: %s\n", tokens[i].c_str());
			buildOpts.push_back(tokens[i]);
		}
	}

	if (buildType == -1)
	{
		DEBUG("Parse g++ parameter and specify a dynamic library\n");
		buildType = ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY;
	}

	if (buildType == ToolChain::BUILD_TYPE::COMPILE)
	{
		// Add remote compiler search path
		buildOpts.push_back("-B .");
	}
}

// Serialize
//------------------------------------------------------------------------------
/*virtual*/ void BuildParser::Serialize()
{
	std::string libPath = Env::GetEnvVariable("LIB");

	std::string prefix = "";
	prefix += "/CURDIR:\"" + FileDirHelper::GetCurrentDir() + "\" ";
	if (!libPath.empty())
	{
		prefix += "/ENVLIBPATH:\"";
		prefix += libPath;
		prefix += "\" ";
	}

	if (envs.size() > 0)
	{
		std::string envStr = "";
		StrHelper::StrJoin(envStr, envs, "|");
		prefix += "/ENV:\"";
		prefix += envStr;
		prefix += "\" ";
		INFO("Serialize env: %s\n", envStr.c_str());
	}
	parserStr.insert(0, prefix);
	DEBUG("Parser serialize: %s\n", parserStr.c_str());
}

// DeSerialize
//------------------------------------------------------------------------------
/*virtual*/ void BuildParser::DeSerialize(const char *buffer)
{
	DEBUG("BuildParser deserialize %s\n", buffer);
	parserStr = std::string(buffer);
	Tokenize();
	ParserContent();
}

// FindSourceFile
//------------------------------------------------------------------------------
SRC_FILE_ITER BuildParser::FindSourceFile(const std::string & source)
{
	SRC_FILE_ITER iter = sourceFiles.begin();
	size_t srcStartPos = source.find_last_of(ALL_SLASH_STR);
	size_t srcEndPos = source.find_last_of('.');

	if (source.compare(0, srcStartPos, outputPath, 0, srcStartPos) != 0)
	{
		return sourceFiles.end();
	}

	srcStartPos = srcStartPos == std::string::npos ? 0 : srcStartPos + 1;

	for (; iter != sourceFiles.end(); ++iter)
	{
		DEBUG("Search source file %s\n", (*iter).first.c_str());
		size_t desStartPos = (*iter).first.find_last_of(ALL_SLASH_STR);
		size_t desEndPos = (*iter).first.find_last_of('.');

		desStartPos = desStartPos == std::string::npos ? 0 : desStartPos + 1;

		if (0 == source.compare(srcStartPos, srcEndPos - srcStartPos, \
			(*iter).first, desStartPos, desEndPos - desStartPos))
		{
			DEBUG("Match source file %s\n", (*iter).first.c_str());
			return iter;
		}
	}
	return iter;
}

// FindOutputFile
//------------------------------------------------------------------------------
bool BuildParser::MatchOutputFile(const std::string & source) const
{
	//DEBUG("Test match output file: %s\n", outputFile.c_str());
	//return 0 == source.compare(0, source.size() - 4, outputFile);
	return source == outputFile;
}

// CheckIfScanFinished
//------------------------------------------------------------------------------
bool BuildParser::CheckIfScanFinished() const
{
	SRC_FILE_CONST_ITER iter = sourceFiles.begin();

	for (; iter != sourceFiles.end(); ++iter)
	{
		if (!(*iter).second)
		{
			return false;
		}
	}
	return true;
}

// DumpScanInfo
//------------------------------------------------------------------------------
void BuildParser::GetEnvByName(const std::string & name, std::string & value) const
{
	for (size_t i = 0; i < envs.size(); ++i)
	{
		if (StrHelper::StrBeginWithI(envs[i], name.c_str()))
		{
			value = envs[i].substr(name.size() + 1);
		}
	}
}

// DumpScanInfo
//------------------------------------------------------------------------------
void BuildParser::DumpScanInfo()
{
	INFO("Dump Scan Info:\n");

	SRC_FILE_ITER iter = sourceFiles.begin();
	for (; iter != sourceFiles.end(); ++iter)
	{
		INFO("file %s state %d\n", (*iter).first.c_str(), (*iter).second);
	}
}

// DumpInfo
//------------------------------------------------------------------------------
/*virtual*/ void BuildParser::DumpInfo()
{
	INFO("[Dump Compiler Info] %s\n", cmd.c_str());
	INFO("[buildOpts]\n");
	for (size_t i = 0; i < buildOpts.size(); ++i)
	{
		INFO("%s\n", buildOpts[i].c_str());
	}
	INFO("[preDefines]:\n");
	for (size_t i = 0; i < preDefines.size(); ++i)
	{
		INFO("%s\n", preDefines[i].c_str());
	}
	INFO("[includeDirs]:\n");
	for (size_t i = 0; i < includeDirs.size(); ++i)
	{
		INFO("%s\n", includeDirs[i].c_str());
	}
	INFO("[sourceFiles]:\n");
	SRC_FILE_ITER iter = sourceFiles.begin();
	for (; iter != sourceFiles.end(); ++iter)
	{
		INFO("%s\n", (*iter).first.c_str());
	}
	INFO("[outputPath]:\n%s\n", outputPath.c_str());
	INFO("[outputFile]:\n%s\n", outputFile.c_str());
}
