//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Parser.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Parser.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Parser.h"

// CreateParser
//------------------------------------------------------------------------------
/*static*/ Parser* Parser::createParser(const std::string &cmd, COMMAND_TYPE cmdType)
{
	switch (cmdType)
	{
	case BUILD_TOOLCHAIN:
		return new BuildParser(cmd);
	case FB_SERVICE_START:
		return new EnvParser(cmd);
	default:
		return nullptr;
	}
}

// Tokenize
//------------------------------------------------------------------------------
void Parser::Tokenize(char splitChar)
{
	int totalSize = parserStr.size();
	int curPos = 0;
	int curTokenStart = 0;
	int curTokenEnd = 0;
	bool lookingForStart = true;
	bool hasQuotation = false;

	// TODO not distinguish ' and "
	for (int idx = 0; idx < totalSize; ++idx)
	{
		if (lookingForStart)
		{
			if (parserStr[idx] == splitChar || parserStr[idx] == '\n' || parserStr[idx] == '\r')
			{
				curTokenStart++;
			}
			else
			{
				if (parserStr[idx] == '"' || parserStr[idx] == '\'')
				{
					hasQuotation = true;
				}
				curTokenStart = idx;
				lookingForStart = false;
			}
		}
		else
		{
			if (parserStr[idx] == splitChar || parserStr[idx] == '\n' || parserStr[idx] == '\r')
			{
				if (hasQuotation)
				{
					continue;
				}
				hasQuotation = false;
				lookingForStart = true;
				curTokenEnd = idx;
				tokens.push_back(std::string(parserStr, curTokenStart, curTokenEnd - curTokenStart));
			}
			else if (parserStr[idx] == '"' || parserStr[idx] == '\'')
			{
				hasQuotation = !hasQuotation;
			}
		}
	}

	// check the last one token if has
	if (!lookingForStart)
	{
		tokens.push_back(std::string(parserStr, curTokenStart, parserStr.size() - curTokenStart));
	}
}

// Tokenize with wrapper
//------------------------------------------------------------------------------
void Parser::Tokenize(char splitChar, char lwrapper, char rwrapper)
{
	int totalSize = parserStr.size();
	int curPos = 0;
	int curTokenStart = 0;
	int curTokenEnd = 0;
	bool lookingForStart = true;
	bool wrapperStart = false;
	bool hasQuotation = false;

	// TODO not distinguish ' and "
	for (int idx = 0; idx < totalSize; ++idx)
	{
		if (wrapperStart)
		{
			if (parserStr[idx] == rwrapper)
			{
				wrapperStart = false;
				curTokenEnd = idx;
				tokens.push_back(std::string(parserStr, curTokenStart, curTokenEnd - curTokenStart));
			}
			continue;
		}
		else
		{
			if (parserStr[idx] == lwrapper)
			{
				lookingForStart = true;
				hasQuotation = false;
				wrapperStart = true;
				curTokenStart = idx + 1;
				continue;
			}
		}

		if (lookingForStart)
		{
			if (parserStr[idx] == splitChar || parserStr[idx] == '\n' || parserStr[idx] == '\r')
			{
				curTokenStart++;
			}
			else
			{
				if (parserStr[idx] == '"' || parserStr[idx] == '\'')
				{
					hasQuotation = true;
				}
				curTokenStart = idx;
				lookingForStart = false;
			}
		}
		else
		{
			if (parserStr[idx] == splitChar || parserStr[idx] == '\n' || parserStr[idx] == '\r')
			{
				if (hasQuotation)
				{
					continue;
				}
				hasQuotation = false;
				lookingForStart = true;
				curTokenEnd = idx;
				tokens.push_back(std::string(parserStr, curTokenStart, curTokenEnd - curTokenStart));
			}
			else if (parserStr[idx] == '"' || parserStr[idx] == '\'')
			{
				hasQuotation = !hasQuotation;
			}
		}
	}

	// check the last one token if has
	if (!lookingForStart)
	{
		tokens.push_back(std::string(parserStr, curTokenStart, parserStr.size() - curTokenStart));
	}
}


// DumpTokens
//------------------------------------------------------------------------------
void Parser::DumpTokens()
{
	INFO("[Tokens Dump]\n");
	for (size_t i = 0; i < tokens.size(); ++i)
	{
		INFO("%s\n", tokens[i].c_str());
	}
}