//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: ProjectParser.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// ProjectParser.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "../Hooker/FBuildHooker.h"
#include "Parser.h"
#include <fstream>


// Constructor
/*explicit*/ ProjectParser::ProjectParser() :
fbToolPath(FBuildHooker::g_FBHookerPath)
{
	InitDefault();
}

/*explicit*/ ProjectParser::ProjectParser(const char *path) :
fbToolPath(FBuildHooker::g_FBHookerPath)
{
	GetContentFromFile(path);
	InitDefault();
}

/*explicit*/ ProjectParser::ProjectParser(const std::string &path) :
fbToolPath(FBuildHooker::g_FBHookerPath)
{
	GetContentFromFile(path.c_str());
	InitDefault();
}

// InitDefault
//------------------------------------------------------------------------------
void ProjectParser::InitDefault()
{
	PROJECTINFO _default;
	_default.projName = "____default____";
	projectInfo.push_back(_default);
}

// ParserContent
//------------------------------------------------------------------------------
void ProjectParser::GetContentFromFile(const char *path)
{
	// TODO
	std::ifstream file(path);
	if (!file)
	{
		FATALERROR("Can't Open file %s\n", path);
		return;
	}
	parserStr = std::string((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());
	DEBUG("project info str: %s\n", parserStr.c_str());
}

// ParserContent
//------------------------------------------------------------------------------
/*virtual*/ void ProjectParser::ParserContent()
{
	if (tokens.size() <= 0)
	{
		return;
	}

	std::string curProjName;

	for (size_t i = 0; i < tokens.size(); ++i)
	{
		INFO("Project Parse token: %s\n", tokens[i].c_str());
		if (StrHelper::StrBeginWithI(tokens[i], "["))
		{
			curProjName = tokens[i];
			StrHelper::StrTrim(curProjName, '[', ']');
			PROJECTINFO projInfo;
			projInfo.projName = curProjName;
			projectInfo.push_back(projInfo);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "VSINSTALLPATH:"))
		{
			projectInfo.back().vsInstallPath = tokens[i].substr(14);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "PROJECTPATH:"))
		{
			projectInfo.back().projPath = tokens[i].substr(12);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "OUTPUTPATH"))
		{
			projectInfo.back().outputPath = tokens[i].substr(11);
			FileDirHelper::TransToFullPath(projectInfo.back().outputPath);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "TOOLSETVER:"))
		{
			projectInfo.back().toolsetVer = tokens[i].substr(11);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "EXECPATH:"))
		{
			projectInfo.back().executablePath = tokens[i].substr(9);
			FilterHookerPath(projectInfo.back().executablePath);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "LIBPATH:"))
		{
			projectInfo.back().libraryPath = tokens[i].substr(8);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "INCLUDEPATH:"))
		{
			projectInfo.back().includePath = tokens[i].substr(12);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "TLOGPATH:"))
		{
			INFO("Project parse tlogpath: %s\n", tokens[i].c_str());
			std::string &projectPath = projectInfo.back().projPath;
			std::string tlogPath = tokens[i].substr(9);
			FileDirHelper::TransToFullPath(tlogPath, projectPath);
			projectInfo.back().tlogPath = tlogPath;
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "REFERENCES:"))
		{
			INFO("Project parse references: %s\n", tokens[i].c_str());
			projectInfo.back().references = tokens[i].substr(11);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "VSLANG:"))
		{
			projectInfo.back().vsLang = tokens[i].substr(7);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "PLATFORM:"))
		{
			projectInfo.back().platform = tokens[i].substr(9);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "CONFIGURATION:"))
		{
			projectInfo.back().configuration = tokens[i].substr(14);
		}
		else
		{
			FATALERROR("Unknown token %s\n", tokens[i].c_str());
		}
	}
}

// FilterHookerPath
//------------------------------------------------------------------------------
void ProjectParser::FilterHookerPath(std::string &path)
{
	if (StrHelper::StrBeginWithI(path, fbToolPath.c_str()))
	{
		size_t pos = path.find_first_of(";");
		path.erase(0, pos);
		DEBUG("Erase fb hooker path: \n", path.c_str());
	}
}

// FindProjectInfo
//------------------------------------------------------------------------------
const ProjectParser::PROJECTINFO& ProjectParser::FindProjectInfo(const std::string &output)
{
	std::vector<PROJECTINFO>::iterator iter = projectInfo.begin();
	for (; iter != projectInfo.end(); ++iter)
	{
		if (StrHelper::StrCompareI((*iter).outputPath, output))
		{
			return (*iter);
		}
	}
	WARNING("Can't find project info of output: %s\n", output.c_str());
	return projectInfo[0];
}

// Serialize
//------------------------------------------------------------------------------
/*virtual*/ void ProjectParser::Serialize()
{
	//DEBUG("ProjectParser Serialize str: %s\n", parserStr.c_str());
}


// DeSerialize
//------------------------------------------------------------------------------
/*virtual*/ void ProjectParser::DeSerialize(const char *buffer)
{
	//DEBUG("ProjectParser deserialize %s\n", buffer);
	parserStr = std::string(buffer);
	Tokenize('\n');
	ParserContent();
	//DumpInfo();
}

// DumpInfo
//------------------------------------------------------------------------------
/*virtual*/ void ProjectParser::DumpInfo()
{
	INFO("[Dump Project Info]\n");
	std::vector<PROJECTINFO>::iterator iter = projectInfo.begin();
	for (; iter != projectInfo.end(); ++iter)
	{
		INFO("ProjectName: %s\n", (*iter).projName.c_str());
		INFO("VSInstallDir: %s\n", (*iter).vsInstallPath.c_str());
		INFO("TLogPath: %s\n", (*iter).tlogPath.c_str());
		INFO("ToolsetVersion: %s\n", (*iter).toolsetVer.c_str());
		INFO("Platform: %s\n", (*iter).platform.c_str());
		INFO("VSLangVersion: %s\n", (*iter).vsLang.c_str());
	}
}


//------------------------------------------------------------------------------
