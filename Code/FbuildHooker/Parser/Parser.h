//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Parser.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Parser.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_PARSER_H
#define FBUILD_PARSER_H

#include <string>
#include <vector>
#include <map>
#include <set>
#include "../Common/Common.h"

typedef std::map<std::string, bool>::iterator		SRC_FILE_ITER;
typedef std::map<std::string, bool>::const_iterator	SRC_FILE_CONST_ITER;

class Parser
{
public:
	explicit Parser() {};
	explicit Parser(const char *string) :parserStr(string){};
	explicit Parser(const std::string &str) :parserStr(str){};
	explicit Parser(const Parser &parser) {};
	virtual ~Parser() {};

	inline void SetParserStr(const std::string &str) { parserStr = str; }
	inline const std::string& GetParseStr() const { return parserStr; };
	inline const std::string& GetSeriaStr() const { return parserStr; };

	inline void AddParserStr(const std::string &str) { parserStr.append("\n"); parserStr.append(str); }

	inline const std::vector<std::string> & GetTokens() const { return tokens; };

	void Tokenize(char splitChar = ' ');
	void Tokenize(char lwrapper, char rwrapper, char splitChar = ' ');
	void DumpTokens();

	virtual void ParserContent() {};
	virtual void Serialize() {};
	virtual void DeSerialize(const char *buffer) {};

	virtual void DumpInfo() {};

	static Parser* createParser(const std::string &cmd, COMMAND_TYPE cmdType);

protected:
	std::string parserStr;
	std::vector<std::string> tokens;
};


// CompilerParser
//------------------------------------------------------------------------------
class BuildParser : public Parser
{
public:
	explicit BuildParser();
	explicit BuildParser(const char *string);
	explicit BuildParser(const std::string &string);
	virtual ~BuildParser();

	virtual void ParserContent();
	virtual void Serialize();
	virtual void DeSerialize(const char *buffer);

	virtual void DumpInfo();
	void DumpScanInfo();

	inline const std::vector<std::string> & GetIncludeDirs() const { return includeDirs; }
	inline const std::vector<std::string> & GetBuildOpts() const { return buildOpts; }
	inline const std::vector<std::string> & GetPreDefines() const { return preDefines; }
	inline const std::vector<std::string> & GetDependency() const { return dependency; }
	inline const std::map<std::string, std::string> & GetOtherOptFile() const { return optFile; }
	inline const std::string & GetOuputFile() const { return outputFile; }
	inline const std::string & GetOuputPath() const { return outputPath; }
	inline const std::string & GetCurrentDir() const { return currentDir; };
	inline const std::string & GetPdbFile() const { return pdbFile; };
	inline const size_t & GetUsePdbType() const { return usePdbType; };
	inline const int GetToolChainType() const { return toolchainType; };
	inline const int GetBuildType() const { return buildType; };
	inline const std::string GetNativeCmd() const { return cmd; };

	inline bool IsPchHeader() const { return genPch; };
	inline const std::string & GetPchHeader() const { return pchHeaderFile; };
	inline const std::string & GetPchOutput() const { return pchOutput; };
	inline const std::vector<std::string> & GetOtherTokens() const { return otherTokens; };
	inline const std::vector<std::string> & GetAdditionalInputs() const { return additionalInputs; }
	inline std::map<std::string, bool> & GetSourceFiles() { return sourceFiles; }

	inline const std::vector<std::string> & GetEnvLibPath() const { return envLibPaths; };
	inline const std::vector<std::string> & GetEnvArray() const { return envs; };

	void GetEnvByName(const std::string & name, std::string & value) const;
	void SetEnvArray(const std::vector<std::string> &envs) { this->envs = envs; };

	SRC_FILE_ITER FindSourceFile(const std::string &);
	bool MatchOutputFile(const std::string &) const;

	bool CheckIfScanFinished() const;

	//void GenTargetInfo();

private:
	void ParserCustom(int &startIdx);
	void ParseMSVCArguments(int &startIdx);
	void ParseGNUArguments(int &startIdx);

	void InitToolChainType(const std::vector<std::string> &tokens, int &startIdx);

	std::vector<std::string> buildOpts;
	std::vector<std::string> preDefines;
	std::vector<std::string> includeDirs;
	std::map<std::string, bool> sourceFiles;		// for cl is .cpp and for lib or link is .obj
	std::vector<std::string> dependency;			// only need by lib or dll
	std::vector<std::string> additionalInputs;

	std::string pdbFile;
	size_t usePdbType;		// 0 with none pdb, 1 with /zi or /zI, 2 with /z7 embed

	std::string pchInput;
	std::string pchOutput;
	std::string pchHeaderFile;
	bool usePch;
	bool genPch;

	std::string outputFile;
	std::string outputPath;		// Fixme: output dir
	std::map<std::string, std::string> optFile;
	std::vector<std::string> otherTokens;

	std::string cmd;
	int toolchainType;
	int buildType;

	// Some MSBuild properties which should be sychronized
	std::string currentDir;
	std::vector<std::string> envLibPaths;
	std::vector<std::string> envs;
};


// ProjectParser
//------------------------------------------------------------------------------
class ProjectParser : public Parser
{
public:
	typedef struct ProjectInfo {
		std::string projName;
		std::string projPath;
		std::string outputPath;
		std::string vsInstallPath;
		std::string toolsetVer;
		std::string executablePath;
		std::string libraryPath;
		std::string includePath;
		std::string tlogPath;
		std::string references;
		std::string vsLang;
		std::string platform;
		std::string configuration;
	} PROJECTINFO;

	explicit ProjectParser();
	explicit ProjectParser(const char *path);
	explicit ProjectParser(const std::string &path);
	virtual ~ProjectParser() {};

	void InitDefault();

	// The paramater name is output name

	inline const std::string & GetProjectName(const std::string &output) { return FindProjectInfo(output).projName; };

	inline const std::string & GetVSInstallDir(const std::string &output) { return FindProjectInfo(output).vsInstallPath; };
	inline const std::string & GetProjectDir(const std::string &output) { return FindProjectInfo(output).projPath; };
	inline const std::string & GetVSLangVer(const std::string &output) { return FindProjectInfo(output).vsLang; };
	inline const std::string & GetToolSetVer(const std::string &output) { return FindProjectInfo(output).toolsetVer; };
	inline const std::string & GetTlogPath(const std::string &output) { return FindProjectInfo(output).tlogPath; };
	inline const std::string & GetReferences(const std::string &output) { return FindProjectInfo(output).references; };
	inline const std::string & GetEnvPath(const std::string &output) { return FindProjectInfo(output).executablePath; };
	inline const std::string & GetEnvInclude(const std::string &output) { return FindProjectInfo(output).includePath; };
	inline const std::string & GetPlatform(const std::string &output) { return FindProjectInfo(output).platform; };
	inline const std::string & GetConfiguration(const std::string &output) { return FindProjectInfo(output).configuration; };

	inline const std::vector<PROJECTINFO> & GetTotalProject() const { return projectInfo; };

	virtual void ParserContent();
	virtual void Serialize();
	virtual void DeSerialize(const char *buffer);

	virtual void DumpInfo();

private:
	const PROJECTINFO& FindProjectInfo(const std::string &output);
	void FilterHookerPath(std::string &);
	void GetContentFromFile(const char *path);

	std::string fbToolPath;
	std::vector<PROJECTINFO> projectInfo;
};


// EnvironmentParser
//------------------------------------------------------------------------------
class EnvParser : public Parser
{
public:
	explicit EnvParser() :showStartInfo(false) {};
	explicit EnvParser(const char *string) : Parser(string) { showStartInfo = false; };
	explicit EnvParser(const std::string &string) : Parser(string) { showStartInfo = false; };
	virtual ~EnvParser() {};

	inline const std::string & GetSolutionDir() const { return solutionDir; };
	inline const std::string & GetSolutionName() const { return solutionName; };
	inline const std::string & GetRawExtraOpts() const { return rawExtraOpts; };
	inline const std::vector<std::string> & GetExtraOpt() const { return extraOpts; };

	inline const std::string & GetFastbuildOpts() const { return fbOpts; };

	inline ProjectParser & GetProjectInfo() { return projectParser; };

	inline const std::string & GetFBFilePath() const { return fbFilePath; };
	inline const std::string & GetCurTargetName() const { return targetNames.back(); };
	inline const std::string & GetCurTargetPath() const { return targetPaths.back(); };
	inline const std::vector<std::string> & GetTargetNames() const { return targetNames; };
	inline const std::vector<std::string> & GetTargetPaths() const { return targetPaths; };
	inline const std::string & GetBrokeragePath() const { return brokeragePath; };

	inline bool GetShowStartInfo() const { return showStartInfo; };
	inline void SetShowStartInfo(bool enable) { showStartInfo = enable; };

	std::string GetTargetsNameStr();
	std::string GetTargetsPathStr();

	virtual void ParserContent();
	virtual void Serialize();
	virtual void DeSerialize(const char *buffer);

	virtual void DumpInfo();

private:
	ProjectParser projectParser;

	std::string solutionDir;
	std::string solutionName;
	std::string rawExtraOpts;
	std::vector<std::string> extraOpts;

	std::string fbOpts;

	// used for link process
	std::string fbFilePath;
	std::vector<std::string> targetNames;
	std::vector<std::string> targetPaths;
	std::string brokeragePath;

	// other infomation
	bool showStartInfo;
};


//------------------------------------------------------------------------------
#endif // FBUILD_PARSER_H
