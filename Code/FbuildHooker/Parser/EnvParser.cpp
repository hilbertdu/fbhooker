//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: EnvParser.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// EnvParser.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Parser.h"

// ParserContent
//------------------------------------------------------------------------------
/*virtual*/ void EnvParser::ParserContent()
{
	if (tokens.size() <= 0)
	{
		return;
	}

	for (size_t i = 0; i < tokens.size(); ++i)
	{
		DEBUG("Env Parse token: %s\n", tokens[i].c_str());
		if (StrHelper::StrBeginWithI(tokens[i], "/SLNDIR:"))
		{
			solutionDir = tokens[i].substr(8);
			StrHelper::StrTrim(solutionDir, '"');
			StrHelper::StrTrim(solutionDir, ' ');
			if (solutionDir.back() != '\\')
			{
				solutionDir.push_back('\\');
			}
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/SLNNAME:"))
		{
			solutionName = tokens[i].substr(9);
			StrHelper::StrTrim(solutionName, '"');
			StrHelper::StrTrim(solutionName, ' ');
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/EXTRAOPT:"))
		{
			rawExtraOpts = tokens[i].substr(10);
			StrHelper::StrTrim(rawExtraOpts, '"');
			Parser parseOpt(rawExtraOpts);
			parseOpt.Tokenize();
			extraOpts = parseOpt.GetTokens();
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/FBOPTS:"))
		{
			fbOpts = tokens[i].substr(8);
			StrHelper::StrTrim(fbOpts, '"');
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/PROJECTINFO:"))
		{
			std::string projInfoFile = tokens[i].substr(13);
			StrHelper::StrTrim(projInfoFile, '"');
			projectParser = ProjectParser(projInfoFile);
			projectParser.Tokenize('\n');
			projectParser.ParserContent();
			projectParser.DumpInfo();
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/FBFILE:"))
		{
			fbFilePath = tokens[i].substr(8);
			StrHelper::StrTrim(fbFilePath, '"');
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/TARGET:"))
		{
			std::string names = tokens[i].substr(8);
			StrHelper::StrTrim(names, '"');
			StrHelper::StrSplit(names, '|', targetNames);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/TARGETPATH:"))
		{
			std::string paths = tokens[i].substr(12);
			StrHelper::StrTrim(paths, '"');
			StrHelper::StrSplit(paths, '|', targetPaths);
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/BROKERAGEPATH:"))
		{
			brokeragePath = tokens[i].substr(15);
			StrHelper::StrTrim(brokeragePath, '"');
		}
		else if (StrHelper::StrBeginWithI(tokens[i], "/STARTINFO"))
		{
			showStartInfo = true;
		}
		else if (tokens[i].size() > 0 && tokens[i].front() == '"' && tokens[i].back() == '"')
		{
			tokens[i] = tokens[i].substr(1, tokens[i].size() - 2);
			i--;
		}
		else
		{
			WARNING("Skip token %s\n", tokens[i].c_str());
		}
	}
	DEBUG("Parse env tokens finished\n");
}

// Serialize
//------------------------------------------------------------------------------
/*virtual*/ void EnvParser::Serialize()
{
	//DEBUG("Env Serialize str: %s\n", parserStr.c_str());
}


// DeSerialize
//------------------------------------------------------------------------------
/*virtual*/ void EnvParser::DeSerialize(const char *buffer)
{
	//DEBUG("EnvParser deserialize %s\n", buffer);
	parserStr = std::string(buffer);
	Tokenize();
	ParserContent();
	//DumpInfo();
}

// GetTargetsNameStr
//------------------------------------------------------------------------------
std::string EnvParser::GetTargetsNameStr()
{
	std::string targetNamesStr;
	StrHelper::StrJoin(targetNamesStr, targetNames);
	return targetNamesStr;
}

// GetTargetsPathStr
//------------------------------------------------------------------------------
std::string EnvParser::GetTargetsPathStr()
{
	std::string targetPathsStr;
	StrHelper::StrJoin(targetPathsStr, targetPaths);
	return targetPathsStr;
}

// DumpInfo
//------------------------------------------------------------------------------
/*virtual*/ void EnvParser::DumpInfo()
{
	INFO("[Dump Env Info]\n");
	INFO("SolutionName: %s\n", solutionName.c_str());
	INFO("SolutionDir: %s\n", solutionDir.c_str());
	INFO("fbOpts: %s\n", fbOpts.c_str());
}


//------------------------------------------------------------------------------