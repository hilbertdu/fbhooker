//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: main.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Includes
//------------------------------------------------------------------------------
#include "Hooker/FBuildHooker.h"

int Main(int argc, char **argv);

// main
//------------------------------------------------------------------------------
int main(int argc, char **argv)
{
#if defined( DUMP_OPEN ) && defined( __WINDOWS__ )
	DumpHelper::Init("FbVSHooker.dmp", DumpHelper::FULL_DUMP);
#endif
	int ret = Main(argc, argv);
	return ret;
}

int Main(int argc, char **argv)
{
	// Don't buffer output
	setvbuf(stdout, nullptr, _IONBF, 0);
	setvbuf(stderr, nullptr, _IONBF, 0);

	// Start hooker loop
	FBuildHooker buildHooker;
	buildHooker.InitCommands(argc, argv);
	buildHooker.InitLogger();
	int ret = buildHooker.DoService();
	buildHooker.FinalizeLogger();
	return ret;
}

//------------------------------------------------------------------------------
