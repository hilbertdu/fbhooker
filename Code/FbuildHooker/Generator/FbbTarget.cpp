//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbTarget.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbTarget.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "FbbTarget.h"
#include "FbbToolChain.h"
#include "../Common/Common.h"
#include "../Generator/FbbGenerator.h"
#include "../Parser/Parser.h"
#include "../Template/FBTTarget.h"
#include "../Task/PrebuildPDBTask.h"

#include <algorithm>

BuildTarget::BuildTarget(std::string name, FASTBUILDTARGET targetType) :
name(name),
fbTargetType(targetType),
currentObjId(0),
useQuickMode(false)
{
}

BuildTarget::~BuildTarget()
{
	ASSERT(!(fbTargetType == OBJECTLIST && objectTargets.size() > 0));

	std::vector<BuildTarget*>::iterator iter = objectTargets.begin();
	for (; iter != objectTargets.end(); ++iter)
	{
		SAFE_DELETE(*iter);
	}
}

void BuildTarget::GenerateFbb()
{
	switch (fbTargetType)
	{
	case OBJECTLIST:
		GenerateObjectListFunc();
		break;
	case LIBRARY:
		GenerateLibraryFunc();
		break;
	case DLL:
		GenerateDllFunc();
		break;
	/*case EXECUTABLE:
		GenerateExecutableFunc();
		break;*/
	default:
		FATALERROR("No build type: %d\n", fbTargetType);
		return;
	}
}

void BuildTarget::GenerateObjectListFunc()
{
	DEBUG("GenerateObjectListFunc %s\n", name.c_str());
	// Options
	std::string opt;
	StrHelper::StrEscape(opt, options, '\'', '^');

	// Input files
	std::string inputFilesStr = "";
	StrHelper::StrJoin(inputFilesStr, sourceFiles, ",\n\t\t\t\t\t\t\t\t", "'");

	// PreDependencies	--	((Deprecated))
	std::string preDenpendStr = "";
	//StrJoin(preDenpendStr, parent->GetTotalDependency(), ",\n\t\t\t\t\t\t\t\t", "'");	

	// Check pch depends
	if (pchHeaderFile == "" && pchTargetName != "")
	{
		DEBUG("PCH depends: %s\n", pchTargetName.c_str());
		preDenpendStr += (preDenpendStr == "" ? "'" + pchTargetName + "'" : \
			",\n\t\t\t\t\t\t\t\t'" + pchTargetName + "'");
	}

	// If use quick mode, set environment
	std::string environmentStr = "";
	if (useQuickMode)
	{
		StrHelper::StrJoin(environmentStr, envs, ",\n\t\t\t\t\t\t\t\t", "'");
		DEBUG("Environment string: %s\n", environmentStr.c_str());
	}

	// If parent is a Library node, set remote pdb dir
	std::string remotePdbDir = " ";
	if (parent->GetTargetType() == LIBRARY)
	{
		remotePdbDir = parent->GetOutputPath();
		DEBUG("Remote pdb dir: %s\n", remotePdbDir.c_str());
	}

	// Using namespace
	std::string usingStruct;
	switch (toolChainType)
	{
	case ToolChain::TOOLCHAIN_TYPE::GNU:
		usingStruct = "Using( .BaseCompilerConfig_" + toolsetKey + " )\n";
		break;
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
		usingStruct = "Using( .MSVCBaseConfigV" + toolsetVer + "_" + toolsetKey + " )\n";
		break;
	}

	// PCH input and output
	if (pchHeaderFile != "")
	{
		StrHelper::StrFormat(fbbStr, FBObjectListPch,
			usingStruct.c_str(),
			name.c_str(),
			environmentStr.c_str(),
			//sourceFiles[0].c_str(),		// Only support one pch file input (deprecated)
			inputFilesStr.c_str(),
			pchOutput.c_str(),
			opt.c_str(),
			pchHeaderFile.c_str(),
			outputPath.c_str(),
			//outputPrefix.size() > 0 ? "" : "//",
			//outputPrefix.c_str(),
			outputFile.size() > 0 ? "" : "//",
			outputFile.c_str(),
			"",//inputFilesStr.c_str(),     // Support by fastbuild now
			pchHeaderFile.c_str(),
			preDenpendStr.c_str());
	}
	else
	{
		StrHelper::StrFormat(fbbStr, FBObjectList,
			usingStruct.c_str(),
			name.c_str(),
			environmentStr.c_str(),
			opt.c_str(),
			outputPath.c_str(),
			//outputPrefix.size() > 0 ? "" : "//",
			//outputPrefix.c_str(),
			outputFile.empty() ? "//" : "",
			outputFile.c_str(),
			inputFilesStr.c_str(),
			remotePdbDir.c_str(),
			preDenpendStr.c_str());
	}
}

void BuildTarget::GenerateLibraryFunc()
{
	DEBUG("GenerateLibraryFunc\n");

	// Options
	std::string opt;
	StrHelper::StrEscape(opt, options, '\'', '^');

	// ObjectList inputs
	std::string objectListInputs = "";
	StrHelper::StrJoin(objectListInputs, objectTargetName, ",\n\t\t\t\t\t\t\t\t", "'");

	// Additional inputs
	std::string extraObjInputs = "";
	StrHelper::StrJoin(extraObjInputs, extraObjectFiles, ",\n\t\t\t\t\t\t\t\t", "'");

	// Extral depend
	std::string extraDepends = "";
	StrHelper::StrJoin(extraDepends, dependencyExternal, " ", "\"");

	// Lib depend	--	(Deprecated)
	std::string libDepends = "";
	//StrJoin(libDepends, dependencyLibPath, " ");

	// Depend targets
	std::string dependTargets = "";
	if (useQuickMode)
	{
		StrHelper::StrJoin(dependTargets, innerDependency, ",\n\t\t\t\t\t\t\t\t", "'");
	}

	// Other deps
	std::string otherDeps = "";
	StrHelper::StrJoin(otherDeps, otherTokens, " ");

	// If use quick mode, set environment
	std::string environmentStr = "";
	if (useQuickMode)
	{
		StrHelper::StrJoin(environmentStr, envs, ",\n\t\t\t\t\t\t\t\t", "'");
		DEBUG("Environment string: %s\n", environmentStr.c_str());
	}

	// TODO: Check reference deps (not used)
	FilterReferenceDeps();

	// TODO output path -- actually is no need but fbb file needs	

	// Using namespace and /c/-c
	std::string usingStruct, fbuildRequired;
	switch (toolChainType)
	{
	case ToolChain::TOOLCHAIN_TYPE::GNU:
		opt = "";		// FIXME
		usingStruct = "Using( .BaseLibrarianConfig_" + toolsetKey + " )\n";
		fbuildRequired += "-c";
		break;
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
		usingStruct = "Using( .MSVCBaseConfigV" + toolsetVer + "_" + toolsetKey + " )\n";
		fbuildRequired += "/c";
		break;
	}

	DEBUG("StrFormat\n");
	StrHelper::StrFormat(fbbStr, FBLibrary,
		usingStruct.c_str(),
		name.c_str(),
		environmentStr.c_str(),
		libDepends.c_str(),
		extraDepends.c_str(),
		otherDeps.c_str(),
		opt.c_str(),
		outputFile.c_str(),
		fbuildRequired.c_str(),
		outputPath.c_str(),
		objectListInputs.c_str(),
		extraObjInputs.c_str(),
		dependTargets.c_str(),
		tlogFilePath.empty() ? "//" : "",
		tlogFilePath.c_str());
}

void BuildTarget::GenerateDllFunc()
{
	DEBUG("GenerateDllFunc\n");
	//// A dll target aways has a input lib target
	//std::string libName = name + "-lib";
	//std::string libOutput = outputFile.substr(0, outputFile.size() - 4) + ".lib";

	// Options
	std::string opt;
	StrHelper::StrEscape(opt, options, '\'', '^');

	// ObjectList inputs
	std::string objectListInputs = "";
	StrHelper::StrJoin(objectListInputs, objectTargetName, ",\n\t\t\t\t\t\t\t\t", "'");

	// Depend targets
	std::string dependTargets = "";
	if (useQuickMode)
	{
		StrHelper::StrJoin(dependTargets, innerDependency, ",\n\t\t\t\t\t\t\t\t", "'");
	}

	// Additional inputs
	std::string extraObjInputs = "";
	//StrJoin(additionalInputs, dependencyDll, ",\n\t\t\t\t\t\t\t\t", "'");
	StrHelper::StrJoin(extraObjInputs, extraObjectFiles, ",\n\t\t\t\t\t\t\t\t", "'");

	std::string additionalFiles = "";
	StrHelper::StrJoin(additionalFiles, additionalInputs, ",\n\t\t\t\t\t\t\t\t", "'");

	// Lib depend -- and dll import lib	--	(Deprecated)
	std::string libDepends = "";
	//StrJoin(libDepends, dependencyLibPath, " ");

	// Other deps
	std::string otherDeps = "";
	StrHelper::StrJoin(otherDeps, otherTokens, " ");

	// Extral depend
	std::string extraDepends = "";
	StrHelper::StrJoin(extraDepends, dependencyExternal, " ", "\"");

	// Env lib path
	std::string libPathStr = "";
	StrHelper::StrJoinEx(libPathStr, libPaths, " ", "/LIBPATH:\"", "\"");
	DEBUG("Lib path str = %s\n", libPathStr.c_str());

	// If use quick mode, set environment
	std::string environmentStr = "";
	if (useQuickMode)
	{
		StrHelper::StrJoin(environmentStr, envs, ",\n\t\t\t\t\t\t\t\t", "'");
		DEBUG("Environment string: %s\n", environmentStr.c_str());
	}

	// Filter reference deps
	FilterReferenceDeps();

	bool isExe = (outputFile.rfind(".exe") == outputFile.size() - 4);

	// Using namespace
	std::string usingStruct;
	switch (toolChainType)
	{
	case ToolChain::TOOLCHAIN_TYPE::GNU:
		usingStruct = "Using( .BaseLinkerConfig_" + toolsetKey + " )\n";
		break;
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
		usingStruct = "Using( .MSVCBaseConfigV" + toolsetVer + "_" + toolsetKey + " )\n";
		break;
	}

	StrHelper::StrFormat(fbbStr, isExe ? FBExecutable : FBDLL,
		usingStruct.c_str(),
		name.c_str(),
		environmentStr.c_str(),
		libDepends.c_str(),
		extraDepends.c_str(),
		otherDeps.c_str(),
		opt.c_str(),
		libPathStr.c_str(),
		outputFile.c_str(),
		additionalFiles.c_str(),
		dependTargets.c_str(),
		tlogFilePath.empty() ? "//" : "",
		tlogFilePath.c_str(),
		references.c_str(),
		objectListInputs.c_str(),
		extraObjInputs.c_str());
}

void BuildTarget::GenerateExecFunc()
{

}

void BuildTarget::AddSourceFile(const std::string &sourceFile, const std::string &prefix)
{
	//DEBUG("AddSourceFile: %s %s\n", sourceFile.c_str(), prefix.c_str());
	if (prefix.size() > 0)
	{
		outputPrefix = prefix;
	}
	sourceFiles.push_back(sourceFile);
}

void BuildTarget::SetSourceFile(const std::string &sourceFile, const std::string &outputFile)
{
	sourceFiles.clear();
	sourceFiles.push_back(sourceFile);
	this->outputFile = outputFile;
}

void BuildTarget::ClearSourceFile()
{
	sourceFiles.clear();
}

void BuildTarget::FindAndRemoveSrcFile(const std::string &sourceFileName, std::string &sourceFilePath)
{
	std::vector<std::string>::iterator srcIter = sourceFiles.begin();
	for (; srcIter != sourceFiles.end(); ++srcIter)
	{
		size_t startPos = (*srcIter).find_last_of("\\");
		std::string fileName = (*srcIter).substr(startPos + 1);

		if (fileName == sourceFileName)
		{
			sourceFilePath = (*srcIter);
			sourceFiles.erase(srcIter);
			return;
		}
	}
	sourceFilePath.clear();
}

void BuildTarget::SetOption(const std::vector<std::string>& option, const std::vector<std::string>& extraOption)
{
	DEBUG("SetOption count: %d %d\n", option.size(), extraOption.size());
	if (fbTargetType == OBJECTLIST)
	{
		SetComplieOption(option, extraOption);
	}
	else
	{
		SetLinkOption(option);
	}
}

void BuildTarget::SetComplieOption(const std::vector<std::string>& option, const std::vector<std::string>& extraOption)
{
	std::vector<std::string>::const_iterator iterOpt;
	std::vector<std::string> excludeOpt;

	if (std::find(extraOption.begin(), extraOption.end(), "/Z7") != extraOption.end())
	{
		DEBUG("SetComplieOption /Z7\n");
		excludeOpt.push_back("/Zi");
		excludeOpt.push_back("/ZI");
	}
	for (iterOpt = option.begin(); iterOpt != option.end(); ++iterOpt)
	{
		DEBUG("SetComplieOption option: %s\n", (*iterOpt).c_str());
		if (std::find(excludeOpt.begin(), excludeOpt.end(), (*iterOpt)) == excludeOpt.end() &&
			std::find(extraOption.begin(), extraOption.end(), (*iterOpt)) == extraOption.end())
		{
			options += ' ';
			options += (*iterOpt);
		}
	}
	for (iterOpt = extraOption.begin(); iterOpt != extraOption.end(); ++iterOpt)
	{
		options += ' ';
		options += (*iterOpt);
	}
	DEBUG("SetComplieOption options: %s\n", options.c_str());
}

void BuildTarget::SetLinkOption(const std::vector<std::string>& option)
{
	std::string opt, extraOpt;
	StrHelper::StrJoin(opt, option, " ");
	options = opt + " " + extraOpt;
}

void BuildTarget::SetIncludes(const std::vector<std::string> includes)
{
}

void BuildTarget::FilterReferenceDeps()
{
	std::vector<std::string> refsVec;
	StrHelper::StrSplit(references, ' ', refsVec, '"');
	std::vector<std::string>::iterator iter = refsVec.begin();
	while (iter != refsVec.end())
	{
		if (std::find(allDependencyPath.begin(), allDependencyPath.end(), *iter) == allDependencyPath.end())
		{
			INFO("Remove unused reference %s\n", (*iter).c_str());
			iter = refsVec.erase(iter);
		}
		else
		{
			iter++;
		}
	}
	StrHelper::StrJoin(references, refsVec, " ", "\"");
}

void BuildTarget::AddDependency(const std::string &depend, const FASTBUILDTARGET type)
{
	if (type == DLL)
	{
		DEBUG("Add dll target depend %s\n", depend.c_str());
		dependencyDll.push_back(depend);
		innerDependency.push_back(depend);
	}
	else if (type == LIBRARY)
	{
		// Be aware that for the lib target we should use absolutely path
		DEBUG("Add lib target depend %s\n", depend.c_str());
		dependencyLib.push_back(depend);
		innerDependency.push_back(depend);
	}
	else if (type == EXTERNAL)
	{
		DEBUG("Add external depend %s\n", depend.c_str());
		dependencyExternal.push_back(depend);
	}
}

void BuildTarget::AddDependencyPath(const std::string &dependPath)
{
	// Include lib and dll import lib
	//dependencyLibPath.push_back(dependPath);
	DEBUG("Add dependency path: %s\n", dependPath.c_str());
	std::string depend;
	StrHelper::StrUpper(depend, dependPath);
	allDependencyPath.push_back(depend);
}

void BuildTarget::AddObjectLists(const std::vector<BuildTarget*> &objTargets)
{
	DEBUG("Add object List %s\n", name.c_str());
	std::vector<BuildTarget*>::const_iterator iter = objTargets.begin();
	for (; iter != objTargets.end(); ++iter)
	{
		DEBUG("Add target name %s\n", (*iter)->name.c_str());
		objectTargetName.push_back((*iter)->name);
	}
	objectTargets.insert(objectTargets.end(), objTargets.begin(), objTargets.end());
}

void BuildTarget::AddExtraObjFile(const std::string objFile)
{
	DEBUG("Add extra obj file: %s\n", objFile.c_str());
	extraObjectFiles.push_back(objFile);
}

void BuildTarget::SetOutputPath(const std::string &outputPath)
{
	DEBUG("Set output path: %s\n", outputPath.c_str());
	this->outputPath = outputPath;
}

void BuildTarget::SetOutputFile(const std::string &outputFile)
{
	DEBUG("SetOutputFile: %s\n", outputFile.c_str());
	this->outputFile = outputFile;
}

void BuildTarget::SetOutputPrefix(const std::string prefix)
{
	outputPrefix = prefix;
}

void BuildTarget::SetOtherTokens(const std::vector<std::string> &tokens)
{
	otherTokens = tokens;
}

void BuildTarget::SetParent(BuildTarget* parent)
{
	this->parent = parent;
}

bool BuildTarget::IsDependsOnTarget(const std::string &targetName)
{
	if (std::find(dependencyDll.begin(), dependencyDll.end(), targetName) != dependencyDll.end() ||
		std::find(dependencyLib.begin(), dependencyLib.end(), targetName) != dependencyLib.end())
	{
		DEBUG("%s depends on %s\n", name.c_str(), targetName.c_str());
		return true;
	}
	return false;
}

// DumpInfo
//------------------------------------------------------------------------------
void BuildTarget::DumpInfo()
{
	INFO("[Dump Targets Info] %s\n", name.c_str());

	INFO("[buildOpts]\n");
	INFO("%s\n", options.c_str());

	INFO("[outputFile]:\n");
	INFO("%s\n", outputFile.c_str());

	INFO("[outputPath]:\n");
	INFO("%s\n", outputPath.c_str());

	INFO("[outputPrefix]:\n");
	INFO("%s\n", outputPrefix.c_str());

	INFO("[sourceFiles]:\n");
	for (size_t i = 0; i < sourceFiles.size(); ++i)
	{
		INFO("%s\n", sourceFiles[i].c_str());
	}

	if (objectTargets.size() > 0)
	{
		INFO("[objectTargets]:\n");
		for (size_t i = 0; i < objectTargets.size(); ++i)
		{
			objectTargets[i]->DumpInfo();
		}
	}
}

// DumpFbbStr
//------------------------------------------------------------------------------
void BuildTarget::DumpFbbStr()
{
	INFO("[Dump FBB String] %s\n", name.c_str());
	INFO("%s\n", fbbStr.c_str());

	if (objectTargets.size() > 0)
	{
		INFO("[Dump Dep ObjectList String] %s\n", name.c_str());
		for (size_t i = 0; i < objectTargets.size(); ++i)
		{
			objectTargets[i]->DumpFbbStr();
		}
	}
}
