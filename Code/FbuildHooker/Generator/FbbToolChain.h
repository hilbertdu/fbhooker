//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbToolChain.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbTarget.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TOOLCHAIN_CONFIG_H
#define FBUILD_TOOLCHAIN_CONFIG_H

#include "Core/ToolChain/ToolChain.h"
#include "Core/Env/Assert.h"
#include <string>
#include <vector>
#include <map>


class FbbToolChain
{
public:

	typedef struct SVCToolChain {
		std::string toolsetKey;
		std::string toolsetVer;
		std::string toolsetPath;
		std::string fbbStr;
	} SVCToolChain;

	typedef struct SGNUToolChain {
		std::string toolsetKey;
		std::string rootPath;
		std::string command;
		std::string version;
		std::string fbbStr;
		int buildType;
	} SGNUToolChain;

	FbbToolChain();
	~FbbToolChain();

	// for windows MSVC
	void AddPathEnv(const std::string &pathEnv);

	// for linux gnu
	void AddToolChainPath(const std::string &path);

	// for both
	void AddToolSet(int toolchainType, int buildType);

	void GenerateFbb();
	void DumpFbbStr();

	std::string GetCurToolsetKey(int buildType = -1);
	std::string GetCurToolsetVer();
	const std::string & GetFbbStr(int toolchainType, const std::string &key) const;

	inline void SetBuildState(int state) { buildStatus = state; };

private:
	const std::string __default__;

	size_t GenToolsetKey(const std::string &toolsetPathVer);

	std::map<std::string, SGNUToolChain> gnuToolsets;
	std::map<std::string, SVCToolChain> vcToolsets;

	std::map<std::string, size_t> vcToolsetKeys;					// toolsetVer -> key
	std::map<int, std::map<std::string, size_t>> gnuToolsetKeys;	// (buildType, toolsetPath) -> key

	std::string curVCToolsetKey;
	std::map<int, std::string> curGNUToolsetKey;
	
	int curToolsetType;
	int curBuildType;
	int buildStatus;
};


#endif	// FBUILD_TOOLCHAIN_CONFIG_H