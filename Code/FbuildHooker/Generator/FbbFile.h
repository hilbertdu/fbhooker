//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbFile.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbFile.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_FILE_H
#define FBUILD_FILE_H

#include <map>
#include <fstream>
#include <vector>

class BuildTarget;

class FbbFile
{
public:
	FbbFile();
	~FbbFile();

	inline void SetBasePath(const std::string &path) { fbbPath = path; };

	inline const std::string & GetFbbPath() const { return fbbPath; };
	inline const std::string & GetFbbFileName() const { return fbbFileName; };
	inline const std::string & GetFbbConfName() const { return fbbConfName; };
	inline std::string GetFbbFilePath() { return fbbPath + '\\' + fbbFileName; };
	inline std::string GetFbbConfPath() { return fbbPath + '\\' + fbbConfName; };

	inline const std::string GetCurFbbFilePath() const { return curFbbFilePath; };
	inline const std::string GetCurFbbFileName() const { return curFbbFileName; };

	inline int GetState() { return genFileState; };

	// --> Only these functions are usefull
	void InitConfigPath();
	void GenSettingFile();
	void GenToolsetConfigFile(const std::string &uniSuffix, const std::string &toolsetConfStr);
	void GenFbbProjectFile(const std::string &name, const std::string &dir, const std::string &fbbStr);
	void GenFbbMainFile(const std::vector<std::string> &dirs, const std::vector<std::string> &names);
	// -->

private:
	std::string fbbFileName;
	std::string fbbConfName;
	std::string fbbPath;

	std::string curFbbFilePath;
	std::string curFbbFileName;

	std::vector<std::string> configList;

	int curFBuildIdx;
	int curConfigIdx;
	int genFileState;
};

#endif // FBUILD_FILE_H
//------------------------------------------------------------------------------
