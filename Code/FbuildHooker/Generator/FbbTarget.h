//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbTarget.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbTarget.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TARGET_H
#define FBUILD_TARGET_H

#include "Core/ToolChain/ToolChain.h"
#include <string>
#include <vector>

class BuildParser;

enum FASTBUILDTARGET
{
	OBJECTLIST		= 0,
	LIBRARY			= 1,
	DLL				= 2,
	//EXECUTABLE,
	EXEC			= 4,
	EXTERNAL		= 5
};

class BuildTarget
{
public:
	BuildTarget(std::string name, FASTBUILDTARGET targetType);
	~BuildTarget();

	void EnableQuickMode() { useQuickMode = true; };
	void SetEnvironment(const std::vector<std::string> &envs) { this->envs = envs; };

	void AddSourceFile(const std::string &sourceFile, const std::string &prefix = "");
	void SetSourceFile(const std::string &sourceFile, const std::string &outputFile);
	void FindAndRemoveSrcFile(const std::string &sourceFileName, std::string &sourceFilePath);
	void ClearSourceFile();

	void SetOption(const std::vector<std::string>& option, const std::vector<std::string>& extraOption);
	void SetIncludes(const std::vector<std::string>);
	void AddDependency(const std::string &depend, const FASTBUILDTARGET type);
	void AddDependencyPath(const std::string &dependPath);
	void AddObjectLists(const std::vector<BuildTarget*> &);
	void AddExtraObjFile(const std::string objFile);

	void SetOutputPath(const std::string &);
	void SetOutputFile(const std::string &);	// TODO Currentlly FBuild can not support output file specify, only can add a prefix
	void SetOutputPrefix(const std::string);	// Set prefix
	void SetOtherTokens(const std::vector<std::string> &);
	void SetParent(BuildTarget* parent);

	inline void SetToolChainType(int type) { toolChainType = type; };
	inline void SetNativeCmd(const std::string &cmd) { nativeCmd = cmd; };
	inline void SetToolsetKey(const std::string &toolsetKey) { this->toolsetKey = toolsetKey; };
	inline void SetToolsetVer(const std::string &toolsetVer) { this->toolsetVer = toolsetVer; };

	inline int GetToolChainType() const { return toolChainType; };
	inline const std::string & GetNativeCmd() const { return nativeCmd; };
	inline const std::string & GetToolsetKey() const { return toolsetKey; };

	inline void SetPchOutput(const std::string &pchOutput) { this->pchOutput = pchOutput; };
	inline void SetPchHeader(const std::string &pchHeader) { pchHeaderFile = pchHeader; };

	inline void SetLibraryPath(const std::vector<std::string> &libPaths) { this->libPaths = libPaths; };
	inline void SetAdditional(const std::vector<std::string> &additional) { additionalInputs = additional; };
	inline void SetTlogPath(const std::string &tlogPath) { tlogFilePath = tlogPath; };
	inline void SetReferences(const std::string &references) { this->references = references; };

	inline const FASTBUILDTARGET GetTargetType() const { return fbTargetType; };
	inline int & GetCurrentObjId() { return currentObjId; };
	inline void AddCurrentObjId(int num) { currentObjId += num; };

	inline bool IsValid() { return sourceFiles.size() > 0; };
	inline const std::string & GetFbbStr() const { return fbbStr; };
	inline const std::string & GetOutputPath() const { return outputPath; };
	inline const std::string & GetOutputFile() const { return outputFile; };
	inline const std::vector<std::string> & GetDependencyLib() const { return dependencyLib; };
	inline const std::vector<std::string> & GetDependencyDll() const { return dependencyDll; };
	inline const std::vector<std::string> & GetDependencyExt() const { return dependencyExternal; };
	inline const std::vector<std::string> & GetInnerDependency() const { return innerDependency; };
	inline const std::vector<std::string> & GetEnvironment() const { return envs; };

	inline const std::vector<std::string> & GetSourceFiles() const { return sourceFiles; };
	inline std::vector<BuildTarget*> & GetObjectTargets() { return objectTargets; };

	inline const std::string & GetOutputPrefix() const { return outputPrefix; };

	inline bool IsPchTarget() const { return pchOutput.size() > 0; };
	inline void SetPchDependTarget(const std::string & target) { pchTargetName = target; }

	void FilterReferenceDeps();
	bool IsDependsOnTarget(const std::string &target);

	void GenerateFbb();

	void DumpInfo();
	void DumpFbbStr();

	std::string name;	// TODO const

private:
	void GenerateObjectListFunc();
	void GenerateLibraryFunc();
	void GenerateDllFunc();
	void GenerateExecFunc();

	void SetComplieOption(const std::vector<std::string>& option, const std::vector<std::string>& extraOption);
	void SetLinkOption(const std::vector<std::string>& option);

	const FASTBUILDTARGET fbTargetType;
	int toolChainType;
	std::string nativeCmd;

	std::string options;							// all
	std::vector<std::string> dependencyExternal;	// lib and dll ( file path )
	//std::vector<std::string> dependencyTarget;	// lib and dll ( target name )
	std::vector<std::string> dependencyLib;
	std::vector<std::string> dependencyDll;
	std::vector<std::string> innerDependency;
	std::vector<std::string> allDependencyPath;
	std::vector<std::string> dependencyLibPath;
	std::vector<std::string> includes;				// Not used now, just put in options
	std::string outputFile;							// lib and dll
	std::string outputPath;							// object list
	std::string outputPrefix;						// object list
	std::vector<std::string> sourceFiles;			// all
	std::vector<BuildTarget*> objectTargets;		// lib and dll
	std::vector<std::string> objectTargetName;		// lib and dll

	std::vector<std::string> extraObjectFiles;      // without target [use in vs increment mode]

	std::string pchOutput;
	std::string pchHeaderFile;
	std::string pchTargetName;
	std::string tlogFilePath;
	std::string references;
	std::vector<std::string> libPaths;
	std::vector<std::string> additionalInputs;
	std::vector<std::string> otherTokens;

	std::vector<std::string> envs;

	std::string fbbStr;

	BuildTarget* parent;
	int currentObjId;			// For generate a denpend objectlist name		
	bool useQuickMode;			// Use quick mode
	std::string toolsetKey;
	std::string toolsetVer;		// For MSVC
};

#endif // FBUILD_TARGET_H