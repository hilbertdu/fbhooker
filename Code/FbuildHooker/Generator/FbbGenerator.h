//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbGenerator.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbGenerator.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_GENERATOR_H
#define FBUILD_GENERATOR_H

#include "FbbFile.h"
#include "FbbTarget.h"
#include "../Parser/Parser.h"
#include <list>

class BuildTarget;
class FbbFile;
class FbbToolChain;

class FBuildGenerator : public Singleton<FBuildGenerator>
{
public:
	FBuildGenerator(bool quickMode);
	~FBuildGenerator();

	void AddParser(BuildParser *);
	void AddParser(EnvParser *);

	void GenTargetInfo();
	void GenFinalFbbFile();
	void GenInterFbbFile();

	// Only create one project bff file.
	// We don't need to check or sort depends, the build sequence is the same as vs    
	void RunProject(BuildParser *parser);
	void GenEntryFbbFile();

	int GetBuildState() { return buildState; };
	std::string GetCurTargetName() { return curTargetNames.size() > 0 ? curTargetNames.back() : ""; };
	std::string GetCurTargetPath() { return curTargetPaths.size() > 0 ? curTargetPaths.back() : ""; };
	std::vector<std::string> GetCurTargetNames() { return curTargetNames; };
	std::vector<std::string> GetCurTargetPaths() { return curTargetPaths; };
	void ClearCurTargetNames() { curTargetNames.clear(); };
	void ClearCurTargetPaths() { curTargetPaths.clear(); };

	inline FbbFile* GetFilePtr() const { return fbbFile; };
	inline FbbToolChain* GetToolChainPtr() const { return fbbToolChain; };
	inline bool UseQuickMode() const { return quickMode; };

	static void SortTargets(std::vector<BuildTarget*> &targets);

private:
	void GenMSVCFbbFile(BuildTarget* linkTarget);
	void GenGNUFbbFile(BuildTarget* linkTarget);

	bool CheckIfFinished();
	bool HasScanCorrect();
	bool HasAnyTarget();
	void SetBuildState(int state) { buildState = state; };

	void FreeAllTargets();

	void Run(std::list<BuildParser*> *, FASTBUILDTARGET);
	void GenObjectList(BuildParser* parser,
						std::vector<BuildTarget*> &,
						std::string targetName,
						BuildTarget* target,
						bool useQuickMode = false);     // scan compiler output to find depend obj target
	void CheckAndSperateObj(BuildTarget* target);

	bool HasTargetExist(const std::string &);
	bool HasSrcAllScaned(std::map<std::string, bool> &);
	const BuildTarget* FindTarget(const std::string &);

	BuildTarget *GetOrCreateTarget(const std::string &, FASTBUILDTARGET);
	std::string GetTargetName(const std::string &);
	std::string GetUniqueName(const std::string &);
	std::string GetOutputWithoutExt(const std::string &);
	size_t GetToolsetKey(const std::string &);

	std::list<BuildParser*> clParsers;
	std::list<BuildParser*> libParsers;
	std::list<BuildParser*> dllParsers;
	EnvParser* envParser;

	std::map<std::string, BuildTarget*> targets;        // <outputpath, BuildTarget*>
	std::map<std::string, bool> targetState;            // <targetName, bool>
	std::map<std::string, size_t> toolsetKeys;

	std::vector<BuildTarget*> sortedTargets;

	std::map<std::string, std::string> tmpPchTargets;

	std::vector<std::string> curTargetNames;
	std::vector<std::string> curTargetPaths;

	FbbFile* fbbFile;
	FbbToolChain* fbbToolChain;
	bool quickMode;
	int buildState;
};

#endif // FBUILD_GENERATOR_H
