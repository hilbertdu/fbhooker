//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbToolChain.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbToolChain.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "FbbToolChain.h"
#include "../Common/Common.h"
#include "../Template/FBTConfig.h"
#include "../Hooker/FBuildHooker.h"


// Constructor
//------------------------------------------------------------------------------
FbbToolChain::FbbToolChain() :
__default__(".Undefined = 'Undefined'"),
curVCToolsetKey("0"),
curGNUToolsetKey({ { 0, "0" }, { 1, "0" }, { 2, "0" } })
{
	// TODO: Initialize all vs install path ( use vs where utility )
	VCToolChain::SetVSWherePath(FBuildHooker::g_FBHookerPath + "\\vswhere.exe");

	VCToolChain::Init();
	NDKToolChain::Init();
}

// Destructor
//------------------------------------------------------------------------------
FbbToolChain::~FbbToolChain()
{
}

// AddToolSet
//------------------------------------------------------------------------------
void FbbToolChain::AddToolSet(int toolchainType, int buildType)
{
	DEBUG("[FbbToolChain] Add toolset: %d, %d\n", toolchainType, buildType);
	curToolsetType = toolchainType;
	curBuildType = buildType;
}

size_t FbbToolChain::GenToolsetKey(const std::string &toolsetPathVer)
{
	static size_t key = 1;
	switch (curToolsetType)
	{
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
	{
		std::map<std::string, size_t>::iterator iter = vcToolsetKeys.find(toolsetPathVer);
		if (iter == vcToolsetKeys.end())
		{
			vcToolsetKeys[toolsetPathVer] = key++;
			return key - 1;
		}
		return (*iter).second;
	}
	case ToolChain::TOOLCHAIN_TYPE::GNU:
	{
		std::map<std::string, size_t>::iterator iter = gnuToolsetKeys[curBuildType].find(toolsetPathVer);
		if (iter == gnuToolsetKeys[curBuildType].end())
		{
			gnuToolsetKeys[curBuildType][toolsetPathVer] = key++;
			return key - 1;
		}
		return (*iter).second;
	}
	default:
		return 0;
	}
	
}

// for windows MSVC
void FbbToolChain::AddPathEnv(const std::string &pathEnv)
{
	DEBUG("[FbbToolChain] Add path env: %s\n", pathEnv.c_str());
	switch (curToolsetType)
	{
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
	{
		std::string toolsetPath = VCToolChain::GetToolsetPath(pathEnv);
		std::string toolsetVer = VCToolChain::GetToolsetVersionFromPath(toolsetPath);
		std::string toolsetKey = std::to_string(GenToolsetKey(toolsetPath));

		INFO("Toolset path: %s\n", toolsetPath.c_str());
		INFO("Toolset version: %s\n", toolsetVer.c_str());
		INFO("Toolset key: %s\n", toolsetKey.c_str());

		if (vcToolsets.find(toolsetKey) == vcToolsets.end())
		{
			vcToolsets[toolsetKey].toolsetKey = toolsetKey;
			vcToolsets[toolsetKey].toolsetVer = toolsetVer;
			vcToolsets[toolsetKey].toolsetPath = toolsetPath;
		}
		curVCToolsetKey = toolsetKey;
		break;
	}
	default:
		return;
	}
}

// for linux gnu
void FbbToolChain::AddToolChainPath(const std::string &path)
{
	DEBUG("FbbToolChain: Add toolchain path: %s\n", path.c_str());
	std::string toolsetKey = std::to_string(GenToolsetKey(path));

	if (gnuToolsets.find(toolsetKey) == gnuToolsets.end())
	{
		gnuToolsets[toolsetKey].toolsetKey = toolsetKey;
		gnuToolsets[toolsetKey].buildType = curBuildType;
		gnuToolsets[toolsetKey].version = NDKToolChain::GetToolsetVersionFromPath(path);
		FileDirHelper::GetDirFromPath(gnuToolsets[toolsetKey].rootPath, path);
		FileDirHelper::GetFileNameFromPath(gnuToolsets[toolsetKey].command, path, false);

		INFO("FbbToolChain: NDK toolchain version: %s\n", gnuToolsets[toolsetKey].version.c_str());
		
		// add suffix to original path to make hook
		switch (curBuildType)
		{
		case ToolChain::BUILD_TYPE::COMPILE:
			gnuToolsets[toolsetKey].command += ".gcc";
			break;
		case ToolChain::BUILD_TYPE::STATIC_LIBRARY:
			gnuToolsets[toolsetKey].command += ".ar";
			break;
		case ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY:
			gnuToolsets[toolsetKey].command += ".g++";
			break;
		default:
			break;
		}
	}

	curGNUToolsetKey[curBuildType] = toolsetKey;
}

std::string FbbToolChain::GetCurToolsetKey(int buildType)
{
	switch (curToolsetType)
	{
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
		return curVCToolsetKey;
		break;
	case ToolChain::TOOLCHAIN_TYPE::GNU:
		return curGNUToolsetKey[buildType];
		break;
	default:
		FATALERROR("Current toolset type %d error!\n", curToolsetType);
		return "Undefined";
	}
}

std::string FbbToolChain::GetCurToolsetVer()
{
	switch (curToolsetType)
	{
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
		return vcToolsets[curVCToolsetKey].toolsetVer;
		break;
	case ToolChain::TOOLCHAIN_TYPE::GNU:
		WARNING("GNU toolchain has no toolset version!\n");
		return "Undefined";
		break;
	default:
		FATALERROR("Current toolset type %d error!\n", curToolsetType);
		return "Undefined";
	}
}

void FbbToolChain::GenerateFbb()
{
	DEBUG("[FbbToolChain] Generate total fbb: %d\n", curToolsetType);
	if (curToolsetType == ToolChain::TOOLCHAIN_TYPE::MSVC)
	{
		std::string toolsetVer = vcToolsets[curVCToolsetKey].toolsetVer;
		if (VSBaseConfig.find(toolsetVer) != VSBaseConfig.end())
		{
			if (toolsetVer == "150") // vs2017
			{
				//std::string redistVersion = VCToolChain::GetRedistVersionFromPath(vcToolsets[curVCToolsetKey].toolsetPath);
				std::string redistCrtDir = VCToolChain::GetRedistPathFromBinPath(vcToolsets[curVCToolsetKey].toolsetPath);
				DEBUG("Redist crt directory: %s\n", redistCrtDir.c_str());

				StrHelper::StrFormat(vcToolsets[curVCToolsetKey].fbbStr, VSBaseConfig[toolsetVer],
					VCToolChain::GetVSInstallDir(toolsetVer).c_str(),
					vcToolsets[curVCToolsetKey].toolsetPath.c_str(),
					VCToolChain::GetVSLangVersion(toolsetVer).c_str(),
					vcToolsets[curVCToolsetKey].toolsetKey.c_str(),
					redistCrtDir.c_str(),
					vcToolsets[curVCToolsetKey].toolsetKey.c_str()
					);
			}
			else
			{
				StrHelper::StrFormat(vcToolsets[curVCToolsetKey].fbbStr, VSBaseConfig[toolsetVer],
					VCToolChain::GetVSInstallDir(toolsetVer).c_str(),
					vcToolsets[curVCToolsetKey].toolsetPath.c_str(),
					VCToolChain::GetVSLangVersion(toolsetVer).c_str(),
					vcToolsets[curVCToolsetKey].toolsetKey.c_str(),
					vcToolsets[curVCToolsetKey].toolsetKey.c_str()
					);
			}
		}
		else
		{
			FATALERROR("Unknown toolset version %s!\n", curVCToolsetKey.c_str());
			SetBuildState(BUILD_STATE_ERROR_TOOLSETVER);
			return;
		}
	}
	else if (curToolsetType == ToolChain::TOOLCHAIN_TYPE::GNU)
	{
		std::string curKey = curGNUToolsetKey[curBuildType];
		switch (curBuildType)
		{
		case ToolChain::BUILD_TYPE::COMPILE:
		{
			DEBUG("Generate gnu compiler fbb str\n");
			StrHelper::StrFormat(gnuToolsets[curKey].fbbStr, GNUBaseConfig[curBuildType],
				gnuToolsets[curKey].toolsetKey.c_str(),
				gnuToolsets[curKey].rootPath.c_str(),
				gnuToolsets[curKey].command.c_str(),
				NDKExtraFileConfig[gnuToolsets[curKey].version],
				gnuToolsets[curKey].toolsetKey.c_str(),
				gnuToolsets[curKey].toolsetKey.c_str()
				);
			break;
		}
		case ToolChain::BUILD_TYPE::STATIC_LIBRARY:
		case ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY:
			DEBUG("Generate gnu ar/ld fbb str\n");
			StrHelper::StrFormat(gnuToolsets[curKey].fbbStr, GNUBaseConfig[curBuildType],
				gnuToolsets[curKey].toolsetKey.c_str(),
				gnuToolsets[curKey].rootPath.c_str(),
				gnuToolsets[curGNUToolsetKey[ToolChain::BUILD_TYPE::COMPILE]].toolsetKey.c_str(),
				gnuToolsets[curKey].command.c_str()
				);
			break;
		default:
			return;
		}
	}
	INFO("Generate config str finished\n");
}

const std::string & FbbToolChain::GetFbbStr(int toolchainType, const std::string &key) const
{
	switch (toolchainType)
	{
	case ToolChain::TOOLCHAIN_TYPE::MSVC:
	{
		std::map<std::string, SVCToolChain>::const_iterator iter = vcToolsets.find(key);
		ASSERT(iter != vcToolsets.end());
		return (*iter).second.fbbStr;
		break;
	}
	case ToolChain::TOOLCHAIN_TYPE::GNU:
	{
		std::map<std::string, SGNUToolChain>::const_iterator iter = gnuToolsets.find(key);
		ASSERT(iter != gnuToolsets.end());
		return (*iter).second.fbbStr;
		break;
	}
	default:
		FATALERROR("Unknown toolchain type: %d\n", toolchainType);
		return __default__;
		break;
	}
}

void FbbToolChain::DumpFbbStr()
{
}


//------------------------------------------------------------------------------