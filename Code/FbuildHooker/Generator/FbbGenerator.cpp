//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbGenerator.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbGenerator.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "FbbTarget.h"
#include "FbbGenerator.h"
#include "FbbFile.h"
#include "FbbToolChain.h"
#include "../Common/Common.h"
#include "../Hooker/FBuildHooker.h"
#include "../Template/FBTConfig.h"
#include "../Template/FBTTarget.h"
#include <sstream>
#include <fstream>
#include <algorithm>
#include <set>

// Constructor
//------------------------------------------------------------------------------
FBuildGenerator::FBuildGenerator(bool quickMode) :
quickMode(quickMode),
envParser(nullptr)
{
	fbbFile = new FbbFile();
	fbbToolChain = new FbbToolChain();
}

// DeConstructor
//------------------------------------------------------------------------------
FBuildGenerator::~FBuildGenerator()
{
	FreeAllTargets();
	SAFE_DELETE(envParser);
	SAFE_DELETE(fbbFile);
	SAFE_DELETE(fbbToolChain);
}

// AddParser
//------------------------------------------------------------------------------
void FBuildGenerator::AddParser(BuildParser *parser)
{
	switch (parser->GetBuildType())
	{
	case ToolChain::BUILD_TYPE::COMPILE:
		DEBUG("Add cl parser\n");
		clParsers.push_back(parser);
		break;
	case ToolChain::BUILD_TYPE::STATIC_LIBRARY:
		DEBUG("Add lib parser\n");
		libParsers.push_back(parser);
		break;
	case ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY:
		DEBUG("Add dll parser\n");
		dllParsers.push_back(parser);
		break;
	default:
		FATALERROR("Try to add undefine parser\n");
		return;
	}
}

// AddParser
//------------------------------------------------------------------------------
void FBuildGenerator::AddParser(EnvParser *parser)
{
	INFO("Add env parser\n");
	if (parser != envParser)
	{
		SAFE_DELETE(envParser);
		envParser = parser;
		fbbFile->SetBasePath(envParser->GetSolutionDir() + FBuildHooker::g_FBBSlnPath);
		fbbFile->InitConfigPath();
		fbbFile->GenSettingFile();
	}
}

// ScanCompilerOutput
//------------------------------------------------------------------------------
void FBuildGenerator::GenObjectList(BuildParser* parser,
	std::vector<BuildTarget*> &objectLists,
	std::string targetName,
	BuildTarget* target,
	bool useQuickMode)
{
	std::map<std::string, bool> &libSrcFiles = parser->GetSourceFiles();
	SRC_FILE_ITER libSrcIter;

	int &currentObjId = target->GetCurrentObjId();
	std::list<BuildParser*>::iterator clIter = clParsers.begin();

	while (clIter != clParsers.end())
	{
		std::string objectName = targetName + "_" + std::to_string(currentObjId);

		DEBUG("Search cl parser: %s\n", objectName.c_str());

		BuildTarget *objectTarget = new BuildTarget(objectName, OBJECTLIST);
		const std::string &outputFile = (*clIter)->GetOuputFile();

		std::set<std::string> objectList;
		const std::map<std::string, bool> &clSrcFiles = (*clIter)->GetSourceFiles();

		for (libSrcIter = libSrcFiles.begin(); libSrcIter != libSrcFiles.end(); ++libSrcIter)
		{
			if ((*libSrcIter).second)
			{
				continue;
			}

			if (outputFile.size() <= 0)
			{
				// If not defined an output file, there should be multi-source files
				DEBUG("Search cl parser file(1): %s\n", (*libSrcIter).first.c_str());
				SRC_FILE_ITER findIter = (*clIter)->FindSourceFile((*libSrcIter).first);
				if (findIter != clSrcFiles.end())
				{
					(*findIter).second = true;
					(*libSrcIter).second = true;

					objectTarget->AddSourceFile((*findIter).first);
				}
			}
			else
			{
				// If defined an output file, there should be only one source file
				DEBUG("Search cl parser file(2): %s\n", (*libSrcIter).first.c_str());
				if ((*clIter)->MatchOutputFile((*libSrcIter).first))
				{
					if (clSrcFiles.size() != 1)
					{
						FATALERROR("Error source file input, size is not 1 : %s\n", outputFile.c_str());
					}

					SRC_FILE_ITER clSrcIter = (*clIter)->GetSourceFiles().begin();
					for (; clSrcIter != (*clIter)->GetSourceFiles().end(); ++clSrcIter)
					{
						(*clSrcIter).second = true;
						(*libSrcIter).second = true;

						objectTarget->SetSourceFile((*clSrcIter).first, outputFile);
					}
				}
			}
		}

		if (objectTarget->IsValid())
		{
			DEBUG("Object target valid %s\n", objectTarget->name.c_str());

			objectTarget->SetToolChainType((*clIter)->GetToolChainType());
			objectTarget->SetNativeCmd((*clIter)->GetNativeCmd());

			objectTarget->SetOption((*clIter)->GetBuildOpts(), envParser->GetExtraOpt());
			objectTarget->SetIncludes((*clIter)->GetIncludeDirs());

			const std::string &outputPath = (*clIter)->GetOuputPath();
			const std::string &outputFile = (*clIter)->GetOuputFile();
			if (outputPath != "")
			{
				objectTarget->SetOutputPath(outputPath);
			}

			if (outputFile != "")
			{
				objectTarget->SetOutputFile(outputFile);
			}

			const std::string &pchHeader = (*clIter)->GetPchHeader();
			const std::string &pchOutput = (*clIter)->GetPchOutput();
			if (pchHeader != "" && pchOutput != "")
			{
				DEBUG("Set tmp pch target name: %s\n", objectName.c_str());
				tmpPchTargets[pchOutput] = objectName;
				objectTarget->SetPchOutput(pchOutput);
			}
			objectTarget->SetPchHeader((*clIter)->GetPchHeader());
			objectTarget->SetPchDependTarget(tmpPchTargets[pchOutput]);
			DEBUG("Set pch depend target: %s\n", tmpPchTargets[pchOutput].c_str());

			objectTarget->SetParent(target);

			if (useQuickMode)
			{
				objectTarget->EnableQuickMode();
			}
			objectTarget->SetEnvironment((*clIter)->GetEnvArray());

			objectLists.push_back(objectTarget);
			currentObjId++;
		}
		else
		{

			SAFE_DELETE(objectTarget);
		}

		// delete clparser
		if ((*clIter)->CheckIfScanFinished())
		{
			DEBUG("Scan finished cl parser %s\n", objectName.c_str());
			SAFE_DELETE((*clIter));
			clParsers.erase(clIter++);	// a trick
		}
		else
		{
			clIter++;
		}
	}
}

void FBuildGenerator::CheckAndSperateObj(BuildTarget* target)
{
	std::vector<std::string> singleSrcNames;
	std::vector<BuildTarget*> &objTargets = target->GetObjectTargets();
	std::vector<BuildTarget*>::iterator iter = objTargets.begin();
	for (; iter != objTargets.end(); ++iter)
	{
		if ((*iter)->GetOutputPrefix().size() > 0)
		{
			const std::string &filePath = (*iter)->GetSourceFiles().front();
			size_t startPos = filePath.find_last_of("\\");
			std::string fileName = filePath.substr(startPos + 1);
			DEBUG("Add single file name: %s\n", fileName.c_str());
			singleSrcNames.push_back(fileName);
		}
	}

	std::vector<std::string>::iterator nameIter = singleSrcNames.begin();
	std::vector<BuildTarget*> extraTargets;
	int &currentObjIdx = target->GetCurrentObjId();

	for (; nameIter != singleSrcNames.end(); ++nameIter)
	{
		for (iter = objTargets.begin(); iter != objTargets.end(); ++iter)
		{
			if ((*iter)->GetOutputPrefix().size() > 0)
			{
				continue;
			}

			std::string srcFilePath;
			(*iter)->FindAndRemoveSrcFile((*nameIter), srcFilePath);
			if (srcFilePath.size() > 0)
			{
				std::string objectName;
				std::string prefix;

				std::stringstream ss;
				ss << target->name;
				ss << '_';
				ss << currentObjIdx;
				ss >> objectName;

				ss.clear();
				ss << currentObjIdx;
				ss << "_";
				ss >> prefix;

				BuildTarget* target = new BuildTarget(*(*iter));
				target->name = objectName;
				target->ClearSourceFile();
				target->AddSourceFile(srcFilePath, prefix);

				extraTargets.push_back(target);

				currentObjIdx++;
				INFO("Seperate target %s\n", objectName.c_str());
			}
		}
	}

	target->AddObjectLists(extraTargets);
}

bool FBuildGenerator::HasSrcAllScaned(std::map<std::string, bool> &srcFiles)
{
	SRC_FILE_ITER iter = srcFiles.begin();
	for (; iter != srcFiles.end(); ++iter)
	{
		if (!(*iter).second)
		{
			return false;
		}
	}
	return true;
}

bool FBuildGenerator::HasTargetExist(const std::string &outputPath)
{
	if (targets.find(outputPath) != targets.end())
	{
		return true;
	}
	return false;
}

const BuildTarget* FBuildGenerator::FindTarget(const std::string &outputPath)
{
	std::string outputKey = GetOutputWithoutExt(outputPath);
	std::map<std::string, BuildTarget*>::iterator iter = targets.find(outputKey);
	if (iter != targets.end())
	{
		INFO("FindTarget: %s\n", outputKey.c_str());
		return (*iter).second;
	}
	INFO("FindTarget Null: %s\n", outputKey.c_str());
	return nullptr;
}

std::string FBuildGenerator::GetTargetName(const std::string &outputFile)
{
	// return file name
	std::string spliter = "\\/";
	size_t first = outputFile.find_last_of(spliter);
	size_t last = outputFile.find_last_of('.');
	first = first == std::string::npos ? 0 : first + 1;
	return outputFile.substr(first, last - first);
}

BuildTarget *FBuildGenerator::GetOrCreateTarget(const std::string &output, FASTBUILDTARGET type)
{
	std::map<std::string, BuildTarget*>::iterator iter;
	iter = targets.find(output);
	if (iter != targets.end())
	{
		return (*iter).second;
	}
	std::string targetName;
	FileDirHelper::GetFileNameFromPath(targetName, output, false);
	targetName = GetUniqueName(targetName);
	std::string outputKey = GetOutputWithoutExt(output);
	targets[outputKey] = new BuildTarget(targetName, type);
	targetState[targetName] = false;
	INFO("GetOrCreateTarget name: %s\n", targetName.c_str());
	INFO("GetOrCreateTarget output: %s\n", outputKey.c_str());
	return targets[outputKey];
}

std::string FBuildGenerator::GetUniqueName(const std::string &name)
{
	std::string uniqueName = name;
	size_t idx = 0;
	while (targetState.find(uniqueName) != targetState.end())
	{
		uniqueName = name + std::to_string(idx);
		idx++;
		WARNING("DuplicateRename target: %s --> %s\n", name.c_str(), uniqueName.c_str());
	}
	return uniqueName;
}

std::string FBuildGenerator::GetOutputWithoutExt(const std::string &outputPath)
{
	size_t last = outputPath.find_last_of('.');
	return outputPath.substr(0, last);
}

size_t FBuildGenerator::GetToolsetKey(const std::string &toolsetPath)
{
	static size_t key = 1;
	std::map<std::string, size_t>::iterator iter = toolsetKeys.find(toolsetPath);
	if (iter == toolsetKeys.end())
	{
		toolsetKeys[toolsetPath] = key++;
		return key - 1;
	}
	return (*iter).second;
}

void FBuildGenerator::FreeAllTargets()
{
	std::list<BuildParser*>::iterator clIter = clParsers.begin();
	std::list<BuildParser*>::iterator libIter = libParsers.begin();
	std::list<BuildParser*>::iterator dllIter = dllParsers.begin();
	std::map<std::string, BuildTarget*>::iterator targetIter = targets.begin();

	for (; clIter != clParsers.end(); ++clIter)
	{
		SAFE_DELETE(*clIter);
	}
	for (; libIter != libParsers.end(); ++libIter)
	{
		SAFE_DELETE(*libIter);
	}
	for (; dllIter != dllParsers.end(); ++dllIter)
	{
		SAFE_DELETE(*dllIter);
	}
	for (; targetIter != targets.end(); ++targetIter)
	{
		SAFE_DELETE((*targetIter).second);
	}
	clParsers.clear();
	libParsers.clear();
	dllParsers.clear();
	targets.clear();
	sortedTargets.clear();
}

/*static*/ void FBuildGenerator::SortTargets(std::vector<BuildTarget*> &targets)
{
	INFO("Began sorting targets...\n");

	// Sort algrithom, currently we don't check cycle state.
	std::sort(targets.begin(), targets.end(),
		[](BuildTarget* a, BuildTarget* b)
	{
		return a->name < b->name;
	});

	std::vector<BuildTarget*>::iterator findFirstIter = targets.begin();
	std::vector<BuildTarget*>::iterator replaceIter;
	bool hasFindTop = false;

	while (findFirstIter != targets.end())
	{
		hasFindTop = true;
		for (replaceIter = findFirstIter; replaceIter != targets.end(); ++replaceIter)
		{
			if ((*findFirstIter)->IsDependsOnTarget((*replaceIter)->name))
			{
				DEBUG("Replace target %s %s\n", (*findFirstIter)->name.c_str(), (*replaceIter)->name.c_str());
				std::swap((*findFirstIter), (*replaceIter));
				hasFindTop = false;
				break;
			}
		}
		if (hasFindTop)
		{
			findFirstIter++;
		}
	}
}

bool FBuildGenerator::CheckIfFinished()
{
	bool state = true;
	if (clParsers.size() > 0)
	{
		state = false;
		FATALERROR("Still have cl parsers to be check!\n");
		std::list<BuildParser*>::iterator iter = clParsers.begin();
		for (; iter != clParsers.end(); ++iter)
		{
			FATALERROR("Please check cl parser %s %s\n", (*iter)->GetOuputFile().c_str(), (*iter)->GetOuputPath().c_str());
		}
	}
	if (libParsers.size() > 0)
	{
		state = false;
		FATALERROR("Still have lib parsers to be check!\n");
		std::list<BuildParser*>::iterator iter = libParsers.begin();
		for (; iter != libParsers.end(); ++iter)
		{
			FATALERROR("Please check lib parser %s\n", (*iter)->GetOuputFile().c_str());
		}
	}
	if (dllParsers.size() > 0)
	{
		state = false;
		FATALERROR("Still have dll parsers to be check!\n");
		std::list<BuildParser*>::iterator iter = dllParsers.begin();
		for (; iter != dllParsers.end(); ++iter)
		{
			FATALERROR("Please check dll parser %s\n", (*iter)->GetOuputFile().c_str());
		}
	}
	return state;
}

bool FBuildGenerator::HasScanCorrect()
{
	std::list<BuildParser*>::iterator libIter = libParsers.begin();
	std::list<BuildParser*>::iterator dllIter = dllParsers.begin();

	for (; libIter != libParsers.end(); ++libIter)
	{
		if (!(*libIter)->CheckIfScanFinished())
		{
			return false;
		}
	}

	for (; dllIter != dllParsers.end(); ++dllIter)
	{
		if (!(*dllIter)->CheckIfScanFinished())
		{
			return false;
		}
	}
	return true;
}

bool FBuildGenerator::HasAnyTarget()
{
	return sortedTargets.size() > 0;
}

void FBuildGenerator::RunProject(BuildParser *parser)
{
	std::string targetPath = parser->GetOuputFile();
	BuildTarget *linkTarget = GetOrCreateTarget(targetPath, (FASTBUILDTARGET)parser->GetBuildType());
	std::vector<BuildTarget*> objectLists;

	DEBUG("Generate project object list begin\n");
	GenObjectList(parser, objectLists, linkTarget->name, linkTarget, quickMode);
	DEBUG("Generate project object list end\n");

	if (quickMode)
	{
		// If use quick mode, environment should be set into target scope
		linkTarget->EnableQuickMode();
	}
	linkTarget->SetEnvironment(parser->GetEnvArray());

	// May be empty but ok under vs increment mode
	linkTarget->AddObjectLists(objectLists);
	std::map<std::string, bool> &inputFileMap = parser->GetSourceFiles();
	std::map<std::string, bool>::iterator iter = inputFileMap.begin();
	for (; iter != inputFileMap.end(); ++iter)
	{
		if (!(*iter).second)
		{
			linkTarget->AddExtraObjFile((*iter).first);
		}
	}

	linkTarget->SetToolChainType(parser->GetToolChainType());
	linkTarget->SetNativeCmd(parser->GetNativeCmd());

	linkTarget->SetOption(parser->GetBuildOpts(), envParser->GetExtraOpt());
	linkTarget->SetOutputFile(parser->GetOuputFile());

	if (parser->GetOuputPath() != "")
	{
		linkTarget->SetOutputPath(parser->GetOuputPath());
	}
	else
	{
		linkTarget->SetOutputPath(parser->GetCurrentDir());
	}
	std::vector<std::string> depends = parser->GetDependency();

	for (size_t i = 0; i < depends.size(); ++i)
	{
		DEBUG("Check Depend: %s\n", depends[i].c_str());
		if (quickMode)
		{
			// Find in the real generate targets
			const BuildTarget* dependTarget = FindTarget(depends[i]);
			if (dependTarget)
			{
				linkTarget->AddDependency(dependTarget->name, dependTarget->GetTargetType());
			}
			else
			{
				// Can't find dep target, consider it as a external depend
				linkTarget->AddDependency(depends[i], EXTERNAL);
			}
		}
		else
		{
			// We don't need any dependency info under vs increment mode
			linkTarget->AddDependency(depends[i], EXTERNAL);
		}
		linkTarget->AddDependencyPath(depends[i]);
	}
	linkTarget->SetOtherTokens(parser->GetOtherTokens());
	linkTarget->SetLibraryPath(parser->GetEnvLibPath());
	linkTarget->SetAdditional(parser->GetAdditionalInputs());	
	linkTarget->SetReferences(envParser->GetProjectInfo().GetReferences(linkTarget->GetOutputFile()));	// TODO use depends as references

	std::string tlogLocation = envParser->GetProjectInfo().GetTlogPath(linkTarget->GetOutputFile());
	if (tlogLocation.empty())
	{
		// TODO use default tlog dir or not
		INFO("Tlog location is empty\n");
	}
	else
	{
		linkTarget->SetTlogPath(tlogLocation);
	}

	if (parser->GetToolChainType() == ToolChain::TOOLCHAIN_TYPE::MSVC)
	{
		GenMSVCFbbFile(linkTarget);
	}
	else if (parser->GetToolChainType() == ToolChain::TOOLCHAIN_TYPE::GNU)
	{
		GenGNUFbbFile(linkTarget);
	}
	SAFE_DELETE(parser);

	if (quickMode)
	{
		sortedTargets.push_back(linkTarget);
	}
	else
	{
		// Delete target
		SAFE_DELETE(linkTarget);
		targets.erase(targetPath);
	}
}

void FBuildGenerator::GenMSVCFbbFile(BuildTarget* linkTarget)
{
	// Just Write config and target bff into {project}.bff file
	// without any cache or increment mechanism.
	INFO("Gen msvc project fbb file: %s\n", linkTarget->name.c_str());

	ProjectParser &projInfo = envParser->GetProjectInfo();
	std::string platform = projInfo.GetPlatform(linkTarget->GetOutputFile());
	std::string configuration = projInfo.GetConfiguration(linkTarget->GetOutputFile());

	curTargetNames.push_back(linkTarget->name);
	curTargetPaths.push_back(linkTarget->GetOutputFile());
	std::string fbTargetDir = linkTarget->name + "-" + platform + "-" + configuration;

	// Genereate object list toolchain
	std::vector<BuildTarget*>::const_iterator objIter = linkTarget->GetObjectTargets().begin();
	for (; objIter != linkTarget->GetObjectTargets().end(); ++objIter)
	{
		fbbToolChain->AddToolSet((*objIter)->GetToolChainType(), (*objIter)->GetTargetType());
		fbbToolChain->AddPathEnv(Env::GetEnvVariable("PATH", (*objIter)->GetEnvironment()));
		fbbToolChain->GenerateFbb();

		(*objIter)->SetToolsetKey(fbbToolChain->GetCurToolsetKey());
		(*objIter)->SetToolsetVer(fbbToolChain->GetCurToolsetVer());
	}

	fbbToolChain->AddToolSet(linkTarget->GetToolChainType(), linkTarget->GetTargetType());
	fbbToolChain->AddPathEnv(Env::GetEnvVariable("PATH", linkTarget->GetEnvironment()));
	fbbToolChain->GenerateFbb();
	linkTarget->SetToolsetKey(fbbToolChain->GetCurToolsetKey());
	linkTarget->SetToolsetVer(fbbToolChain->GetCurToolsetVer());

	// Generate fbb str
	std::string fbuildStr;

	// Generate include str
	std::string toolsetKey = fbbToolChain->GetCurToolsetKey();
	std::string toolsetVer = fbbToolChain->GetCurToolsetVer();
	if (quickMode == false)
	{
		fbuildStr += "#include \"..\\Config\\fbconfig_vc_" + toolsetKey + ".bff\"\n\n";
	}

	// Generate object list fbb str
	for (objIter = linkTarget->GetObjectTargets().begin(); objIter != linkTarget->GetObjectTargets().end(); ++objIter)
	{
		(*objIter)->GenerateFbb();
		fbuildStr += (*objIter)->GetFbbStr() + "\n";
	}
	linkTarget->GenerateFbb();
	fbuildStr += linkTarget->GetFbbStr() + "\n\n";

	fbbFile->GenToolsetConfigFile("vc_" + toolsetKey, fbbToolChain->GetFbbStr(ToolChain::TOOLCHAIN_TYPE::MSVC, toolsetKey));
	fbbFile->GenFbbProjectFile(linkTarget->name, fbTargetDir, fbuildStr);

	SetBuildState(fbbFile->GetState());
}

void FBuildGenerator::GenGNUFbbFile(BuildTarget* linkTarget)
{
	// TODO: Generate gnu project fbb file
	INFO("Gen gnu project fbb file: %s\n", linkTarget->name.c_str());

	ProjectParser &projInfo = envParser->GetProjectInfo();
	std::string platform = projInfo.GetPlatform(linkTarget->GetOutputFile());
	std::string configuration = projInfo.GetConfiguration(linkTarget->GetOutputFile());

	curTargetNames.push_back(linkTarget->name);
	curTargetPaths.push_back(linkTarget->GetOutputFile());
	std::string fbTargetDir = linkTarget->name + "-" + platform + "-" + configuration;

	// Genereate object list toolchain
	std::vector<BuildTarget*>::const_iterator objIter = linkTarget->GetObjectTargets().begin();
	for (; objIter != linkTarget->GetObjectTargets().end(); ++objIter)
	{
		fbbToolChain->AddToolSet((*objIter)->GetToolChainType(), (*objIter)->GetTargetType());
		fbbToolChain->AddToolChainPath((*objIter)->GetNativeCmd());
		fbbToolChain->GenerateFbb();

		std::string key = fbbToolChain->GetCurToolsetKey((*objIter)->GetTargetType());
		(*objIter)->SetToolsetKey(key);
		fbbFile->GenToolsetConfigFile("gnu_compiler_" + key, fbbToolChain->GetFbbStr(ToolChain::TOOLCHAIN_TYPE::GNU, key));
	}

	fbbToolChain->AddToolSet(linkTarget->GetToolChainType(), linkTarget->GetTargetType());
	fbbToolChain->AddToolChainPath(linkTarget->GetNativeCmd());
	fbbToolChain->GenerateFbb();
	linkTarget->SetToolsetKey(fbbToolChain->GetCurToolsetKey(linkTarget->GetTargetType()));

	// Generate fbuild str
	std::string fbuildStr;

	// Generate include str
	if (quickMode == false)
	{
		std::set<std::string> toolsetKeys;
		for (objIter = linkTarget->GetObjectTargets().begin(); objIter != linkTarget->GetObjectTargets().end(); ++objIter)
		{
			std::string key = (*objIter)->GetToolsetKey();
			if (toolsetKeys.find(key) == toolsetKeys.end())
			{
				switch ((*objIter)->GetTargetType())
				{
				case ToolChain::BUILD_TYPE::COMPILE:
					fbuildStr += "#include \"..\\Config\\fbconfig_gnu_compiler_" + key + ".bff\"\n";
					break;
				default:
					FATALERROR("Target build type %d error!", (*objIter)->GetTargetType());
					break;
				}
				toolsetKeys.insert(key);
			}
		}
	}
	std::string key = linkTarget->GetToolsetKey();
	switch (linkTarget->GetTargetType())
	{
	case ToolChain::BUILD_TYPE::STATIC_LIBRARY:
		fbbFile->GenToolsetConfigFile("gnu_librarian_" + key, fbbToolChain->GetFbbStr(ToolChain::TOOLCHAIN_TYPE::GNU, key));
		if (quickMode == false)
		{
			fbuildStr += "#include \"..\\Config\\fbconfig_gnu_librarian_" + key + ".bff\"\n";
		}
		break;
	case ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY:
		fbbFile->GenToolsetConfigFile("gnu_linker_" + key, fbbToolChain->GetFbbStr(ToolChain::TOOLCHAIN_TYPE::GNU, key));
		if (quickMode == false)
		{
			fbuildStr += "#include \"..\\Config\\fbconfig_gnu_linker_" + key + ".bff\"\n";
		}
		break;
	default:
		FATALERROR("Target build type %d error!", linkTarget->GetTargetType());
		break;
	}


	// Generate object list fbb str
	for (objIter = linkTarget->GetObjectTargets().begin(); objIter != linkTarget->GetObjectTargets().end(); ++objIter)
	{
		(*objIter)->GenerateFbb();
		fbuildStr += (*objIter)->GetFbbStr() + "\n";
	}

	linkTarget->GenerateFbb();
	fbuildStr += linkTarget->GetFbbStr() + "\n\n";

	fbbFile->GenFbbProjectFile(linkTarget->name, fbTargetDir, fbuildStr);

	SetBuildState(fbbFile->GetState());
}

void FBuildGenerator::GenEntryFbbFile()
{
	ProjectParser &projInfo = envParser->GetProjectInfo();

	INFO("Gen entry fbb file\n");

	// Sort targets
	DEBUG("GenEntryFbbFile target size: %d\n", sortedTargets.size());

	if (sortedTargets.size() == 0)
	{
		INFO("No targets need to build\n");
		SetBuildState(BUILD_STATE_NO_TARGET);
		return;
	}

	std::vector<std::string> targetDirs, targetNames, projectNames;
	for (size_t i = 0; i < sortedTargets.size(); ++i)
	{
		std::string platform = projInfo.GetPlatform(sortedTargets[i]->GetOutputFile());
		std::string configuration = projInfo.GetConfiguration(sortedTargets[i]->GetOutputFile());

		targetDirs.push_back(sortedTargets[i]->name + "-" + platform + "-" + configuration);
		targetNames.push_back(sortedTargets[i]->name);

		projectNames.push_back(projInfo.GetProjectName(sortedTargets[i]->GetOutputFile()));
	}
	fbbFile->GenFbbMainFile(targetDirs, targetNames);
	SetBuildState(fbbFile->GetState());

	// Delete all targets
	FreeAllTargets();
}
