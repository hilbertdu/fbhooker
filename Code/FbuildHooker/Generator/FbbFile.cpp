//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FbbFile.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FbbFile.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "FbbFile.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "FbbTarget.h"
#include "FbbGenerator.h"
#include "../Common/Common.h"
#include "../Template/FBTTarget.h"
#include "../Template/FBTSettings.h"

// Constructor
//------------------------------------------------------------------------------
FbbFile::FbbFile() :
curFBuildIdx(0),
curConfigIdx(0),
fbbConfName("fbconfig.bff"),
fbbFileName("fbuild.bff")
{
}

// Deconstructor
//------------------------------------------------------------------------------
FbbFile::~FbbFile()
{
}

// InitConfigPath
//-----------------------------------------------------------------------------
void FbbFile::InitConfigPath()
{
	FileDirHelper::DirectoryDelete(fbbPath + "\\Config");
	FileDirHelper::DirectoryCreate(fbbPath + "\\Config");
}

// GenFbbProjectFile
//-----------------------------------------------------------------------------
void FbbFile::GenSettingFile()
{
	std::string settings = "";
	StrHelper::StrFormat(settings, FBVSSettings, "");

	std::string filePath = fbbPath + "\\Config\\fbsettings.bff";
	std::ofstream file(filePath);
	if (!file)
	{
		genFileState = BUILD_STATE_ERROR_WRITE_FILE;
		FATALERROR("Can't Open file %s\n", filePath.c_str());
		return;
	}
	file << settings << std::endl;
	file.close();
	configList.push_back("Config\\fbsettings.bff");
}

// GenToolsetConfigFile
//----------------------------------------------------------------------------
void FbbFile::GenToolsetConfigFile(const std::string &uniSuffix, const std::string &toolsetConfStr)
{
	std::string relPath = "Config\\fbconfig_" + uniSuffix + ".bff";
	std::string filePath = fbbPath + "\\" + relPath;

	INFO("GenToolsetConfigFile: %s\n", filePath.c_str());

	if (FileDirHelper::FileExists(filePath))
	{
		INFO("Config file already exist\n");
		return;
	}
	std::ofstream file(filePath);
	if (!file)
	{
		genFileState = BUILD_STATE_ERROR_WRITE_FILE;
		FATALERROR("Can't Open file %s\n", filePath.c_str());
		return;
	}
	file << toolsetConfStr << std::endl;
	file.close();
	configList.push_back(relPath);
}

// GenFbbProjectFile
//------------------------------------------------------------------------------
void FbbFile::GenFbbProjectFile(const std::string &name, const std::string &dir, const std::string &fbbStr)
{
	INFO("Gen project file dir: %s\n", dir.c_str());
	std::string curFbbFileDir = fbbPath + "\\" + dir;

	FileDirHelper::DirectoryCreate(curFbbFileDir);

	curFbbFileName = dir;
	curFbbFilePath = curFbbFileDir + "\\fbuild.bff";

	std::ofstream file(curFbbFilePath);
	if (!file)
	{
		genFileState = BUILD_STATE_ERROR_WRITE_FILE;
		FATALERROR("Can't Open file %s\n", curFbbFilePath.c_str());
		return;
	}
	file << fbbStr << std::endl;
	file.close();

	// Generate targets file
	std::ofstream targetFs(fbbPath + "\\targets.bff", std::ios::app);
	if (!targetFs)
	{
		FATALERROR("Can't Open file %s\n", (fbbPath + "\\targets.bff").c_str());
		return;
	}
	targetFs << name << std::endl;
	targetFs.close();
	INFO("Create targets file OK!\n");
}

// GenFbbMainFile
//-----------------------------------------------------------------------------
void FbbFile::GenFbbMainFile(const std::vector<std::string> &dirs, const std::vector<std::string> &names)
{
	std::string filePath = fbbPath + '\\' + fbbFileName;
	std::string includeStr, fbbStr, targetsStr;

	for (size_t i = 0; i < configList.size(); ++i)
	{
		includeStr += "#include \"" + configList[i] + "\"\n";
	}

	for (size_t i = 0; i < dirs.size(); ++i)
	{
		includeStr += "#include \"" + dirs[i] + "\\" + fbbFileName + "\"\n";
	}
	StrHelper::StrJoin(fbbStr, names, ",", "\"");
	StrHelper::StrFormat(targetsStr, FBTargets, fbbStr.c_str());

	std::ofstream file(filePath);
	if (!file)
	{
		genFileState = BUILD_STATE_ERROR_WRITE_FILE;
		FATALERROR("Can't Open file %s\n", filePath.c_str());
		return;
	}
	file << (includeStr + targetsStr);
	file.close();

	INFO("Create fbuild main file OK!\n");
}

//------------------------------------------------------------------------------
