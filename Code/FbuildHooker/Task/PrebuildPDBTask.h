//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: PrebuildPDBTask.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/01/03
//*******************************************************

// PrebuildPDBTask.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_PDBTASK_H
#define FBUILD_PDBTASK_H

// Includes
//------------------------------------------------------------------------------
#include <string>
#include <vector>

static std::string prebuildSrcName = "EmptySource";
static std::string prebuildCommand = "cl.exe";

class PrebuildPDBTask
{
public:
	PrebuildPDBTask(const std::string & commandPaths,
					const std::vector<std::string> & options,
					const std::string & outputPath,
					const std::string & projectDir);

	bool DoBuild();

private:
	void GetFullArgs(std::string & args);
	void GetCmdPath(std::string & cmdPath, const std::string & envPaths);

	std::string commandPaths;
	std::string outputDir;
	std::string workingDir;
	std::vector<std::string> options;
};

#endif // FBUILD_PDBTASK_H