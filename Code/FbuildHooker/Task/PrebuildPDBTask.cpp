//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: PrebuildPDBTask.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/01/03
//*******************************************************

// PrebuildPDBTask.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "PrebuildPDBTask.h"
#include "../Hooker/FBuildHooker.h"
#include "../Common/Common.h"

PrebuildPDBTask::PrebuildPDBTask(const std::string & commandPaths,
	const std::vector<std::string> & options,
	const std::string & outputPath,
	const std::string & projectDir) :
	commandPaths(commandPaths),
	options(options),
	workingDir(projectDir)
{
	INFO("Command Paths: %s\n", commandPaths.c_str());
	FileDirHelper::GetDirFromPath(outputDir, outputPath);
}

void PrebuildPDBTask::GetFullArgs(std::string & args)
{
	std::string optionStr;
	for (size_t i = 0; i < options.size(); ++i)
	{
		if (StrHelper::StrBeginWithI(options[i], "/Yu"))
		{
			continue;
		}
		optionStr += options[i] + " ";
	}
	INFO("Options: %s\n", optionStr.c_str());

	args += optionStr;
	args += FBuildHooker::g_FBHookerPath + NATIVE_SLASH_STR + prebuildSrcName + ".cpp ";
	args += "/c /Fo\"" + outputDir + NATIVE_SLASH_STR + prebuildSrcName + ".obj\" ";
}

void PrebuildPDBTask::GetCmdPath(std::string & path, const std::string & envPaths)
{
	std::vector<std::string> paths;
	StrHelper::StrSplit(envPaths, ';', paths, '"');

	for (size_t i = 0; i < paths.size(); ++i)
	{
		INFO("Search cmd path: %s\n", paths[i].c_str());
		std::string exePath = paths[i] + NATIVE_SLASH_STR + prebuildCommand;
		if (FileDirHelper::FileExists(exePath))
		{
			path = exePath;
			return;
		}
	}
	path = "";
}

bool PrebuildPDBTask::DoBuild()
{
	std::string exePath = "";
	std::string args = "";
	GetFullArgs(args);
	GetCmdPath(exePath, commandPaths);

	INFO("Prebuild PDB full command path: %s\n", exePath.c_str());
	INFO("Prebuild PDB full command args: %s\n", args.c_str());

	Process proc;
#ifdef SHOW_INFO
	int ret = proc.Spawn(exePath.c_str(), args.c_str(), workingDir.c_str(), nullptr, true);
#else
	int ret = proc.Spawn(exePath.c_str(), args.c_str(), workingDir.c_str(), nullptr, false);
#endif
	if (ret == PROCESS_CREATE_OK)
	{
		int exitState = proc.WaitForExit();
		if (exitState == 0)
		{
			INFO("Prebuild PDB ok\n");
			FileDirHelper::FileDelete(outputDir + NATIVE_SLASH_STR + prebuildSrcName + ".obj");
			return true;
		}
		else
		{
			FATALERROR("Prebuild PDB failed\n");
			return false;
		}
	}
	else
	{
		FATALERROR("Can not spwan prebuild pdb process!\n");
		return false;
	}
}
