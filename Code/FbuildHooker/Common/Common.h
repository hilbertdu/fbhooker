//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Macros.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Macros.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_COMMON_H
#define FBUILD_COMMON_H

#include "Core/Utils/Macros.h"
#include "Core/Utils/Utils.h"
#include "Core/Logger/Tracing.h"
#include "Core/Env/Environment.h"
#include "Core/Process/Process.h"
#include "Core/Process/SharedMemory.h"
#include "Core/Process/SystemMutex.h"
#include "Core/Process/Thread.h"
#include "Core/Helper/DumpHelper.h"
#include "Core/Container/Singleton.h"
#include "Core/ToolChain/ToolChain.h"

// Macros
//------------------------------------------------------------------------------
#define LOCK_BUILD_TRY_COUNT	100
#define LOCK_FINAL_TRY_COUNT	10
#define LOCK_MEM_TRY_COUNT		20

// Command type
//------------------------------------------------------------------------------
enum COMMAND_TYPE
{
	BUILD_TOOLCHAIN,
	FB_SERVICE_START,
	FB_SERVICE_START_QUICK,
	FB_SERVICE_INTER_BUILD,
	FB_SERVICE_EXIT,
	UNDEFINED
};

// State code
enum BUILD_STATES
{
	BUILD_STATE_OK = 0,
	BUILD_STATE_ERROR = -1,

	BUILD_STATE_NO_TARGET = 0x0100,
	BUILD_STATE_NOT_BEGIN = 0x0101,
	BUILD_STATE_CANCEL = 0x0201,

	BUILD_STATE_ERROR_PARSE_CMD = 0x0102,
	BUILD_STATE_ERROR_NOT_FINISH = 0x0103,
	BUILD_STATE_ERROR_SCAN_ERROR = 0x0103,

	BUILD_STATE_ERROR_READ_FILE = 0x0104,
	BUILD_STATE_ERROR_WRITE_FILE = 0x0104,

	BUILD_STATE_ERROR_FASTBUILD = 0x0105,
	BUILD_STATE_ERROR_TOOLSETVER = 0x0107,
	BUILD_STATE_NOT_FINISHED = 0x108,
	BUILD_STATE_ERROR_VS = 0x109,
	BUILD_STATE_ERROR_UNDEFINE = 0x0110,
};

#endif // FBUILD_COMMON_H
