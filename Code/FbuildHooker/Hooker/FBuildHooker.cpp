//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBuildHooker.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/28
//*******************************************************

// FBuildHooker.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "FBuildHooker.h"
#include "../Common/Common.h"
#include "../Job/Job.h"
#include <fstream>
#include <string.h>

#if defined( __WINDOWS__ )
#include <codecvt>
#include <locale>
#endif

#if defined( __LINUX__ ) || defined ( __APPLE__ )
#define _stricmp strcasecmp
#endif

// Static data
//------------------------------------------------------------------------------
/*static*/ const char * FBuildHooker::g_StartProcKey = "Global\\FBOneProcStartMutex_xxxx";
/*static*/ const char * FBuildHooker::g_FinalProcKey = "Global\\FBOneProcFinalMutex_xxxx";
/*static*/ const char * FBuildHooker::g_SyncSharedKey = "Global\\FBSyncMutex_xxxx";
/*static*/ SystemMutex FBuildHooker::g_OneProcStartMutex = SystemMutex(g_StartProcKey, FOR_ONE_PROCESS);
/*static*/ SystemMutex FBuildHooker::g_OneProcFinalMutex = SystemMutex(g_FinalProcKey, FOR_ONE_PROCESS);
/*static*/ SystemMutex FBuildHooker::g_SyncSharedMutex = SystemMutex(g_SyncSharedKey, FOR_SYNC_PROCESS);

/*static*/ std::string FBuildHooker::g_FBHookerPath = "C:\\Fastbuild";
/*static*/ std::string FBuildHooker::g_FBBSlnPath = "\\.fbuild";

// Constructor
//------------------------------------------------------------------------------
FBuildHooker::FBuildHooker()
{
}

// Deconstructor
//------------------------------------------------------------------------------
FBuildHooker::~FBuildHooker()
{
}

// GetCommandType
//------------------------------------------------------------------------------
/*static*/ COMMAND_TYPE FBuildHooker::GetCommandType(const std::string &cmd, const char *arg)
{
	if (0 == _stricmp(cmd.c_str(), "cl") ||
		0 == _stricmp(cmd.c_str(), "lib") ||
		0 == _stricmp(cmd.c_str(), "link"))
	{
		return BUILD_TOOLCHAIN;
	}
	else if (0 == _stricmp(cmd.c_str(), "gcc") ||
		0 == _stricmp(cmd.c_str(), "ar") ||
		0 == _stricmp(cmd.c_str(), "g++"))
	{
		return BUILD_TOOLCHAIN;
	}
	else if (0 == _stricmp(cmd.c_str(), "arm-linux-androideabi-gcc") ||
		0 == _stricmp(cmd.c_str(), "arm-linux-androideabi-ar") ||
		0 == _stricmp(cmd.c_str(), "arm-linux-androideabi-g++"))
	{
		return BUILD_TOOLCHAIN;
	}
	else if (0 == _stricmp(cmd.c_str(), "fbservice") ||
		0 == _stricmp(cmd.c_str(), "fbuildhooker"))
	{
		if (0 == _stricmp(arg, "-exit"))
		{
			return FB_SERVICE_EXIT;
		}
		else if (0 == _stricmp(arg, "-start"))
		{
			return FB_SERVICE_START;
		}
		else if (0 == _stricmp(arg, "-startquick"))
		{
			return FB_SERVICE_START_QUICK;
		}
		else if (0 == _stricmp(arg, "-interbuild"))
		{
			return FB_SERVICE_INTER_BUILD;
		}
	}
	return UNDEFINED;
}

// InitCommands
//------------------------------------------------------------------------------
void FBuildHooker::InitCommands(int argc, char **argv)
{
	DEBUG("Init commands: %d\n", argc);
	CHECK_CONDITION(argc >= 1);

	FileDirHelper::GetFileNameFromPath(cmdExec, argv[0], false);
	cmdType = GetCommandType(cmdExec, argc > 1 ? argv[1] : "");

	for (int i = 0; i < argc; ++i)
	{
		std::string filePath = argv[i];
		if (filePath.front() != '@')
		{
			commandOption.append("\n");
			commandOption.append(argv[i]);
			continue;
		}

#if defined( __WINDOWS__ )
		filePath.erase(filePath.begin());
		std::wifstream fin(filePath, std::ios::binary);
		if (!fin.is_open())
		{
			FATALERROR("File doesn't exist %s\n", filePath.c_str());
			continue;
		}
		// Imbue the file stream with a codecvt facet that uses UTF-16 as the external multibyte encoding
		fin.imbue(std::locale(fin.getloc(), new std::codecvt_utf16<wchar_t, 0xffff, std::consume_header>));

		std::wstring sLine;
		std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> convert;
		while (getline(fin, sLine))
		{
			commandOption.append("\n");
			commandOption.append(convert.to_bytes(sLine));
		}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		commandOption.append(argv[i]);
#endif
	}
}

// DoService
//------------------------------------------------------------------------------
int FBuildHooker::DoService()
{
	if (cmdType == UNDEFINED)
	{
		FATALERROR("Undefine type! %s\n", cmdExec.c_str());
		return 0;
	}

	DEBUG("Proc start: %d\n", Process::GetCurrentId());

	Job *fbJob;
	if (cmdType == FB_SERVICE_START || cmdType == FB_SERVICE_START_QUICK)
	{
		if (g_OneProcStartMutex.TryLock() == false)
		{
			FATALERROR("A FBService is already running!\n");
			return PROCESS_CREATE_EXIST;
		}
		fbJob = new BuildGeneratorJob(commandOption, cmdType == FB_SERVICE_START_QUICK);
		fbJob->DoJob();
	}
	else if (cmdType == FB_SERVICE_EXIT || cmdType == FB_SERVICE_INTER_BUILD)
	{
		if (g_OneProcFinalMutex.TryLock() == false)
		{
			FATALERROR("A FBServiceExit is already running!\n");
			return PROCESS_CREATE_EXIST;
		}

		int tryCount = 0;
		while (!g_OneProcStartMutex.IsLocked() && tryCount++ < LOCK_FINAL_TRY_COUNT)
		{
			Thread::Sleep(100);
		}
		if (!g_OneProcStartMutex.IsLocked())
		{
			FATALERROR("No FBService is running!\n");
			return PROCESS_CREATE_ERROR;
		}
		fbJob = new FinalEventJob(commandOption, cmdType == FB_SERVICE_INTER_BUILD);
		fbJob->DoJob();
	}
	else
	{
		size_t tryCount = 0;
		while (!g_OneProcStartMutex.IsLocked() && tryCount++ < LOCK_BUILD_TRY_COUNT)
		{
			Thread::Sleep(100);
		}
		if (!g_OneProcStartMutex.IsLocked())
		{
			FATALERROR("No FBStart is running!\n");
			return PROCESS_CREATE_ERROR;
		}
		fbJob = new BuildHookerJob(commandOption, cmdType);
		fbJob->DoJob();
	}
	int retCode = fbJob->GetJobExitState();
	INFO("Fb proc exit: %s, exit state: %d\n", cmdExec.c_str(), retCode);
	return retCode;
}

// InitLogger
//------------------------------------------------------------------------------
void FBuildHooker::InitLogger()
{
#ifdef FASTBUILD_SERVICE
#define DUMP_OPEN
	if (cmdType == FB_SERVICE_START || cmdType == FB_SERVICE_START_QUICK)
	{
		Tracing::outputToFile = true;
		Tracing::logFilePath = FileDirHelper::GetCurrentDir() + g_FBBSlnPath + "\\.tmp\\lastfbuild.log";
	}
	Tracing::showInfo = true;
#endif

#ifdef FASTBUILD_HOOKER1
	Tracing::showInfo = true;
#endif

#ifdef _DEBUG
	Tracing::showInfo = true;
	Tracing::showDebug = true;
#endif

	Tracing::Init();
}

// FinalizeLogger
//------------------------------------------------------------------------------
void FBuildHooker::FinalizeLogger()
{
	Tracing::Finalize();
}
