//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBuildHooker.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/28
//*******************************************************

// FBuildHooker.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_HOOKER_H
#define FBUILD_HOOKER_H

// Includes
//------------------------------------------------------------------------------
#include "../Common/Common.h"

class FBuildHooker : public Singleton<FBuildHooker>
{
public:
	FBuildHooker();
	~FBuildHooker();

	static COMMAND_TYPE GetCommandType(const std::string &cmd, const char *arg);

	void InitLogger();
	void FinalizeLogger();

	void InitCommands(int argc, char **argv);
	int DoService();

	// Static Data
	static const char * g_StartProcKey;
	static const char * g_FinalProcKey;
	static const char * g_SyncSharedKey;
	static SystemMutex g_OneProcStartMutex;
	static SystemMutex g_OneProcFinalMutex;
	static SystemMutex g_SyncSharedMutex;

	static std::string g_FBHookerPath;
	static std::string g_FBBSlnPath;

private:
	std::string cmdExec;
	std::string commandOption;
	COMMAND_TYPE cmdType;
};

#endif	//	FBUILD_HOOKER_H
