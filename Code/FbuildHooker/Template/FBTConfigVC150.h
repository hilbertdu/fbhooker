//*******************************************************
// Copyright (c) 2017/04/04 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTConfigv150.h
// Contact : hilbertdu@corp.netease.com
// Date: 2017/04/04
//*******************************************************

// FBTConfigV150.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_CONFIG_VC150_H
#define FBUILD_TEMPLATE_CONFIG_VC150_H

static const char * FBVSBaseConfigVC150 =
"\n\
;-------------------------------------------------------------------------------\n\
; Windows Platform\n\
; ------------------------------------------------------------------------------\n\
\n\
.VSBasePath			= '%s'\n\
.VSToolBinPath		= '%s'\n\
.VSLangVersion		= '%s'\n\
.VSBuildToolKey		= '%s'\n\
.RedistCrtDir		= '%s'\n\
\n\
; ------------------------------------------------------------------------------\n\
; VS Compiler\n\
; ------------------------------------------------------------------------------\n\
Compiler( 'Compiler-v150-$VSBuildToolKey$' )\n\
{\n\
	.Root		= '$VSToolBinPath$'\n\
	.Executable = '$Root$\\cl.exe'\n\
	.ExtraFiles = { '$Root$\\c1.dll',\n\
					'$Root$\\c1xx.dll',\n\
					'$Root$\\c2.dll',\n\
					'$Root$\\$VSLangVersion$\\clui.dll',\n\
					'$VSBasePath$\\Common7\\IDE\\msobj140.dll',\n\
					'$VSBasePath$\\Common7\\IDE\\mspdb140.dll',\n\
					'$VSBasePath$\\Common7\\IDE\\mspdbsrv.exe',\n\
					'$VSBasePath$\\Common7\\IDE\\mspdbcore.dll',\n\
					'$VSBasePath$\\VC\\redist\\MSVC\\$RedistCrtDir$\\vcruntime140.dll',\n\
					'$VSBasePath$\\VC\\redist\\MSVC\\$RedistCrtDir$\\msvcp140.dll',\n\
					'$VSBasePath$\\VC\\redist\\MSVC\\$RedistCrtDir$\\vccorlib140.dll'\n\
				  }\n\
}\n\
\n\
; ------------------------------------------------------------------------------\n\
; VS Base Config\n\
; ------------------------------------------------------------------------------\n\
.MSVCBaseConfigV150 =\n\
[\n\
	.BaseIncludePaths	= ' /I\"./\"'\n\
	.CompilerOptions	= '%%1 /Fo\"%%2\" /FS '\n\
	.LinkerOptions		= '/OUT:\"%%2\" \"%%1\" '\n\
	.LibrarianOptions	= '/OUT:\"%%2\" \"%%1\" '\n\
	.PCHOptions			= ' %%1 /Fp\"%%2\" /Fo\"%%3\"'\n\
]\n\
\n\
.MSVCBaseConfigV150_%s =\n\
[\n\
	Using( .MSVCBaseConfigV150 )\n\
	.Compiler		= 'Compiler-v150-$VSBuildToolKey$'\n\
	.Librarian		= '$VSToolBinPath$\\lib.exe'\n\
	.Linker			= '$VSToolBinPath$\\link.exe'\n\
]\n\
";

#endif // FBUILD_TEMPLATE_CONFIG_VC150_H