// FbbTarget.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_H
#define FBUILD_TEMPLATE_H

static const char * FBVSBaseConfig =
"\n\
;-------------------------------------------------------------------------------\n\
; Windows Platform\n\
; ------------------------------------------------------------------------------\n\
\n\
.VSBasePath = 'D:\\Program Files (x86)\\Microsoft Visual Studio 12.0'\n\
.WindowsSDKBasePath = 'C:\\Program Files (x86)\\Windows Kits\\8.1'\n\
\n\
; ------------------------------------------------------------------------------\n\
; Settings\n\
; ------------------------------------------------------------------------------\n\
Settings\n\
{\n\
	#if __WINDOWS__\n\
		.Environment = { 'PATH=$VSBasePath$\\Common7\\IDE\\;$VSBasePath$\\VC\\bin\\;$WindowsSDKBasePath$\\bin\\x86',\n\
						 'TMP=C:\\Windows\\Temp',\n\
						 'SystemRoot=C:\\Windows' }\n\
		.CachePath = '.tmp\\fbuild.cache'\n\
	#endif\n\
	#if __OSX__\n\
		.CachePath = '.tmp/.fbuild.cache'\n\
	#endif\n\
	#if __LINUX__\n\
		.CachePath = '.tmp/.fbuild.cache'\n\
	#endif\n\
}\n\
\n\
; ------------------------------------------------------------------------------\n\
; VS Compiler\n\
; ------------------------------------------------------------------------------\n\
Compiler( 'Compiler-x86' )\n\
{\n\
	.Root		= '$VSBasePath$\\VC\\bin'\n\
	.Executable = '$Root$\\cl.exe'\n\
	.ExtraFiles = { '$Root$\\c1.dll'\n\
					'$Root$\\c1ast.dll',\n\
					'$Root$\\c1xx.dll',\n\
					'$Root$\\c1xxast.dll',\n\
					'$Root$\\c2.dll',\n\
					'$Root$\\1033\\clui.dll',\n\
					'$Root$\\msobj120.dll',\n\
					'$Root$\\mspdb120.dll',\n\
					'$Root$\\mspdbsrv.exe',\n\
					'$Root$\\mspdbcore.dll',\n\
					'$Root$\\mspft120.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC120.CRT\\msvcp120.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC120.CRT\\msvcr120.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC120.CRT\\vccorlib120.dll'\n\
				  }\n\
}\n\
\n\
Compiler('Compiler-x64')\n\
{\n\
	.Root		= '$VSBasePath$\\VC\\bin'\n\
	.Executable = '$Root$\\x86_amd64\\cl.exe'\n\
	.ExtraFiles = { '$Root$\\x86_amd64\\c1.dll'\n\
					'$Root$\\x86_amd64\\c1ast.dll',\n\
					'$Root$\\x86_amd64\\c1xx.dll',\n\
					'$Root$\\x86_amd64\\c1xxast.dll',\n\
					'$Root$\\x86_amd64\\c2.dll',\n\
					'$Root$\\x86_amd64\\1033\\clui.dll',\n\
					'$Root$\\msobj120.dll',\n\
					'$Root$\\mspdb120.dll',\n\
					'$Root$\\mspdbsrv.exe',\n\
					'$Root$\\mspdbcore.dll',\n\
					'$Root$\\mspft120.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC120.CRT\\msvcp120.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC120.CRT\\msvcr120.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC120.CRT\\vccorlib120.dll'\n\
				  }\n\
}\n\
\n\
; ------------------------------------------------------------------------------\n\
; VS Base Config\n\
; ------------------------------------------------------------------------------\n\
.MSVCBaseConfig =\n\
[\n\
	.BaseIncludePaths	= ' /I\"./\"'\n\
						+ ' /I\"$VSBasePath$/VC/include/\"'\n\
						+ ' /I\"$WindowsSDKBasePath$/include/um\"'\n\
						+ ' /I\"$WindowsSDKBasePath$/include/shared\"'\n\
						+ ' /I\"$WindowsSDKBasePath$/include/winrt\"'\n\
	.CompilerOptions	= '%1 /Fo\"%2\" /FS '\n\
	.LinkerOptions		= '/OUT:\"%2\" \"%1\" '\n\
	.LibrarianOptions	= '/OUT:\"%2\" \"%1\" '\n\
	.WindowsLibPaths	= '$WindowsSDKBasePath$/lib/winv6.3/um'\n\
]\n\
\n\
.X86BaseConfig =\n\
[\n\
	Using( .MSVCBaseConfig )\n\
	.ToolsBasePath	= '$VSBasePath$\\VC\\bin'\n\
	.Compiler		= 'Compiler-x86'\n\
	.Librarian		= '$ToolsBasePath$\\lib.exe'\n\
	.Linker			= '$ToolsBasePath$\\link.exe'\n\
	.LinkerOptions	+ ' /LIBPATH:\"$WindowsLibPaths$/x86\" /LIBPATH:\"$VSBasePath$/VC/lib\"'\n\
]\n\
\n\
.X64BaseConfig = \n\
[\n\
	Using( .MSVCBaseConfig )\n\
	.ToolsBasePath	= '$VSBasePath$\\VC\\bin\\x86_amd64'\n\
	.Compiler		= 'Compiler-x64'\n\
	.Librarian		= '$ToolsBasePath$\\lib.exe'\n\
	.Linker			= '$ToolsBasePath$\\link.exe'\n\
	.LinkerOptions	+ ' /LIBPATH:\"$WindowsLibPaths$/x64\" /LIBPATH:\"$VSBasePath$/VC/lib/amd64\"'\n\
]\n\
";

static const char * FBObjectList =
"\n\
ObjectList( '%s' )\n\
{\n\
	.CompilerOptions		+ '%s'\n\
	.CompilerOptions		+ .BaseIncludePaths\n\
	.CompilerOutputPath		= '%s'\n\
	%s.CompilerOutputPrefix	= '%s'\n\
	.CompilerInputFiles		= { %s }\n\
}\n\
";

static const char * FBLibrary =
"\n\
Library( '%s' )\n\
{\n\
	.DependLibs					= ' %s'\n\
	.LibrarianOptions			+ '%s'\n\
								+ .DependLibs\n\
	.LibrarianOutput			= '%s'\n\
	.CompilerOptions			+ ' /c'\n\
	.CompilerOptions			+ .BaseIncludePaths\n\
	.CompilerOutputPath			= '%s'\n\
	.ObjectListInputs			= { %s }\n\
	.AdditionalInputs			= { %s }\n\
	.LibrarianAdditionalInputs	= { .ObjectListInputs, .AdditionalInputs }\n\
	//.PreBuildDependencies		= {}\n\
}\n\
";

static const char * FBDLL =
"\n\
DLL( '%s' )\n\
{\n\
	.DependLibs				= ' %s'\n\
	.LinkerOptions			+ '%s'\n\
							+ .DependLibs\n\
	.LinkerOutput			= '%s'\n\
	.Libraries				= { %s }\n\
							+ { %s }\n\
}\n\
";

static const char * FBExecutable =
"\n\
Executable( '%s' )\n\
{\n\
	.DependLibs				= ' %s'\n\
	.LinkerOptions			+ '%s'\n\
							+ .DependLibs\n\
	.LinkerOutput			= '%s'\n\
	.Libraries				= { %s }\n\
							+ { %s }\n\
}\n\
";

static const char * FBTargets =
"\n\
Alias( 'All' )\n\
{\n\
	.Targets = { %s }\n\
}\n\
";

static const char * FBExecState =
"\n\
Exec( '%s' )\n\
{\n\
	.ExecExecutable			= 'C:\\FBHooker\\ExecState.bat'\n\
	.ExecArguments			= '%1 %2 %s'\n\
	.ExecOutput				= '%s'\n\
	.ExecInput				= '%s'\n\
	.PreBuildDependencies	= { %s }\n\
	}\n\
";

#endif // FBUILD_TEMPLATE_H