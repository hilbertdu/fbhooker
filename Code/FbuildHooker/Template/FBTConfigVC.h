//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTConfigVC.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/04/13
//*******************************************************

// FBTConfigVC.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_CONFIG_VC_H
#define FBUILD_TEMPLATE_CONFIG_VC_H

static const char * FBBaseCompilerConfig =
"\n\
; ------------------------------------------------------------------------------\n\
; Compiler\n\
; ------------------------------------------------------------------------------\n\
Compiler( 'Compiler-%s-%s' )\n\
{\n\
	.Executable = '%s'\n\
	.ExtraFiles = { %s }\n\
}\n\
\n\
.BaseCompilerConfig_%s_%s =\n\
[\n\
	.Compiler			= 'Compiler-%s'\n\
	.CompilerOptions	= '%s'\n\
]\n\
";


static const char * FBBaseLibrarianConfig = 
"\n\
.BaseLibrarianConfig_%s_%s =\n\
[\n\
	.Librarian			= '%s'\n\
	.LibrarianOptions	= '%s'\n\
]\n\
";


static const char * FBBaseLinkerConfig =
"\n\
.BaseLinkerConfig_%s_%s =\n\
[\n\
	.Linker				= '%s'\n\
	.LinkerOptions		= '%s'\n\
]\n\
";


#endif // FBUILD_TEMPLATE_CONFIG_VC_H