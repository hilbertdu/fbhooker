//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTConfigBase.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/04/13
//*******************************************************

// FBTConfigBase.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_CONFIG_BASE_H
#define FBUILD_TEMPLATE_CONFIG_BASE_H

static const char * FBGNUBaseCompilerConfig =
"\n\
; ------------------------------------------------------------------------------\n\
; Compiler\n\
; ------------------------------------------------------------------------------\n\
Compiler( 'Compiler-%s' )\n\
{\n\
	.Root		= '%s'\n\
	.Executable = '$Root$\\%s'\n\
	.ExtraFiles = { %s }\n\
}\n\
\n\
.BaseCompilerConfig_%s =\n\
[\n\
	.Compiler			= 'Compiler-%s'\n\
	.CompilerOptions	= '-o \"%%2\" \"%%1\" '\n\
	.BaseIncludePaths	= ' '\n\
]\n\
";


static const char * FBGNUBaseLibrarianConfig = 
"\n\
.BaseLibrarianConfig_%s =\n\
[\n\
	.Root				= '%s'\n\
	.Compiler			= 'Compiler-%s'\n\
	.CompilerOptions	= '-o \"%%2\" \"%%1\" '\n\
	.Librarian			= '$Root$\\%s'\n\
	.LibrarianOptions	= 'rcs \"%%2\" \"%%1\" '\n\
	.BaseIncludePaths	= ' '\n\
]\n\
";


static const char * FBGNUBaseLinkerConfig =
"\n\
.BaseLinkerConfig_%s =\n\
[\n\
	.Root				= '%s'\n\
	.Compiler			= 'Compiler-%s'\n\
	.CompilerOptions	= '-o \"%%2\" \"%%1\" '\n\
	.Linker				= '$Root$\\%s'\n\
	.LinkerOptions		= '\"%%1\" -o \"%%2\" '\n\
	.CompilerOptions	= ' '\n\
	.BaseIncludePaths	= ' '\n\
]\n\
";


// NDK const string
//------------------------------------------------------------------------------
static const char * FBNDK46ExtraFiles =
"\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\as.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\c++.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\g++.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\gcc.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\gcc.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\nm.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\objcopy.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\objdump.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\ranlib.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\strip.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\cc1.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\cc1plus.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\collect2.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\lto-wrapper.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\lto1.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\libfunction_reordering_plugin-0.dll',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\libfunction_reordering_plugin.dll.a',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\liblto_plugin-0.dll',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.6\\liblto_plugin.dll.a',\n\
\t\t\t\t  \
";

static const char * FBNDK48ExtraFiles =
"\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\as.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\c++.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\g++.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\gcc.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\gcc.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\nm.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\objcopy.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\objdump.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\ranlib.exe',\n\
\t\t\t\t\t'$Root$\\..\\arm-linux-androideabi\\bin\\strip.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\cc1.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\cc1plus.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\collect2.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\lto-wrapper.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\lto1.exe',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\libfunction_reordering_plugin-0.dll',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\libfunction_reordering_plugin.dll.a',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\liblto_plugin-0.dll',\n\
\t\t\t\t\t'$Root$\\..\\libexec\\gcc\\arm-linux-androideabi\\4.8\\liblto_plugin.dll.a',\n\
\t\t\t\t  \
";

#endif // FBUILD_TEMPLATE_CONFIG_BASE_H