//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTConfigV100.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/02
//*******************************************************

// FBTConfigVC110.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_CONFIG_VC100_H
#define FBUILD_TEMPLATE_CONFIG_VC100_H

static const char * FBVSBaseConfigVC100 =
"\n\
;-------------------------------------------------------------------------------\n\
; Windows Platform\n\
; ------------------------------------------------------------------------------\n\
\n\
.VSBasePath			= '%s'\n\
.VSToolBinPath		= '%s'\n\
.VSLangVersion		= '%s'\n\
.VSBuildToolKey		= '%s'\n\
\n\
; ------------------------------------------------------------------------------\n\
; VS Compiler\n\
; ------------------------------------------------------------------------------\n\
Compiler( 'Compiler-v100-$VSBuildToolKey$' )\n\
{\n\
	.Root		= '$VSToolBinPath$'\n\
	.Executable = '$Root$\\cl.exe'\n\
	.ExtraFiles = { '$Root$\\c1.dll',\n\
					'$Root$\\c1ast.dll',\n\
					'$Root$\\c1xx.dll',\n\
					'$Root$\\c1xxast.dll',\n\
					'$Root$\\c2.dll',\n\
					'$Root$\\mspft80.dll',\n\
					'$Root$\\$VSLangVersion$\\clui.dll',\n\
					'$VSBasePath$\\Common7\\IDE\\msobj100.dll',\n\
					'$VSBasePath$\\Common7\\IDE\\mspdb100.dll',\n\
					'$VSBasePath$\\Common7\\IDE\\mspdbsrv.exe',\n\
					'$VSBasePath$\\Common7\\IDE\\mspdbcore.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC100.CRT\\msvcp100.dll',\n\
					'$VSBasePath$\\VC\\redist\\x86\\Microsoft.VC100.CRT\\msvcr100.dll',\n\
				  }\n\
}\n\
\n\
; ------------------------------------------------------------------------------\n\
; VS Base Config\n\
; ------------------------------------------------------------------------------\n\
.MSVCBaseConfigV100 =\n\
[\n\
	.BaseIncludePaths	= ' /I\"./\"'\n\
	.CompilerOptions	= '%%1 /Fo\"%%2\" '\n\
	.LinkerOptions		= '/OUT:\"%%2\" \"%%1\" '\n\
	.LibrarianOptions	= '/OUT:\"%%2\" \"%%1\" '\n\
	.PCHOptions			= ' %%1 /Fp\"%%2\" /Fo\"%%3\"'\n\
]\n\
\n\
.MSVCBaseConfigV100_%s =\n\
[\n\
	Using( .MSVCBaseConfigV100 )\n\
	.Compiler		= 'Compiler-v100-$VSBuildToolKey$'\n\
	.Librarian		= '$VSToolBinPath$\\lib.exe'\n\
	.Linker			= '$VSToolBinPath$\\link.exe'\n\
]\n\
";

#endif // FBUILD_TEMPLATE_CONFIG_VC110_H