//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTConfig.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FBTConfig.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_CONFIG_H
#define FBUILD_TEMPLATE_CONFIG_H

#include "FBTConfigGNU.h"
#include "FBTConfigVC100.h"
#include "FBTConfigVC110.h"
#include "FBTConfigVC120.h"
#include "FBTConfigVC140.h"
#include "FBTConfigVC150.h"

#include <string>
#include <map>

typedef std::map<const std::string, const char*>	TARGET_CONFIG_TABLE_STR;
typedef std::map<const int, const char*>			TARGET_CONFIG_TABLE_INT;

static TARGET_CONFIG_TABLE_INT GNUBaseConfig = {
	{ 0, FBGNUBaseCompilerConfig },
	{ 1, FBGNUBaseLibrarianConfig },
	{ 2, FBGNUBaseLinkerConfig },
};

static TARGET_CONFIG_TABLE_STR NDKExtraFileConfig = {
	{ "4.6", FBNDK46ExtraFiles },
	{ "4.8", FBNDK48ExtraFiles }
};

static TARGET_CONFIG_TABLE_STR VSBaseConfig = {
	{ "100", FBVSBaseConfigVC100 },
	{ "110", FBVSBaseConfigVC110 },
	{ "120", FBVSBaseConfigVC120 },
	{ "140", FBVSBaseConfigVC140 },
	{ "150", FBVSBaseConfigVC150 }
};

#endif // FBUILD_TEMPLATE_CONFIG_H