//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTTarget.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FBTTarget.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_TARGET_H
#define FBUILD_TEMPLATE_TARGET_H

static const char * FBObjectList =
"\n\
{\n\
	%s\n\
	ObjectList( '%s' )\n\
	{\n\
		.Environment			= { %s }\n\
		.CompilerOptions		+ '%s'\n\
								+ .BaseIncludePaths\n\
		.CompilerOutputPath		= '%s'\n\
	%s	.CompilerOutputName		= '%s'\n\
		.CompilerInputFiles		= { %s }\n\
	\
		.RemotePdbDir			= '%s'\n\
		.PreBuildDependencies	= { %s }\n\
	}\n\
}\n\
";

static const char * FBObjectListPch =
"\n\
{\n\
	%s\n\
	ObjectList( '%s' )\n\
	{\n\
		.Environment			= { %s }\n\
		.PCHInputFiles			= { %s }\n\
		.PCHOutputFile			= '%s'\n\
	\
		.CompilerBaseOptions	= '%s'\n\
		.CompilePCHOptions		= ' /Yu\"%s\" /Fp\"$PCHOutputFile$\"'\n\
		.CompilerOptions		+ .CompilerBaseOptions\n\
								+ .BaseIncludePaths\n\
								+ .CompilePCHOptions\n\
		.CompilerOutputPath		= '%s'\n\
	%s	.CompilerOutputName		= '%s'\n\
		.CompilerInputFiles		= { %s }\n\
	\
		.PCHOptions				= '%%1 /Fp\"%%2\" /Fo\"%%3\" '\n\
		.PCHOptions				+ ' /Yc\"%s\" '\n\
								+ .CompilerBaseOptions\n\
		.PreBuildDependencies	= { %s }\n\
	}\n\
}\n\
";

static const char * FBLibrary =
"\n\
{\n\
	%s\n\
	Library( '%s' )\n\
	{\n\
		.Environment				= { %s }\n\
		.DependLibs					= ' %s'\n\
		.DependExtenalLibs			= ' %s'\n\
		.DependOthers				= ' %s'\n\
		.LibrarianOptions			+ '%s'\n\
									+ .DependLibs\n\
									+ .DependExtenalLibs\n\
									+ .DependOthers\n\
		.LibrarianOutput			= '%s'\n\
		.CompilerOptions			+ ' %s'\n\
		.CompilerOptions			+ .BaseIncludePaths\n\
		.CompilerOutputPath			= '%s'\n\
		.ObjectListInputs			= { %s }\n\
		.AdditionalInputs			= { %s }\n\
		.DependTargets				= { %s }\n\
		.LibrarianAdditionalInputs	= { .ObjectListInputs, .AdditionalInputs, .DependTargets }\n\
	%s	.ProjectTLogPath			= '%s'\n\
	}\n\
}\n\
";

static const char * FBDLL =
"\n\
{\n\
	%s\n\
	DLL( '%s' )\n\
	{\n\
		.Environment			= { %s }\n\
		.DependLibs				= ' %s'\n\
		.DependExtenalLibs		= ' %s'\n\
		.DependOthers			= ' %s'\n\
		.LinkerOptions			+ '%s'\n\
								+ ' %s'\n\
								+ .DependLibs\n\
								+ .DependExtenalLibs\n\
								+ .DependOthers\n\
		.LinkerOutput			= '%s'\n\
		.AdditionalInputs		= { %s }\n\
		.DependTargets			= { %s }\n\
	%s	.ProjectTLogPath		= '%s'\n\
		.ProjectReferences		= ' %s'\n\
		.LinkerLinkObjects		= false\n\
		.Libraries				= { %s }\n\
								+ { %s }\n\
								+ { .AdditionalInputs }\n\
								+ { .DependTargets }\n\
	}\n\
}\n\
";

static const char * FBExecutable =
"\n\
{\n\
	%s\n\
	Executable( '%s' )\n\
	{\n\
		.Environment			= { %s }\n\
		.DependLibs				= ' %s'\n\
		.DependExtenalLibs		= ' %s'\n\
		.DependOthers			= ' %s'\n\
		.LinkerOptions			+ '%s'\n\
								+ ' %s'\n\
								+ .DependLibs\n\
								+ .DependExtenalLibs\n\
								+ .DependOthers\n\
		.LinkerOutput			= '%s'\n\
		.AdditionalInputs		= { %s }\n\
		.DependTargets			= { %s }\n\
	%s	.ProjectTLogPath		= '%s'\n\
		.ProjectReferences		= ' %s'\n\
		.Libraries				= { %s }\n\
								+ { %s }\n\
								+ { .AdditionalInputs }\n\
								+ { .DependTargets }\n\
	}\n\
}\n\
";

static const char * FBTargets =
"\n\
Alias( 'All' )\n\
{\n\
\t.Targets = { %s }\n\
}\n\
";

static const char * FBExecState =
"\n\
Exec( '%s' )\n\
{\n\
	.ExecExecutable			= 'C:\\FBHooker\\ExecState.bat'\n\
	.ExecArguments			= '%1 %2 %s'\n\
	.ExecOutput				= '%s'\n\
	.ExecInput				= '%s'\n\
	.PreBuildDependencies	= { %s }\n\
	}\n\
";


#endif // FBUILD_TEMPLATE_TARGET_H