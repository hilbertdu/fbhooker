//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FBTSettings.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/12/29
//*******************************************************

// FBTSettings.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_TEMPLATE_CONFIG_SETTINGS_H
#define FBUILD_TEMPLATE_CONFIG_SETTINGS_H

static const char * FBVSSettings = 
"; ------------------------------------------------------------------------------\n\
; Settings\n\
; ------------------------------------------------------------------------------\n\
\n\
Settings\n\
{\n\
#if __WINDOWS__\n\
		.Environment = { %s }\n\
		.CachePath = '.tmp\\fbuild.cache'\n\
	#endif\n\
	#if __OSX__\n\
		.CachePath = '.tmp/.fbuild.cache'\n\
	#endif\n\
	#if __LINUX__\n\
		.CachePath = '.tmp/.fbuild.cache'\n\
	#endif\n\
}\n\
";

#endif // FBUILD_TEMPLATE_CONFIG_SETTINGS_H