//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: HookerJob.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// HookerJob.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Job.h"
#include "../Common/Common.h"
#include "../Hooker/FBuildHooker.h"
#include "../Parser/Parser.h"
#include "../Task/PrebuildPDBTask.h"

#include <string.h>

// Constructor
//------------------------------------------------------------------------------
BuildHookerJob::BuildHookerJob() :
fbParser(nullptr)
{
	// TODO check tmp file exist
	useQuickMode = true;
}

BuildHookerJob::BuildHookerJob(const std::string &command,
	COMMAND_TYPE type)
{
	fbParser = (BuildParser*)Parser::createParser(command, type);
	envParser = new EnvParser();
}

// Deconstructor
//------------------------------------------------------------------------------
/*virtual*/ BuildHookerJob::~BuildHookerJob()
{
	SAFE_DELETE(fbParser);
	SAFE_DELETE(envParser);
}

// DoJob
//------------------------------------------------------------------------------
void BuildHookerJob::DoJob()
{
	if (!fbParser)
	{
		return;
	}

	int tryCount = 0;
	bool openState = false;
#if defined( __WINDOWS__ )
	sharedState = new SharedMemory("FBHookerSharedState_xxxx", sizeof(SharedState));
	sharedData = new SharedMemory("FBHookerSharedData_xxxx");
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	sharedState = new SharedMemory(30001, sizeof(SharedState));
	sharedData = new SharedMemory(30002);
#endif

	while (!openState && tryCount++ < LOCK_MEM_TRY_COUNT)
	{
		DEBUG("[Fb hooker] waiting for mutex\n");
		lock->Lock();
		DEBUG("FBHookerSharedMemory test Open\n");
		openState = sharedState->Open();
		openState &= sharedData->Open();
		if (openState)
		{
			sharedData->Close();
			lock->Unlock();
			break;
		}
		lock->Unlock();
		Thread::Sleep(100);
	}

	if (!openState)
	{
		INFO("Job exit without done--------------\n");
		exitState = BUILD_STATE_CANCEL;
		return;
	}

	GenerateEnvArray();

	// Do job
	fbParser->SetEnvArray(Env::GetUserEnvs());
	fbParser->Tokenize();
	fbParser->ParserContent();	// need to parse content because we need to know the cl output file
	fbParser->Serialize();

	fbParser->DumpTokens();
	fbParser->DumpInfo();

	DeleteNeedToBuild();

	StartWriteData();

	INFO("Job done --------------\n");
}

// StartWriteData
//------------------------------------------------------------------------------
void BuildHookerJob::PrebuildPDB()
{
	size_t pdbType = fbParser->GetUsePdbType();
	std::string path = fbParser->GetPdbFile();

	if (pdbType != 1 || path.empty())
	{
		return;
	}

	// To support MSVC increment build, add a empty pdb file to /Fo output
	if (!FileDirHelper::FileExists(path))
	{
		INFO("Generate an empty pdb file to enable MSVC increment build: %s\n", path.c_str());
		std::string execPaths, options;
		fbParser->GetEnvByName("PATH", execPaths);
		PrebuildPDBTask emptyPDBTask(execPaths, fbParser->GetBuildOpts(), path, FileDirHelper::GetCurrentDir());

		// TODO check result
		emptyPDBTask.DoBuild();
	}
}

// StartWriteData
//------------------------------------------------------------------------------
void BuildHookerJob::StartWriteData()
{
	const std::string &str = fbParser->GetSeriaStr();
	bool readyToExit = false;

	while (true)
	{
		//DEBUG("Waiting for job mutex\n");
		lock->Lock();

		int state = GET_SHARED_STATE(sharedState->GetPtr());

		// Check if size has been modified by others
		int size = GET_SHARED_STATE_REMAP(sharedState->GetPtr());
		if (sharedData->GetMaxSize() != size)
		{
			if (state != NEED_TO_RMAP)
			{
				ReMapSharedMem(GET_SHARED_STATE_REMAP(sharedState->GetPtr()));
				if (exitState == SHARED_MEM_CANNOT_REMAP)
				{
					FATALERROR("can not remap shared mem FBHookerSharedData_xxxx %d\n", str.size() + 1);
					ErrorHelper::ShowLastErrorInfo();
					break;
				}
			}
			else
			{
				lock->Unlock();
				continue;
			}
		}
		else if (!sharedData->Open())
		{
			FATALERROR("Can not open shared data\n");
			exitState = PROCESS_START_EXIT;
			lock->Unlock();
			break;
		}

		if (state == READY_TO_WRITE && !readyToExit)
		{
			if (str.size() + 1 > sharedData->GetMaxSize())
			{
				DEBUG("Need to call service to remap!\n");
				SET_SHARED_STATE(sharedState->GetPtr(), NEED_TO_RMAP);
				SET_SHARED_STATE_REMAP(sharedState->GetPtr(), str.size() + 1);
			}
			else
			{
				if (fbParser->GetToolChainType() == ToolChain::TOOLCHAIN_TYPE::MSVC)
				{
					PrebuildPDB();		// in the lock
				}
				DEBUG("Write shared memory\n");
				memcpy(GET_SHARED_DATA(sharedData->GetPtr()), str.c_str(), str.size() + 1);
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_READ);
				readyToExit = true;
			}
		}
		else if (state == READY_TO_EXIT && readyToExit)
		{
			// Should wait for exit state to support prebuild targets
			exitState = GET_SHARED_STATE_EXIT(sharedState->GetPtr());
			SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_WRITE);
			break;
		}
		else if (state == READY_TO_FBUILD && readyToExit)
		{
			DEBUG("Ready to DeSerialize: %s!\n", GET_SHARED_DATA(sharedData->GetPtr()));
			envParser->DeSerialize(GET_SHARED_DATA(sharedData->GetPtr()));
			SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_WRITE);
			sharedData->Close();
			lock->Unlock();
			ShowStartupInfo();
			StartFastBuild();
			break;
		}
		else if (state == BUILD_ERROR)
		{
			exitState = BUILD_STATE_ERROR_FASTBUILD;
			break;
		}

		sharedData->Close();
		lock->Unlock();
		Thread::Sleep(1);
	}

	// Clean
	sharedState->Close();
	sharedData->Close();
	lock->Unlock();
}

// StartFastBuild
//------------------------------------------------------------------------------
void BuildHookerJob::DeleteNeedToBuild()
{
	// delete those cl outputs to make sure the obj need to build by vs
	/*if (fbParser->GetBuildType() == COMPILE)
	{*/
	DEBUG("DeleteNeedToBuild\n");
	const std::string &outputFile = fbParser->GetOuputFile();
	if (outputFile.length() > 0)
	{
		DEBUG("Delete file: %s\n", outputFile.c_str());
		FileDirHelper::FileDelete(outputFile);
	}
	else if (fbParser->GetOuputPath().length() > 0)
	{
		SRC_FILE_ITER sourceIter = fbParser->GetSourceFiles().begin();
		for (; sourceIter != fbParser->GetSourceFiles().end(); ++sourceIter)
		{
			std::string nameWithoutExt;
			FileDirHelper::GetFileNameFromPath(nameWithoutExt, (*sourceIter).first);
			nameWithoutExt = nameWithoutExt.substr(0, nameWithoutExt.rfind('.'));
			nameWithoutExt += ".obj";

			DEBUG("Delete file: %s\n", (fbParser->GetOuputPath() + "\\" + nameWithoutExt).c_str());
			FileDirHelper::FileDelete(fbParser->GetOuputPath() + "\\" + nameWithoutExt);
		}
	}
	//}
}

// TargetFileExist
//------------------------------------------------------------------------------
bool BuildHookerJob::TargetFileExist()
{
	const std::vector<std::string> targetPaths = envParser->GetTargetPaths();
	for (size_t idx = 0; idx < targetPaths.size(); ++idx)
	{
		if (!FileDirHelper::FileExists(targetPaths[idx]))
		{
			return false;
		}
	}
	return true;
}

// GenerateEnvString
//------------------------------------------------------------------------------
void BuildHookerJob::GenerateEnvArray()
{
	std::string envPath = Env::GetEnvVariable("PATH");
	while (StrHelper::StrBeginWithI(envPath, FBuildHooker::g_FBHookerPath.c_str()))
	{
		envPath = envPath.substr(FBuildHooker::g_FBHookerPath.length() + 1);
	}
	INFO("GenerateEnvPath: %s\n", envPath.c_str());

	Env::ResetUserEnv();
	Env::AddUserEnv("PATH", envPath);
	Env::AddUserEnv("INCLUDE");
	Env::AddUserEnv("TMP");
	Env::AddUserEnv("SystemRoot");
}

// StartFastBuild
//------------------------------------------------------------------------------
void BuildHookerJob::StartFastBuild()
{
	DEBUG("Starting fast build\n");
	if (exitState != BUILD_STATE_OK && exitState != BUILD_STATE_NOT_BEGIN)
	{
		if (exitState == BUILD_STATE_NO_TARGET)
		{
			INFO("No target need to be build, no need to run fastbuild!\n");
			exitState = BUILD_STATE_OK;
		}
		return;
	}

	std::string args = "-noprogress -fixuperrorpaths ";
	args += "-config \"" + envParser->GetFBFilePath() + "\" ";
	args += envParser->GetFastbuildOpts() + " ";
	args += envParser->GetCurTargetName();

	std::string customFile = envParser->GetSolutionDir() + "\\" + envParser->GetSolutionName() + ".bff";
	if (FileDirHelper::FileExists(customFile))
	{
		args += " -custom \"" + customFile + "\" ";
	}
	else
	{
		INFO("Not sepecify custom file: %s\n", customFile.c_str());
	}
#if defined( _DEBUG )
	args += " -showcmds";
#endif

	Env::AddUserEnv("FASTBUILD_BROKERAGE_PATH", envParser->GetBrokeragePath());
	Env::GenerateUserEnvBlock();

	std::string workingDir = FileDirHelper::GetCurrentDir();
	INFO("Fbuild args: %s\n", args.c_str());

	INFO("************************** Fastbuild Started ******************************\n");
	Process proc;
	int ret = proc.Spawn((FBuildHooker::g_FBHookerPath + "\\Fbuild.exe").c_str(), args.c_str(), workingDir.c_str(), Env::GetUserEnvBlock()/*envBlock*/, true);
	if (ret == PROCESS_CREATE_OK)
	{
		exitState = proc.WaitForExit();
		if (exitState != BUILD_STATE_OK)
		{
			// Fastbuild has some memory error When exit process and which can lead to crash and return an error code.
			// Now I simply ignore the error exit code when target is found to be created successfully.
			// TODO this problem should be resolve later.
			if (TargetFileExist())
			{
				WARNING("%s has been created but fbuild exit unexpectedly: %d\n", envParser->GetCurTargetName().c_str(), exitState);
				exitState = BUILD_STATE_OK;
			}
			else
			{
				FATALERROR("Failed to build %s, fbuild exit code: %d\n", envParser->GetCurTargetName().c_str(), exitState);
				exitState = BUILD_STATE_ERROR;
			}
		}
	}
	else
	{
		FATALERROR("Failed to create fbuild process! %d\n", ErrorHelper::GetLastError());
		ErrorHelper::ShowLastErrorInfo();
		exitState = PROCESS_CREATE_ERROR;
	}
	INFO("************************** Fastbuild Finished *****************************\n");
}

// StartFastBuild
//------------------------------------------------------------------------------
void BuildHookerJob::ShowStartupInfo()
{
	if (envParser->GetShowStartInfo())
	{
		static const char * startupInfo =
			"\n\
			*************************************************************\n\
			**   Welcome to use fastbuild in VS integration version.   **\n\
			**   If you have any problem in use, you can contact us.   **\n\
			**           Popo: hzduli@corp.netease.com                 **\n\
			**   (Close this startup info in events solution config)   **\n\
			*************************************************************\n\
			\n";
		OUTPUT_DIRECT(startupInfo);
	}
}

//------------------------------------------------------------------------------
