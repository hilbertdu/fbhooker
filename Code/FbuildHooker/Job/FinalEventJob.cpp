//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: FinalEventJob.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// FinalEventJob.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Job.h"
#include "../Common/Common.h"
#include "../Hooker/FBuildHooker.h"
#include "../Parser/Parser.h"

// Constructor
//------------------------------------------------------------------------------
FinalEventJob::FinalEventJob(const std::string & cmd, bool interbuild) :
isInterBuild(interbuild)
{
	envParser = static_cast<EnvParser*>(Parser::createParser(cmd, FB_SERVICE_START));
	envParserS = new EnvParser();
}

// Deconstructor
//------------------------------------------------------------------------------
/*virtual*/ FinalEventJob::~FinalEventJob()
{
	SAFE_DELETE(envParser);
	SAFE_DELETE(envParserS);
}

// DoJob
//------------------------------------------------------------------------------
void FinalEventJob::DoJob()
{
	DEBUG("Final event job doing ...\n");

	int tryCount = 0;
	bool openState = false;
#if defined( __WINDOWS__ )
	sharedState = new SharedMemory("FBHookerSharedState_xxxx", sizeof(SharedState));
	sharedData = new SharedMemory("FBHookerSharedData_xxxx");
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	sharedState = new SharedMemory(30001, sizeof(SharedState));
	sharedData = new SharedMemory(30002);
#endif

	while (!openState && tryCount++ < LOCK_MEM_TRY_COUNT)
	{
		lock->Lock();
		openState = sharedState->Open();
		openState &= sharedData->Open();
		if (openState)
		{
			sharedData->Close();
			lock->Unlock();
			break;
		}
		lock->Unlock();
		Thread::Sleep(100);
	}

	if (!openState)
	{
		INFO("Job exit without done--------------\n");
		exitState = BUILD_STATE_CANCEL;
		return;
	}

	StartSyncState();
}

// StartSyncState
//------------------------------------------------------------------------------
void FinalEventJob::StartSyncState()
{
	bool readyToExit = false;
	int tryExitCount = 0;

	while (true)
	{
		lock->Lock();

		int state = GET_SHARED_STATE(sharedState->GetPtr());

		// Check if size has been modified by others
		int size = GET_SHARED_STATE_REMAP(sharedState->GetPtr());
		if (sharedData->GetMaxSize() != size)
		{
			if (state != NEED_TO_RMAP)
			{
				ReMapSharedMem(GET_SHARED_STATE_REMAP(sharedState->GetPtr()));
				if (exitState == SHARED_MEM_CANNOT_REMAP)
				{
					FATALERROR("can not remap shared mem FBHookerSharedData_xxxx %d\n", size);
					break;
				}
			}
			else
			{
				lock->Unlock();
				continue;
			}
		}
		else if (!sharedData->Open())
		{
			FATALERROR("Can not open shared data\n");
			exitState = PROCESS_START_EXIT;
			lock->Unlock();
			break;
		}

		if (state == READY_TO_WRITE)
		{
			if (isInterBuild)
			{
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_IBUILD);
			}
			else
			{
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_FINISH);
				break;
			}
		}
		else if (state == READY_TO_FBUILD)
		{
			if (isInterBuild)
			{
				DEBUG("Ready to start a inter fbuild\n");
				DEBUG("Ready to DeSerialize: %s!\n", GET_SHARED_DATA(sharedData->GetPtr()));
				envParserS->DeSerialize(GET_SHARED_DATA(sharedData->GetPtr()));
				StartFastBuild();
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_WRITE);
				break;
			}
		}
		else if (state == READY_TO_EXIT)
		{
			if (isInterBuild)
			{
				INFO("No targets need to build\n");
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_WRITE);
				exitState = BUILD_STATE_OK;
				break;
			}
		}
		sharedData->Close();

		lock->Unlock();
		Thread::Sleep(1);
	}

	// Clean
	sharedData->Close();
	lock->Unlock();
}

// GenerateEnvArray
//------------------------------------------------------------------------------
void FinalEventJob::GenerateEnvArray()
{
	std::string envPath = Env::GetEnvVariable("PATH");
	if (StrHelper::StrBeginWithI(envPath, FBuildHooker::g_FBHookerPath.c_str()))
	{
		envPath = envPath.substr(FBuildHooker::g_FBHookerPath.length() + 1);
	}
	DEBUG("GenerateEnvPath: %s\n", envPath.c_str());

	Env::ResetUserEnv();
	Env::AddUserEnv("PATH", envPath);
	Env::AddUserEnv("INCLUDE");
	Env::AddUserEnv("TMP");
	Env::AddUserEnv("SystemRoot");
	Env::AddUserEnv("FASTBUILD_BROKERAGE_PATH", envParser->GetBrokeragePath());
}

// TargetFileExist
//------------------------------------------------------------------------------
bool FinalEventJob::TargetFileExist()
{
	const std::vector<std::string> targetPaths = envParserS->GetTargetPaths();
	for (size_t idx = 0; idx < targetPaths.size(); ++idx)
	{
		DEBUG("Check target file exist: %s\n", targetPaths[idx].c_str());
		if (!FileDirHelper::FileExists(targetPaths[idx]))
		{
			return false;
		}
	}
	return true;
}

// StartFastBuild
//------------------------------------------------------------------------------
void FinalEventJob::StartFastBuild()
{
	if (exitState != BUILD_STATE_OK && exitState != BUILD_STATE_NOT_BEGIN)
	{
		if (exitState == BUILD_STATE_NO_TARGET)
		{
			INFO("No target need to be build, no need to run fastbuild!\n");
			exitState = BUILD_STATE_OK;
		}
		return;
	}

	envParser->Tokenize();
	envParser->ParserContent();

	std::string args = "-noprogress -fixuperrorpaths ";
	args += "-config \"" + envParser->GetFBFilePath() + "\" ";
	args += envParser->GetFastbuildOpts() + " ";

	std::string customFile = envParser->GetSolutionDir() + "\\" + envParser->GetSolutionName() + ".bff";
	if (FileDirHelper::FileExists(customFile))
	{
		args += " -custom \"" + customFile + "\" ";
	}
	else
	{
		DEBUG("Not sepecify custom file: %s\n", customFile.c_str());
	}

	GenerateEnvArray();
	Env::GenerateUserEnvBlock();

	std::string workingDir = FileDirHelper::GetCurrentDir();
	INFO("Fbuild args: %s\n", args.c_str());

	INFO("************************** Fastbuild Started ******************************\n");
	Process proc;
	int ret = proc.Spawn((FBuildHooker::g_FBHookerPath + "\\Fbuild.exe").c_str(), args.c_str(), workingDir.c_str(), Env::GetUserEnvBlock(), true);
	if (ret == PROCESS_CREATE_OK)
	{
		exitState = proc.WaitForExit();
		if (exitState != BUILD_STATE_OK)
		{
			// Fastbuild has some memory error When exit process and which can lead to crash and return an error code.
			// Now I simply ignore the error exit code when target is found to be created successfully.
			// TODO this problem should be resolve later.
			if (TargetFileExist())
			{
				WARNING("%s has been created but fbuild exit unexpectedly: %d\n", envParserS->GetTargetsNameStr().c_str(), exitState);
				exitState = BUILD_STATE_OK;
			}
			else
			{
				FATALERROR("Failed to build %s, fbuild exit code: %d\n", envParserS->GetTargetsNameStr().c_str(), exitState);
				exitState = BUILD_STATE_ERROR;
			}
		}
	}
	else
	{
		FATALERROR("Failed to create fbuild process! %d\n", ErrorHelper::GetLastError());
		ErrorHelper::ShowLastErrorInfo();
		exitState = PROCESS_CREATE_ERROR;
	}
	INFO("************************** Fastbuild Finished *****************************\n");
}

//------------------------------------------------------------------------------
