//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: BuildGeneratorJob.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// BuildGeneratorJob.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Job.h"
#include "../Parser/Parser.h"
#include "../Generator/FbbGenerator.h"
#include "../Generator/FbbFile.h"
#include <fstream>
#include <string.h>


// Constructor
//------------------------------------------------------------------------------
BuildGeneratorJob::BuildGeneratorJob(const std::string & cmd, bool quickMode) :
useQuickMode(quickMode)
{
	fbParser = static_cast<EnvParser*>(Parser::createParser(cmd, FB_SERVICE_START));
	fbGenerator = new FBuildGenerator(useQuickMode);
	INFO("Use quick mode to build generator job: %d\n", quickMode);
}

// Deconstructor
//------------------------------------------------------------------------------
/*virtual*/ BuildGeneratorJob::~BuildGeneratorJob()
{
	SAFE_DELETE(fbParser);
	SAFE_DELETE(fbGenerator);
}

// InitTmpEnv
//------------------------------------------------------------------------------
void BuildGeneratorJob::InitTmpEnv()
{
	// we need send some fastbuild env to linker
	// 1.solution dir and solution name
	// 2.bff file path
	// 3.fastbuild extra opts
	// 4.target name

	tmpEnv = "";
	tmpEnv += "/SLNDIR:\"" + fbParser->GetSolutionDir() + "\"";
	tmpEnv += " /SLNNAME:" + fbParser->GetSolutionName();
	tmpEnv += " /EXTRAOPT:\"" + fbParser->GetRawExtraOpts() + "\"";
	tmpEnv += " /FBOPTS:\"" + fbParser->GetFastbuildOpts() + "\"";

	tmpEnv += " /FBFILE:\"" + fbGenerator->GetFilePtr()->GetCurFbbFilePath() + "\"";

	std::string targetNames;
	StrHelper::StrJoin(targetNames, fbGenerator->GetCurTargetNames(), "|");
	tmpEnv += " /TARGET:\"" + targetNames + "\"";

	std::string targetPaths;
	StrHelper::StrJoin(targetPaths, fbGenerator->GetCurTargetPaths(), "|");
	tmpEnv += " /TARGETPATH:\"" + targetPaths + "\"";

	tmpEnv += " " + remoteEnv;

	if (fbParser->GetShowStartInfo())
	{
		tmpEnv += " /STARTINFO";
		fbParser->SetShowStartInfo(false);
	}
	INFO("Tmp env: %s\n", tmpEnv.c_str());
}

void BuildGeneratorJob::InitRemoteEnv()
{
	remoteEnv = "/BROKERAGEPATH:\"" + fbParser->GetBrokeragePath() + "\"";
	DEBUG("Remote env: %s\n", remoteEnv.c_str());
}

// DoJob
//------------------------------------------------------------------------------
void BuildGeneratorJob::DoJob()
{
#if defined( __WINDOWS__ )
	sharedState = new SharedMemory("FBHookerSharedState_xxxx", sizeof(SharedState));	// TODO -- Creat a hash tag according to current proj path
	sharedData = new SharedMemory("FBHookerSharedData_xxxx");		// Do not use global shared memory
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	sharedState = new SharedMemory(30001, sizeof(SharedState));	// TODO -- Creat a hash tag according to current proj path
	sharedData = new SharedMemory(30002);		// Do not use global shared memory
#endif

	lock->Lock();
	DEBUG("FBHookerSharedMemory created\n");
	sharedState->Create();
	sharedData->Create();
	SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_WRITE);
	SET_SHARED_STATE_REMAP(sharedState->GetPtr(), 2048);
	lock->Unlock();

	fbParser->Tokenize();
	fbParser->ParserContent();
	//fbParser->DumpInfo();

	fbGenerator->AddParser(fbParser);

	// TODO init data and start
	InitRemoteEnv();
	StartReadData();

	INFO("Exit generator\n");
}

// StartReadData
//------------------------------------------------------------------------------
void BuildGeneratorJob::StartReadData()
{
	INFO("Start looping...\n");
	while (true)
	{
		lock->Lock();
		int state = GET_SHARED_STATE(sharedState->GetPtr());
		//DEBUG("Shared mem state: %d\n", state);
		if (state == READY_TO_READ)
		{
			DEBUG("Read shared memory\n");
			BuildParser *parser = new BuildParser();
			parser->DeSerialize(GET_SHARED_DATA(sharedData->GetPtr()));

			int type = parser->GetBuildType();
			if (type == ToolChain::BUILD_TYPE::STATIC_LIBRARY || type == ToolChain::BUILD_TYPE::DYNAMIC_LIBRARY)
			{
				INFO("Start build target %s\n", parser->GetOuputFile().c_str());

				fbGenerator->RunProject(parser);
				if (useQuickMode)
				{
					// In quick mode, the generator will create total bff with dependency, and not send READY_TO_FBUILD signal
					SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_EXIT);
				}
				else
				{
					exitState = fbGenerator->GetBuildState();
					InitTmpEnv();
					if (tmpEnv.size() + 1 > sharedData->GetMaxSize())
					{
						ReAllocSharedMem(tmpEnv.size() + 1);
						if (exitState == SHARED_MEM_CANNOT_CREATE)
						{
							FATALERROR("Can not realloc shared mem : from tmpEnv\n");
							ErrorHelper::ShowLastErrorInfo();
							break;
						}
						SET_SHARED_STATE_REMAP(sharedState->GetPtr(), tmpEnv.size() + 1);
					}
					memcpy(GET_SHARED_DATA(sharedData->GetPtr()), tmpEnv.c_str(), tmpEnv.size() + 1);
					SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_FBUILD);
				}
			}
			else
			{
				fbGenerator->AddParser(parser);
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_EXIT);
			}
		}
		else if (state == READY_TO_IBUILD)
		{
			// Only enable in the quick mode
			fbGenerator->GenEntryFbbFile();
			if (fbGenerator->GetBuildState() == BUILD_STATE_NO_TARGET)
			{
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_EXIT);
			}
			else
			{
				InitTmpEnv();
				if (tmpEnv.size() + 1 > sharedData->GetMaxSize())
				{
					ReAllocSharedMem(tmpEnv.size() + 1);
					if (exitState == SHARED_MEM_CANNOT_CREATE)
					{
						FATALERROR("Can not realloc shared mem : from tmpEnv\n");
						ErrorHelper::ShowLastErrorInfo();
						break;
					}
					SET_SHARED_STATE_REMAP(sharedState->GetPtr(), tmpEnv.size() + 1);
				}
				memcpy(GET_SHARED_DATA(sharedData->GetPtr()), tmpEnv.c_str(), tmpEnv.size() + 1);
				SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_FBUILD);
			}
		}
		else if (state == NEED_TO_RMAP)
		{
			ReAllocSharedMem(GET_SHARED_STATE_REMAP(sharedState->GetPtr()));
			if (exitState == SHARED_MEM_CANNOT_CREATE)
			{
				FATALERROR("Can not realloc shared mem : from hooker\n");
				ErrorHelper::ShowLastErrorInfo();
				break;
			}
			SET_SHARED_STATE(sharedState->GetPtr(), READY_TO_WRITE);
		}
		else if (state == READY_TO_FINISH)
		{
			INFO("Ready to finish\n");
			break;
		}
		lock->Unlock();
		Thread::Sleep(1);
	}

	// Clean
	lock->Unlock();
}

//------------------------------------------------------------------------------
