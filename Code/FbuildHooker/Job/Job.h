//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Job.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Job.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FBUILD_JOB_H
#define FBUILD_JOB_H

#include "../Common/Common.h"

class BuildParser;
class EnvParser;
class FBuildGenerator;

struct SharedState
{
	size_t	state;
	size_t  remapSize;
	int		exitState;
};

// SharedMemState
//------------------------------------------------------------------------------
#define READY_TO_WRITE		1
#define READY_TO_READ		2
#define NEED_TO_RMAP		3

#define READY_TO_IBUILD		6
#define READY_TO_FBUILD		7
#define READY_TO_FINISH		8
#define READY_TO_EXIT		10

#define BUILD_ERROR			12

//#define LAST_BUILD_MODE		20

// SharedMemData
//------------------------------------------------------------------------------
#define GET_SHARED_STATE(p)				((SharedState *)(p))->state
#define GET_SHARED_STATE_EXIT(p)		((SharedState *)(p))->exitState
#define GET_SHARED_STATE_REMAP(p)		((SharedState *)(p))->remapSize
#define GET_SHARED_DATA(p)				(char *)(p)

#define SET_SHARED_STATE(p, s)			((SharedState *)(p))->state = (s)
#define SET_SHARED_STATE_EXIT(p, e)		((SharedState *)(p))->exitState = (e)
#define SET_SHARED_STATE_REMAP(p, e)	((SharedState *)(p))->remapSize = (e)

// Job
//------------------------------------------------------------------------------
class Job
{
public:
	Job();
	virtual ~Job();

	virtual void DoJob() = 0;
	inline int GetJobExitState() { return exitState; };

protected:
	void ReAllocSharedMem(size_t size);
	void ReMapSharedMem(size_t size);

	SharedMemory *sharedState;
	SharedMemory *sharedData;
	SystemMutex *lock;
	int exitState;
};

// BuildHookerJob
//------------------------------------------------------------------------------
class BuildHookerJob : public Job
{
public:
	BuildHookerJob();
	BuildHookerJob(const std::string &, COMMAND_TYPE);
	virtual ~BuildHookerJob();

	void SetCommand(const std::string &command, COMMAND_TYPE type);
	virtual void DoJob();

private:
	void StartFastBuild();
	void StartWriteData();
	void PrebuildPDB();
	void ShowStartupInfo();

	void GenerateEnvArray();
	void DeleteNeedToBuild();
	bool TargetFileExist();

	BuildParser *fbParser;
	EnvParser	*envParser;

	bool useQuickMode;
};

// BuildGeneratorJob
//------------------------------------------------------------------------------
class BuildGeneratorJob : public Job
{
public:
	BuildGeneratorJob(const std::string & cmd, bool quickMode = false);
	virtual ~BuildGeneratorJob();

	virtual void DoJob();

private:
	void InitTmpEnv();
	void InitRemoteEnv();

	void StartReadData();

	FBuildGenerator *fbGenerator;
	EnvParser *fbParser;

	std::string tmpEnv;
	std::string remoteEnv;
	bool useQuickMode;
};

// FinalEventJob
//------------------------------------------------------------------------------
class FinalEventJob : public Job
{
public:
	FinalEventJob(const std::string & cmd, bool interBuild = false);
	virtual ~FinalEventJob();

	virtual void DoJob();

private:
	void StartSyncState();
	void StartFastBuild();
	void GenerateEnvArray();

	bool TargetFileExist();

	EnvParser *envParser;
	EnvParser *envParserS;

	int finalState;
	int finalType;
	bool isInterBuild;
};

#endif // FBUILD_JOB_H