//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Job.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Job.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Job.h"
#include "../Hooker/FBuildHooker.h"

// Constructor
//------------------------------------------------------------------------------
Job::Job() :
sharedState(nullptr),
sharedData(nullptr),
lock(&FBuildHooker::g_SyncSharedMutex),
exitState(BUILD_STATE_NOT_BEGIN)
{
}

// Deconstructor
//------------------------------------------------------------------------------
/*virtual*/ Job::~Job()
{
	SAFE_DELETE(sharedState);
	SAFE_DELETE(sharedData);
}

// ReAllocSharedMem
//------------------------------------------------------------------------------
void Job::ReAllocSharedMem(size_t size)
{
	DEBUG("ReAllocSharedMem %d\n", size);
	if (!sharedData->ReCreate(size))
	{
		exitState = SHARED_MEM_CANNOT_CREATE;
	}
}

// ReMapSharedMem
//------------------------------------------------------------------------------
void Job::ReMapSharedMem(size_t size)
{
	DEBUG("ReMapSharedMem %d\n", size);
	if (!sharedData->ReOpen(size))
	{
		exitState = SHARED_MEM_CANNOT_REMAP;
	}
}


//------------------------------------------------------------------------------