//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: main.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/04
//*******************************************************

// Workflow:
// 1. Start fbservice -start
// 2. Parse input file (sln or vcproj), replace all Microsoft.Cpp.targets
// 3. Run MSBuild.exe with correct parameter (only support MSVC currently)
// 4. Start fbservice -exit
// 5. Exit console

// Includes
//------------------------------------------------------------------------------
#include "Common/Common.h"
#include "BuildTask/MSBuild.h"
#include "BuildTask/NDKBuild.h"
#include <string>

void DisplayHelp();
int Main(int argc, char **argv);
#if defined( __WINDOWS__ )
#include <Windows.h>
BOOL CtrlHandler(DWORD fdwCtrlType); // Handle Ctrl+C etc
#else
// TODO:MAC Implement CtrlHandler
// TODO:LINUX Implement CtrlHandler
#endif

// Global Data
//------------------------------------------------------------------------------

// DisplayHelp
//------------------------------------------------------------------------------
void DisplayHelp()
{
	OUTPUT_DIRECT(
		"----------------------------------------------------------------------\n"
		"Usage: FBConsole.exe [Options]\n"
		"----------------------------------------------------------------------\n"
		"Options:\n"
		" -native  [devenv|msbuild|ndk] Only support devenv/msbuild/ndk-build currently (default devenv).\n"
		" -sln     [solution file] MSBuild solution file or NDK build directory.\n"
		" -proj    [project] MSBuild project or NDK project.\n"
		" -config  [configuration|platform] Build config (e.g. Debug|Win32).\n"
		" -target  [build|rebuild|clean] Build target.\n"
		" -ndkpath [ndk path] ndk path (default use env PATH)\n"
		" -args    [devenv|msbuild|ndk arguments] Original build args, should be wrappered with \".\n"
		" -fbargs  [fastbuld args] Fastbuild args, should be wrappered with \".\n"
		" -bkpath  [brokerage path] Fastbuild brokerage path.\n"
		" -debug   [use debug mode] If use debug mode to run this console.\n"
	);
}

// CtrlHandler
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
BOOL CtrlHandler(DWORD fdwCtrlType)
{
	// tell FBuild we want to stop the build cleanly
	BuildTask::AbortBuild();

	// only printf output for the first break received
	static bool received = false;
	if (received == false)
	{
		received = true;

		// get the console colours
		CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
		VERIFY(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &consoleInfo));

		// print a big red msg
		VERIFY(SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED));
		OUTPUT_DIRECT("<<<< ABORT SIGNAL RECEIVED >>>>\n");

		// put the console back to normal
		VERIFY(SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), consoleInfo.wAttributes));
	}

	return TRUE; // tell Windows we've "handled" it
}
#endif

// main
//------------------------------------------------------------------------------
int main(int argc, char **argv)
{
	InitLogger();
	int ret = Main(argc, argv);
	FinalizeLogger();
	return ret;
}

// Main
//------------------------------------------------------------------------------
int Main(int argc, char **argv)
{
	DEBUG("Arg count: %d\n", argc);

	Timer t;

#if defined( __WINDOWS__ )
	VERIFY(SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE)); // Register
#endif

	std::vector<std::string> buildOptions;
	std::string buildTool;
	std::string buildToolPath;
	std::string buildSlnPath;
	std::string buildProject;
	std::string buildConfig;
	std::string buildTarget;
	std::string oriBuildArgs;
	std::string fbuildArgs;
	std::string inputFile;
	std::string brokeragePath;
	
	bool clear = false;
	bool quickMode = false;
	bool debugMode = false;

	std::string totalArgs;

	for (int i = 1; i < argc; ++i)
	{
		std::string curArg = argv[i];
		if (StrHelper::StrBeginWithI(curArg, "-"))
		{
			DEBUG("Parse arg %d: %s\n", i, curArg.c_str());
			if (curArg == "-native")
			{
				int index = i + 1;
				if (index >= argc)
				{
					FATALERROR("FBConsole: Error: Missing <build tool> for '-build' argument\n");
					INFO("Try \"FBConsole.exe -help\"\n");
					return FB_CONSOLE_BAD_ARGS;
				}
				buildTool = argv[index];
				++i;
				continue;
			}
			else if (curArg == "-sln")
			{
				int index = i + 1;
				if (index >= argc)
				{
					FATALERROR("FBConsole: Error: Missing <build solution file> for '-sln' argument\n");
					INFO("Try \"FBConsole.exe -help\"\n");
					return FB_CONSOLE_BAD_ARGS;
				}
				buildSlnPath = argv[index];
				StrHelper::StrTrim(buildSlnPath, '"');
				++i;
				continue;
			}
			else if (curArg == "-proj")
			{
				int index = i + 1;
				if (index >= argc)
				{
					FATALERROR("FBConsole: Error: Missing <build project> for '-proj' argument\n");
					INFO("Try \"FBConsole.exe -help\"\n");
					return FB_CONSOLE_BAD_ARGS;
				}
				buildProject = argv[index];
				StrHelper::StrTrim(buildProject, '"');
				++i;
				continue;
			}
			else if (curArg == "-config")
			{
				int index = i + 1;
				if (index >= argc)
				{
					FATALERROR("FBConsole: Error: Missing <config> for '-config' argument\n");
					INFO("Try \"FBConsole.exe -help\"\n");
					return FB_CONSOLE_BAD_ARGS;
				}
				buildConfig = argv[index];
				StrHelper::StrTrim(buildConfig, '"');
				++i;
				continue;
			}
			else if (curArg == "-target")
			{
				int index = i + 1;
				if (index >= argc)
				{
					FATALERROR("FBConsole: Error: Missing <target> for '-target' argument\n");
					INFO("Try \"FBConsole.exe -help\"\n");
					return FB_CONSOLE_BAD_ARGS;
				}
				buildTarget = argv[index];
				StrHelper::StrTrim(buildTarget, '"');
				++i;
				continue;
			}
			else if (curArg == "-ndkpath")
			{
				int index = i + 1;
				if (index >= argc)
				{
					FATALERROR("FBConsole: Error: Missing <ndkpath> for '-ndkpath' argument\n");
					INFO("Try \"FBConsole.exe -help\"\n");
					return FB_CONSOLE_BAD_ARGS;
				}
				buildToolPath = argv[index];
				StrHelper::StrTrim(buildToolPath, '"');
				++i;
				continue;
			}
			else if (curArg == "-args")
			{
				int index = i + 1;
				if (index < argc)
				{
					oriBuildArgs = argv[index];
					StrHelper::StrTrim(oriBuildArgs, '"');
				}
				++i;
				continue;
			}
			else if (curArg == "-fbargs")
			{
				int index = i + 1;
				if (index < argc)
				{
					fbuildArgs = argv[index];
					StrHelper::StrTrim(fbuildArgs, '"');
				}
				++i;
				continue;
			}
			else if (curArg == "-bkpath")
			{
				int index = i + 1;
				if (index < argc)
				{
					brokeragePath = argv[index];
					StrHelper::StrTrim(brokeragePath, '"');
				}
				++i;
				continue;
			}
			else if (curArg == "-quick")
			{
				quickMode = true;
				continue;
			}
			else if (curArg == "-debug")
			{
				debugMode = true;
				continue;
			}
			else if (curArg == "--clear")
			{
				clear = true;
				continue;
			}
			else if (curArg == "-h" || curArg == "-help")
			{
				DisplayHelp();
				return FB_CONSOLE_BUILD_OK;
			}
			else
			{
				WARNING("Unknown parameter: %s\n", curArg.c_str());
			}
		}
		else
		{
			FATALERROR("FBConsole: Error: Unknown '%s' argument\n", curArg.c_str());
			INFO("Try \"FBConsole.exe -help\"\n");
			return 0;
		}
	}

	for (int i = 1; i < argc; ++i)
	{
		totalArgs += argv[i];
		totalArgs += " ";
	}

	if (buildTool.empty())
	{
		FATALERROR("Not input navtive build tool!\n");
		return FB_CONSOLE_BAD_ARGS;
	}

	DEBUG("Build tool: %s\n", buildTool.c_str());
	DEBUG("Build tool args: %s\n", oriBuildArgs.c_str());
	DEBUG("Build tool solution file: %s\n", buildSlnPath.c_str());
	DEBUG("Build tool project: %s\n", buildProject.c_str());
	DEBUG("Build tool build target: %s\n", buildTarget.c_str());

	int retCode;

	if (clear)
	{
		INFO("Run clean up\n");
		if (buildTool == "msbuild" || buildTool == "devenv") 
		{
			MSBuildTask buildTask("c:\\fastbuild",
				buildSlnPath,
				buildProject,
				buildConfig,
				buildTarget,
				oriBuildArgs,
				fbuildArgs,
				brokeragePath,
				buildTool == "msbuild",
				quickMode,
				debugMode);
			buildTask.SetTotalArgs(totalArgs);
			buildTask.Cleanup();
			retCode = buildTask.GetExitState();
		}
		else if (buildTool == "ndk") 
		{
			NDKBuildTask buildTask("c:\\fastbuild",
				buildToolPath,
				buildSlnPath,
				buildProject,
				buildTarget,
				oriBuildArgs,
				fbuildArgs,
				brokeragePath,
				quickMode,
				debugMode);
			buildTask.SetTotalArgs(totalArgs);
			buildTask.Cleanup();
			retCode = buildTask.GetExitState();
		}
	}
	else if (buildTool == "msbuild" || buildTool == "devenv")
	{
		MSBuildTask buildTask("c:\\fastbuild", 
							buildSlnPath, 
							buildProject,
							buildConfig, 
							buildTarget, 
							oriBuildArgs, 
							fbuildArgs, 
							brokeragePath, 
							buildTool == "msbuild",
							quickMode,
							debugMode);
		buildTask.SetTotalArgs(totalArgs);
		buildTask.DoBuild();
		retCode = buildTask.GetExitState();
	}
	else if (buildTool == "ndk") 
	{
		NDKBuildTask buildTask("c:\\fastbuild",
			buildToolPath,
			buildSlnPath,
			buildProject,
			buildTarget,
			oriBuildArgs,
			fbuildArgs,
			brokeragePath,
			quickMode,
			debugMode);
		buildTask.SetTotalArgs(totalArgs);
		buildTask.DoBuild();
		retCode = buildTask.GetExitState();
	}

	// final line of output - status of build
	float totalBuildTime = t.GetElapsed(); // FBuildStats::GetTotalBuildTimeS();
	uint32_t minutes = uint32_t(totalBuildTime / 60.0f);
	totalBuildTime -= (minutes * 60.0f);
	float seconds = totalBuildTime;

	INFO("\n");
	INFO("===========================================\n");
	INFO("Build finished, exit state: %d\n", retCode);
	if (minutes > 0)
	{
		INFO("Total Time: %um %05.3fs\n", minutes, seconds);
	}
	else
	{
		INFO("Total Time: %05.3fs\n", seconds);
	}
	INFO("===========================================\n");
	
	return retCode;
}


//------------------------------------------------------------------------------
