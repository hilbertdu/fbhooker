//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Macros.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Macros.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FCONSOLE_COMMON_H
#define FCONSOLE_COMMON_H

#include "Core/Utils/Macros.h"
#include "Core/Utils/Utils.h"
#include "Core/Logger/Tracing.h"
#include "Core/Env/Assert.h"
#include "Core/Env/Environment.h"
#include "Core/Process/Process.h"
#include "Core/Process/SharedMemory.h"
#include "Core/Process/SystemMutex.h"
#include "Core/Process/Thread.h"
#include "Core/Helper/DumpHelper.h"
#include "Core/Container/Singleton.h"
#include "Core/ToolChain/ToolChain.h"
#include "Core/Time/Timer.h"

// Error
//------------------------------------------------------------------------------
enum {
	FB_CONSOLE_BUILD_OK			= 0,
	FB_CONSOLE_BUILD_FAILED		= -1,
	FB_CONSOLE_BUILD_CANCELLED	= -2,
	FB_CONSOLE_BAD_ARGS			= -3,

	FB_FILE_PARSE_FAILED		= -10,
	FB_FILE_READ_FAILED			= -11,
	FB_FILE_REWRITE_FAILED		= -12,
};

// Functions
//------------------------------------------------------------------------------
static void InitLogger();
static void FinalizeLogger();

// InitLogger
//------------------------------------------------------------------------------
/*static*/ void InitLogger()
{
#ifdef _DEBUG
	Tracing::showDebug = true;
	Tracing::logFilePath = "build.log";
	Tracing::outputToFile = true;
#endif
	Tracing::showInfo = true;
	Tracing::Init();
}

// FinalizeLogger
//------------------------------------------------------------------------------
/*static*/ void FinalizeLogger()
{
	Tracing::Finalize();
}

#endif	// FCONSOLE_COMMON_H
