//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: MSBuild.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/05
//*******************************************************

// Includes
//------------------------------------------------------------------------------
#include "MSBuild.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <regex>

// Constructor
//------------------------------------------------------------------------------
MSBuildTask::MSBuildTask(const std::string &servicePath,
	const std::string &slnPath,
	const std::string &project,
	const std::string &buildConfig,
	const std::string &buildTarget,
	const std::string &args,
	const std::string &fbuildArgs,
	const std::string &brokeragePath,
	bool useMSBuild,
	bool quickMode,
	bool debugMode) :
	solutionFile(slnPath),
	buildProject(project),
	buildTarget(buildTarget),
	buildArgs(args),
	useMSBuild(useMSBuild),
	BuildTask(servicePath, fbuildArgs, brokeragePath, quickMode, debugMode)
{
	MSBuildTargetsDir = "C:\\Program Files (x86)\\MSBuild\\Microsoft.Cpp\\v4.0";
	HookerTargetsFileDir = servicePath + "\\MSBuildCppTargets";
	HookerTargetsDir = servicePath + "\\MSBuild";

	FileDirHelper::TransToFullPath(solutionFile, FileDirHelper::GetCurrentDir());
	FileDirHelper::GetDirFromPath(solutionDir, solutionFile);
	FileDirHelper::GetFileNameFromPath(solutionName, solutionFile, false);

	hookSolutionPath = solutionDir + "\\" + solutionName + "_fb.sln";

	std::vector<std::string> configVec;
	StrHelper::StrSplit(buildConfig, '|', configVec);
	if (configVec.size() == 2)
	{
		configuration = configVec[0];
		platform = configVec[1];
	}

	SetWorkingDir(solutionDir);

	INFO("Solution file path: %s\n", solutionFile.c_str());
	INFO("Solution file dir: %s\n", solutionDir.c_str());
}

// Deconstructor
//------------------------------------------------------------------------------
MSBuildTask::~MSBuildTask()
{

}

// CheckParamValid
//------------------------------------------------------------------------------
bool MSBuildTask::CheckParamValid()
{
	if (!FileDirHelper::FileExists(solutionFile))
	{
		FATALERROR("Solution file is not exist: %s\n", solutionFile.c_str());
		return false;
	}
	if (buildTarget.empty())
	{
		FATALERROR("Please specify build target: [build|rebuild|clean]\n");
		return false;
	}
	if (configuration.empty() || platform.empty())
	{
		FATALERROR("Please specify configuration and platform like: [Debug|Win32]\n");
		return false;
	}
	return true;
}

// CopyHookerTools
//------------------------------------------------------------------------------
/*virtual*/ void MSBuildTask::CopyHookerTools()
{
	if (debugMode)
	{
		FileDirHelper::FileCopy(hooker1Path, serviceDir + "\\cl.exe");
		FileDirHelper::FileCopy(hooker1Path, serviceDir + "\\lib.exe");
		FileDirHelper::FileCopy(hooker1Path, serviceDir + "\\link.exe");
	}
	else
	{
		FileDirHelper::FileCopy(hooker0Path, serviceDir + "\\cl.exe");
		FileDirHelper::FileCopy(hooker0Path, serviceDir + "\\lib.exe");
		FileDirHelper::FileCopy(hooker0Path, serviceDir + "\\link.exe");
	}

	// Copy "C:\Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0" to hooker path
	FileDirHelper::DirectoryCreate(HookerTargetsDir);
	FileDirHelper::DirectoryCopy(MSBuildTargetsDir, HookerTargetsDir);
	FileDirHelper::DirectoryCopy(HookerTargetsFileDir, HookerTargetsDir);
}

// ClearHookerTools
//------------------------------------------------------------------------------
/*virtual*/ void MSBuildTask::ClearHookerTools()
{
	DEBUG("Clear hooker tools\n");
	FileDirHelper::FileDelete(serviceDir + "\\cl.exe");
	FileDirHelper::FileDelete(serviceDir + "\\lib.exe");
	FileDirHelper::FileDelete(serviceDir + "\\link.exe");

	FileDirHelper::DirectoryDelete(HookerTargetsDir);
	if (!debugMode)
	{
		for (ProjectIter iter = projects.begin(); iter != projects.end(); ++iter)
		{
			FileDirHelper::FileDelete((*iter).second.fullReplacedPath);
		}
		FileDirHelper::FileDelete(hookSolutionPath);
		FileDirHelper::FileDelete(solutionDir + "\\" + solutionName + "_fb.sdf");
	}
}

// HookerCppTargets
//------------------------------------------------------------------------------
void MSBuildTask::HookerCppTargets()
{
	static const std::vector<std::string> supportVers = { "v110", "v120", "v140" };
	for (std::vector<std::string>::const_iterator iter = supportVers.begin(); iter != supportVers.end(); ++iter)
	{
		// TODO
	}
}

// DoBuild
//------------------------------------------------------------------------------
/*virtual*/ void MSBuildTask::DoBuild()
{
	CHECK_CONDITION(CheckParamValid());

	INFO("MSBuild task do build\n");
	ParseSolution();
	RewriteSolution();
	RewriteVCProject();

	BuildTask::PreBuild();
	BuildTask::DoBuild();
	StartMSBuild();
	BuildTask::PostBuild();
}

// GetServiceArgs
//------------------------------------------------------------------------------
/*virtual*/ void MSBuildTask::GetServiceArgs(std::string &args)
{
	BuildTask::GetServiceArgs(args);
	args += " /EXTRAOPT:\"\"\"/nologo\"\"\"";
}

// Parser
//------------------------------------------------------------------------------
void MSBuildTask::ParseSolution()
{
	CHECK_CONDITION(ExitStateValid());

	bool ret = FileDirHelper::FileReadAll(solutionFile, solutionContent);
	if (!ret)
	{
		FATALERROR("Can't read solution file: %s\n", solutionFile.c_str());
		exitState = FB_FILE_READ_FAILED;
		return;
	}

	// Get visual studio version
	//static const std::regex versionPattern("VisualStudioVersion\\s*=\\s*(.*)");
	static const std::regex versionPattern("# Visual Studio (.*)");
	std::smatch versionMatches;
	std::string vsVersion;

	if (std::regex_search(solutionContent, versionMatches, versionPattern))
	{
		vsVersion = versionMatches[1].str();
		INFO("Visual studio version: %s\n", vsVersion.c_str());
		ExecutablePath = GetBuildExePath(vsVersion);
	}
	else
	{
		exitState = FB_FILE_PARSE_FAILED;
	}
	CHECK_CONDITION(ExitStateValid());

	// Find all vcproj file path to be build
	static const std::regex pattern("Project\\(\"(.*)\"\\)\\s*=\\s*\"(.*)\"\\s*,\\s*\"(.*)\"\\s*,\\s*\"\\{(.*)\\}\"");
	std::string s = solutionContent;
	std::smatch matches;

	while (std::regex_search(s, matches, pattern))
	{
		if (matches.size() != 5)
		{
			FATALERROR("Can't recognize solution file format!\n");
			exitState = FB_FILE_PARSE_FAILED;
			return;
		}

		if (!StrHelper::StrEndWithI(matches[3], ".vcxproj"))
		{
			DEBUG("Meet a non vc project file: %s\n", matches[3].str().c_str());
			s = matches.suffix().str();
			continue;
		}

		std::string vcxprojFile = matches[3];
		FileDirHelper::TransToFullPath(vcxprojFile, solutionDir);
		VCProjectInfo info = {
			matches[2].str(), matches[4].str(), matches[3], vcxprojFile
		};

		projects[info.guid] = info;
		s = matches.suffix().str();
	}

	// Initialize projects info
	for (ProjectIter iter = projects.begin(); iter != projects.end(); ++iter)
	{
		std::string vcxprojFileName, vcxprojFileDir, fullReplacedPath, patternStr, replaceStr;
		FileDirHelper::GetFileNameFromPath(vcxprojFileName, (*iter).second.path, false);
		FileDirHelper::GetDirFromPath(vcxprojFileDir, (*iter).second.path);
		(*iter).second.replacedPath = vcxprojFileDir + "\\" + vcxprojFileName + "_fb.vcxproj";
		(*iter).second.fullReplacedPath = (*iter).second.replacedPath;
		FileDirHelper::TransToFullPath((*iter).second.fullReplacedPath, solutionDir);
		DEBUG("Initialize projects info: %s\n", (*iter).second.fullPath.c_str());
	}
}

// RewriteSolution
//------------------------------------------------------------------------------
void MSBuildTask::RewriteSolution()
{
	CHECK_CONDITION(ExitStateValid());

	INFO("Rewrite solution files\n");
	static const std::regex pattern("Project\\(\"(.*)\"\\)\\s*=\\s*\"(.*)\"\\s*,\\s*\"(.*)\\.vcxproj\"\\s*,\\s*\"\\{(.*)\\}\"");
	static const std::string replaced = "Project(\"$1\") = \"$2\", \"$3_fb.vcxproj\", \"{$4}\"";
	std::string content = solutionContent;

	content = std::regex_replace(content, pattern, replaced);

	bool ret = FileDirHelper::FileWrite(hookSolutionPath, content);
	if (!ret)
	{
		FATALERROR("Error rewrite solution file: %s\n", hookSolutionPath.c_str());
		exitState = FB_FILE_REWRITE_FAILED;
	}
}

// GenerateNew
//------------------------------------------------------------------------------
void MSBuildTask::RewriteVCProject()
{
	CHECK_CONDITION(ExitStateValid());

	INFO("Rewrite vc project files\n");

	// Combine guid string
	std::string guids;
	for (ProjectIter tmpiter = projects.begin(); tmpiter != projects.end(); ++tmpiter)
	{
		guids += (*tmpiter).second.guid + "|";
	}
	StrHelper::StrTrim(guids, '|');

	// Static regex
	static const std::regex globalPropsPattern("<PropertyGroup\\s*Label=\"Globals\">([^]*?)<\\/PropertyGroup>");
	static const char * replacedFormat = "<PropertyGroup Label=\"Globals\">$1%s\n%s\n%s\n%s\n  </PropertyGroup>";
	static const char *referenceFormat = "<ProjectReference\\s*Include\\s*=\\s*\"(.*)\\.vcxproj\">[^]*?<Project>\\{(%s)\\}<\\/Project>[^]*?<\\/ProjectReference>";
	static const std::string replacedReference = "<ProjectReference Include=\"$1_fb.vcxproj\">\n      "
												 "<Project>{$2}</Project>\n    "
												 "</ProjectReference>";

	std::string referenceStr;
	StrHelper::StrFormat(referenceStr, referenceFormat, guids.c_str());

	for (ProjectIter iter = projects.begin(); iter != projects.end(); ++iter)
	{
		std::string content;
		bool ret = FileDirHelper::FileReadAll((*iter).second.fullPath, content);
		if (!ret)
		{
			FATALERROR("Error read vcproj file: %s\n", (*iter).second.fullPath.c_str());
			exitState = FB_FILE_READ_FAILED;
			return;
		}

		// Get project build toolset version
		(*iter).second.toolsetVer = GetToolsetVersion(content);

		
		// Replace targets path
		static const std::regex cppTargetPattern("\\$\\(VCTargetsPath\\)\\\\Microsoft\\.Cpp\\.targets");
		std::string replacedStr;

		if ((*iter).second.toolsetVer == "v110")
		{
			replacedStr = HookerTargetsDir + "\\V110\\Microsoft.Cpp.targets";
		}
		else if ((*iter).second.toolsetVer == "v120")
		{
			replacedStr = HookerTargetsDir + "\\V120\\Microsoft.Cpp.targets";
		}
		else if ((*iter).second.toolsetVer == "v140")
		{
			replacedStr = HookerTargetsDir + "\\V140\\Microsoft.Cpp.targets";
		}
		else
		{
			FATALERROR("Can not recognize toolset version: %s\n", (*iter).second.toolsetVer.c_str());
			exitState = FB_FILE_PARSE_FAILED;
			return;
		}

		content = std::regex_replace(content, cppTargetPattern, replacedStr);// , std::regex_constants::format_no_copy);
		

		// Add new properties
		static const std::regex globalPropsPattern("<PropertyGroup\\s*Label=\"Globals\">([^]*?)<\\/PropertyGroup>");
		static const char * replacedFormat = "<PropertyGroup Label=\"Globals\">$1%s\n%s\n%s\n%s\n%s\n%s\n  </PropertyGroup>";

		StrHelper::StrFormat(replacedStr, replacedFormat, 
			("  <ProjectName>" + (*iter).second.name + "</ProjectName>").c_str(),
			("    <SolutionName>" + solutionName + "</SolutionName>").c_str(),
			("    <SolutionFileName>" + solutionName + ".vcxproj" + "</SolutionFileName>").c_str(),
			("    <SolutionPath>" + solutionFile + "</SolutionPath>").c_str(),
			("    <VCTargetsPath>" + HookerTargetsDir + "\\" + (*iter).second.toolsetVer + "</VCTargetsPath>").c_str(),
			("    <TrackFileAccess>False</TrackFileAccess>")
			);
		content = std::regex_replace(content, globalPropsPattern, replacedStr);

		// Replace reference projects
		content = std::regex_replace(content, std::regex(referenceStr, std::regex::ECMAScript | std::regex::icase), replacedReference);

		// Write new project file
		ret = FileDirHelper::FileWrite((*iter).second.fullReplacedPath, content);
		if (!ret)
		{
			FATALERROR("Error rewrite vcproj file: %s\n", (*iter).second.fullReplacedPath.c_str());
			exitState = FB_FILE_REWRITE_FAILED;
			return;
		}
	}
}

// InitToolsetVersion
//------------------------------------------------------------------------------
std::string MSBuildTask::GetToolsetVersion(const std::string &projectContent)
{
	//static const std::regex patternToolsetVer("<Project DefaultTargets\\s*=\\s*\"(.*)\"\\s*ToolsVersion\\s*=\\s*\"([\\d\\.]*)\"\\s*(.*)>");
	static const char * patternFormat("<PropertyGroup\\s*Condition\\s*=\\s*\"'\\$\\(Configuration\\)\\|\\$\\(Platform\\)'\\s*==\\s*'%s\\|%s'\"\\s*Label\\s*=\\s*\"Configuration\">"
									  "[^]*?<PlatformToolset>(.*?)<\\/PlatformToolset>[^]*?<\\/PropertyGroup>");

	std::string patternToolsetVerStr;
	std::smatch result;

	StrHelper::StrFormat(patternToolsetVerStr, patternFormat, configuration.c_str(), platform.c_str());
	std::regex patternToolsetVer(patternToolsetVerStr, std::regex::ECMAScript | std::regex::icase);

	if (std::regex_search(projectContent, result, patternToolsetVer))
	{
		std::string toolsetVer = result[1];
		DEBUG("GetToolsetVersion: [%s|%s] %s\n", configuration.c_str(), platform.c_str(), toolsetVer.c_str());
		return toolsetVer;
	}
	else
	{
		FATALERROR("Can not get toolset version: [%s|%s]\n", configuration.c_str(), platform.c_str());
		exitState = FB_FILE_PARSE_FAILED;
		return "";
	}
}

// GetBuildExePath
//------------------------------------------------------------------------------
std::string MSBuildTask::GetBuildExePath(const std::string &toolsetVer)
{
	std::string exePath;
	if (useMSBuild)
	{
		if (StrHelper::StrBeginWithI(toolsetVer, "11.0") || toolsetVer == "2012")
		{
			exePath = "C:\\Program Files (x86)\\MSBuild\\11.0\\Bin\\MSBuild.exe";
		}
		else if (StrHelper::StrBeginWithI(toolsetVer, "12.0") || toolsetVer == "2013")
		{
			exePath = "C:\\Program Files (x86)\\MSBuild\\12.0\\Bin\\MSBuild.exe";
		}
		else if (StrHelper::StrBeginWithI(toolsetVer, "14.0") || toolsetVer == "14")
		{
			exePath = "C:\\Program Files (x86)\\MSBuild\\14.0\\Bin\\MSBuild.exe";
		}
		else
		{
			FATALERROR("Can not found msbuild exe path\n");
			exitState = FB_CONSOLE_BUILD_FAILED;
			return "";
		}

		if (!FileDirHelper::FileExists(exePath))
		{
			exePath = "C:\\Program Files (x86)\\MSBuild\\12.0\\Bin\\MSBuild.exe";
		}
	}
	else
	{
		if (StrHelper::StrBeginWithI(toolsetVer, "11.0") || toolsetVer == "2012")
		{
			exePath = VCToolChain::GetVSInstallDir("110") + "\\Common7\\IDE\\devenv.com";
		}
		else if (StrHelper::StrBeginWithI(toolsetVer, "12.0") || toolsetVer == "2013")
		{
			exePath = VCToolChain::GetVSInstallDir("120") + "\\Common7\\IDE\\devenv.com";
		}
		else if (StrHelper::StrBeginWithI(toolsetVer, "14.0") || toolsetVer == "14")
		{
			exePath = VCToolChain::GetVSInstallDir("140") + "\\Common7\\IDE\\devenv.com";
		}
		else
		{
			FATALERROR("Can not found vs install path, please check if you have installed visual studio version %s\n", toolsetVer.c_str());
			exitState = FB_CONSOLE_BUILD_FAILED;
			return "";
		}
	}
	
	return exePath;
}

// GenerateNew
//------------------------------------------------------------------------------
void MSBuildTask::StartMSBuild()
{
	CHECK_CONDITION(ExitStateValid());

	INFO("Start native build: %s %s\n", ExecutablePath.c_str(), (hookSolutionPath + " " + buildArgs).c_str());

	std::string extraArgs;
	if (useMSBuild)
	{
		if (!buildProject.empty())
		{
			extraArgs += " /t:\"" + buildProject + ":" + buildTarget + "\"";
		}
		else
		{
			extraArgs += " /t:" + buildTarget;
		}
		extraArgs += " /p:Configuration=" + configuration + ";Platform=" + platform;
		extraArgs += " /nologo /v:m";
	}
	else
	{
		if (!buildProject.empty())
		{
			extraArgs += " /Project " + buildProject;
		}
		extraArgs += " /" + buildTarget;
		extraArgs += " " + configuration + "|" + platform;
	}

#if defined( __WINDOWS__ )
	nativeProc.CreateJob();
#endif
	exitState = nativeProc.Spawn(ExecutablePath.c_str(), (hookSolutionPath + " " + buildArgs + extraArgs).c_str(), nullptr, nullptr, false);
	if (exitState == PROCESS_CREATE_OK)
	{
		INFO("Create build process OK!\n");

#if defined( __WINDOWS__ )
		size_t bufferSize = MEGABYTE;
		size_t readSize = -1;
		char *output = new char[bufferSize]();
		for (; nativeProc.IsRunning() || readSize != 0;)
		{
			readSize = nativeProc.ReadStdOut(output, bufferSize - 1);
			output[readSize] = '\000';
			OUTPUT_DIRECT(output);
			Thread::Sleep(15);
		}
		SAFE_DELETE_ARRAY(output);

		ASSERT(!nativeProc.IsRunning());
#endif

		exitState = nativeProc.WaitForExit();

		if (exitState == FB_CONSOLE_BUILD_OK)
		{
			INFO("Build exit OK\n");
			return;
		}
		else if (exitState == FB_CONSOLE_BUILD_CANCELLED)
		{
			INFO("Build cancelled\n");
			exitState = FB_CONSOLE_BUILD_CANCELLED;
			return;
		}
		else
		{
			FATALERROR("Failed to build, build exit code: %d\n", exitState);
			exitState = FB_CONSOLE_BUILD_FAILED;
			return;
		}
	}
	else
	{
		FATALERROR("Create build process failed!\n");
	}
}

// Cleanup
//------------------------------------------------------------------------------
/*virtual*/ void MSBuildTask::Cleanup()
{
	for (uint16_t idx = 0; idx < 100; ++idx)
	{
		ClearHookerTools();
		if (FileDirHelper::FileExists(serviceDir + "\\cl.exe") ||
			FileDirHelper::FileExists(serviceDir + "\\lib.exe") ||
			FileDirHelper::FileExists(serviceDir + "\\link.exe") ||
			FileDirHelper::DirectoryExist(HookerTargetsDir))
		{
			Thread::Sleep(100);
			continue;
		}
		else
		{
			return;
		}
	}
}
