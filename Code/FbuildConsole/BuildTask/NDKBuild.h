
// NDKBuild.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FCONSOLE_NDKBUILD_H
#define FCONSOLE_NDKBUILD_H

// Includes
//------------------------------------------------------------------------------
#include "BuildTask.h"

class NDKBuildTask : public BuildTask
{
public:
	NDKBuildTask(const std::string &servicePath,
				const std::string &buildToolPath,
				const std::string &solutionDir,
				const std::string &projectName,
				const std::string &buildTarget,
				const std::string &args,
				const std::string &fbuildArgs,
				const std::string &brokeragePath,
				bool quickMode,
				bool debugMode);
	~NDKBuildTask();

	virtual void DoBuild();
	virtual void Cleanup();

	int GetExitState() { return exitState; };

private:
	bool CheckParamValid();

	virtual void GetServiceArgs(std::string &args);
	virtual void CopyHookerTools();
	virtual void ClearHookerTools();

	void InitToolchainDir();
	void StartNDKBuild();
	void HookOneFile(const std::string &hookerPath, const std::string &origFilePath, const std::string &copyPath);
	void UnhookOneFile(const std::string &origFilePath, const std::string &copyPath);
	bool IsHooked();

	std::string ExecutablePath;

	std::string ndkPath;
	std::string toolchainDir;
	std::string toolchain2Dir;
	std::string buildArgs;
	std::string buildTarget;
};

#endif	// FCONSOLE_NDKBUILD_H