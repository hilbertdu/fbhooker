
// Includes
//------------------------------------------------------------------------------
#include "NDKBuild.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <regex>

// Constructor
//------------------------------------------------------------------------------
NDKBuildTask::NDKBuildTask(const std::string &servicePath,
	const std::string &buildToolPath,
	const std::string &solutionDir,
	const std::string &projectName,
	const std::string &buildTarget,
	const std::string &args,
	const std::string &fbuildArgs,
	const std::string &brokeragePath,
	bool quickMode,
	bool debugMode) :
	ndkPath(buildToolPath),
	buildArgs(args),
	buildTarget(buildTarget),
	BuildTask(servicePath, fbuildArgs, brokeragePath, quickMode, debugMode)
{
	INFO("build project path: %s\n", solutionDir.c_str());
	this->solutionDir = solutionDir;
	this->solutionName = projectName;
	SetWorkingDir(solutionDir);
	InitToolchainDir();

	ExecutablePath = ndkPath + "\\ndk-build.cmd";
}

// Deconstructor
//------------------------------------------------------------------------------
NDKBuildTask::~NDKBuildTask()
{

}

// CheckParamValid
//------------------------------------------------------------------------------
bool NDKBuildTask::CheckParamValid()
{
	if (!FileDirHelper::DirectoryExist(solutionDir))
	{
		FATALERROR("Solution dir is not exist: %s\n", solutionDir.c_str());
		return false;
	}
	if (buildTarget.empty())
	{
		FATALERROR("Please specify build target: [build|rebuild|clean]\n");
		return false;
	}
	if (!FileDirHelper::FileExists(ExecutablePath))
	{
		ExecutablePath = ndkPath + "\\ndk-build";
		if (!FileDirHelper::FileExists(ExecutablePath))
		{
			FATALERROR("Ndk-build not found!\n");
			return false;
		}
	}
	return true;
}

void NDKBuildTask::InitToolchainDir() {
	if (ndkPath.empty()) 
	{
		INFO("ndkPath is empty!");
	}
	else 
	{
		toolchainDir = ndkPath + "\\toolchains\\arm-linux-androideabi-4.6\\prebuilt\\windows-x86_64\\bin";
		toolchain2Dir = ndkPath + "\\toolchains\\arm-linux-androideabi-4.8\\prebuilt\\windows-x86_64\\bin";
	}
}

// CopyHookerTools
//------------------------------------------------------------------------------
/*virtual*/ void NDKBuildTask::CopyHookerTools()
{
	if (IsHooked())
	{
		ClearHookerTools();
	}
	if (debugMode)
	{
		HookOneFile(hooker1Path, toolchainDir + "\\arm-linux-androideabi-ar.exe", toolchainDir + "\\arm-linux-androideabi-ar.ar");
		HookOneFile(hooker1Path, toolchainDir + "\\arm-linux-androideabi-gcc.exe", toolchainDir + "\\arm-linux-androideabi-gcc.gcc");
		HookOneFile(hooker1Path, toolchainDir + "\\arm-linux-androideabi-g++.exe", toolchainDir + "\\arm-linux-androideabi-g++.g++");
		FileDirHelper::FileCopy(toolchainDir + "\\arm-linux-androideabi-g++.g++", toolchainDir + "\\arm-linux-androideabi-g++.gcc");
		FileDirHelper::FileCopy(toolchainDir + "\\arm-linux-androideabi-gcc.gcc", toolchainDir + "\\arm-linux-androideabi-gcc.g++");

		HookOneFile(hooker1Path, toolchain2Dir + "\\arm-linux-androideabi-ar.exe", toolchain2Dir + "\\arm-linux-androideabi-ar.ar");
		HookOneFile(hooker1Path, toolchain2Dir + "\\arm-linux-androideabi-gcc.exe", toolchain2Dir + "\\arm-linux-androideabi-gcc.gcc");
		HookOneFile(hooker1Path, toolchain2Dir + "\\arm-linux-androideabi-g++.exe", toolchain2Dir + "\\arm-linux-androideabi-g++.g++");
		FileDirHelper::FileCopy(toolchain2Dir + "\\arm-linux-androideabi-g++.g++", toolchain2Dir + "\\arm-linux-androideabi-g++.gcc");
		FileDirHelper::FileCopy(toolchain2Dir + "\\arm-linux-androideabi-gcc.gcc", toolchain2Dir + "\\arm-linux-androideabi-gcc.g++");
	}
	else
	{
		HookOneFile(hooker0Path, toolchainDir + "\\arm-linux-androideabi-ar.exe", toolchainDir + "\\arm-linux-androideabi-ar.ar");
		HookOneFile(hooker0Path, toolchainDir + "\\arm-linux-androideabi-gcc.exe", toolchainDir + "\\arm-linux-androideabi-gcc.gcc");
		HookOneFile(hooker0Path, toolchainDir + "\\arm-linux-androideabi-g++.exe", toolchainDir + "\\arm-linux-androideabi-g++.g++");
		FileDirHelper::FileCopy(toolchainDir + "\\arm-linux-androideabi-g++.g++", toolchainDir + "\\arm-linux-androideabi-g++.gcc");
		FileDirHelper::FileCopy(toolchainDir + "\\arm-linux-androideabi-gcc.gcc", toolchainDir + "\\arm-linux-androideabi-gcc.g++");

		HookOneFile(hooker0Path, toolchain2Dir + "\\arm-linux-androideabi-ar.exe", toolchain2Dir + "\\arm-linux-androideabi-ar.ar");
		HookOneFile(hooker0Path, toolchain2Dir + "\\arm-linux-androideabi-gcc.exe", toolchain2Dir + "\\arm-linux-androideabi-gcc.gcc");
		HookOneFile(hooker0Path, toolchain2Dir + "\\arm-linux-androideabi-g++.exe", toolchain2Dir + "\\arm-linux-androideabi-g++.g++");
		FileDirHelper::FileCopy(toolchain2Dir + "\\arm-linux-androideabi-g++.g++", toolchain2Dir + "\\arm-linux-androideabi-g++.gcc");
		FileDirHelper::FileCopy(toolchain2Dir + "\\arm-linux-androideabi-gcc.gcc", toolchain2Dir + "\\arm-linux-androideabi-gcc.g++");
	}
}

void NDKBuildTask::HookOneFile(const std::string &hookerPath, const std::string &origFilePath, const std::string &copyPath)
{
	FileDirHelper::FileCopy(origFilePath, copyPath);
	FileDirHelper::FileCopy(hookerPath, origFilePath);
}

void NDKBuildTask::UnhookOneFile(const std::string &origFilePath, const std::string &copyPath)
{
	if (FileDirHelper::FileExists(copyPath))
	{
		FileDirHelper::FileDelete(origFilePath);
		FileDirHelper::FileCopy(copyPath, origFilePath);
		FileDirHelper::FileDelete(copyPath);
	}
}

// ClearHookerTools
//------------------------------------------------------------------------------
/*virtual*/ void NDKBuildTask::ClearHookerTools()
{
	DEBUG("Clear hooker tools\n");
	UnhookOneFile(toolchainDir + "\\arm-linux-androideabi-ar.exe", toolchainDir + "\\arm-linux-androideabi-ar.ar");
	UnhookOneFile(toolchainDir + "\\arm-linux-androideabi-gcc.exe", toolchainDir + "\\arm-linux-androideabi-gcc.gcc");
	UnhookOneFile(toolchainDir + "\\arm-linux-androideabi-g++.exe", toolchainDir + "\\arm-linux-androideabi-g++.g++");
	FileDirHelper::FileDelete(toolchainDir + "\\arm-linux-androideabi-g++.gcc");
	FileDirHelper::FileDelete(toolchainDir + "\\arm-linux-androideabi-gcc.g++");
	UnhookOneFile(toolchain2Dir + "\\arm-linux-androideabi-ar.exe", toolchain2Dir + "\\arm-linux-androideabi-ar.ar");
	UnhookOneFile(toolchain2Dir + "\\arm-linux-androideabi-gcc.exe", toolchain2Dir + "\\arm-linux-androideabi-gcc.gcc");
	UnhookOneFile(toolchain2Dir + "\\arm-linux-androideabi-g++.exe", toolchain2Dir + "\\arm-linux-androideabi-g++.g++");
	FileDirHelper::FileDelete(toolchain2Dir + "\\arm-linux-androideabi-g++.gcc");
	FileDirHelper::FileDelete(toolchain2Dir + "\\arm-linux-androideabi-gcc.g++");
}

bool NDKBuildTask::IsHooked() 
{
	return FileDirHelper::FileExists(toolchainDir + "\\arm-linux-androideabi-ar.ar") ||
		FileDirHelper::FileExists(toolchainDir + "\\arm-linux-androideabi-gcc.gcc") ||
		FileDirHelper::FileExists(toolchainDir + "\\arm-linux-androideabi-g++.g++") || 
		FileDirHelper::FileExists(toolchain2Dir + "\\arm-linux-androideabi-ar.ar") ||
		FileDirHelper::FileExists(toolchain2Dir + "\\arm-linux-androideabi-gcc.gcc") ||
		FileDirHelper::FileExists(toolchain2Dir + "\\arm-linux-androideabi-g++.g++");
}

// DoBuild
//------------------------------------------------------------------------------
/*virtual*/ void NDKBuildTask::DoBuild()
{
	CHECK_CONDITION(CheckParamValid());

	INFO("NDKBuild task do build\n");

	BuildTask::PreBuild();
	BuildTask::DoBuild();
	StartNDKBuild();
	BuildTask::PostBuild();
}

// GetServiceArgs
//------------------------------------------------------------------------------
/*virtual*/ void NDKBuildTask::GetServiceArgs(std::string &args)
{
	BuildTask::GetServiceArgs(args);
}

// GenerateNew
//------------------------------------------------------------------------------
void NDKBuildTask::StartNDKBuild()
{
	CHECK_CONDITION(ExitStateValid());

	if (buildTarget == "rebuild")
	{
		buildArgs += " -B";
	}
	else if (buildTarget == "clean")
	{
		buildArgs += " clean";
	}

	INFO("Start native build: %s %s\n", ExecutablePath.c_str(), buildArgs.c_str());

#if defined( __WINDOWS__ )
	nativeProc.CreateJob();
#endif
	exitState = nativeProc.Spawn(ExecutablePath.c_str(), buildArgs.c_str(), workingDir.c_str(), nullptr, false);
	if (exitState == PROCESS_CREATE_OK)
	{
		INFO("Create build process OK!\n");

#if defined( __WINDOWS__ )
		size_t bufferSize = MEGABYTE;
		size_t readSize = -1;
		char *output = new char[bufferSize]();
		for (; nativeProc.IsRunning() || readSize != 0;)
		{
			readSize = nativeProc.ReadStdOut(output, bufferSize - 1);
			output[readSize] = '\000';
			OUTPUT_DIRECT(output);
			Thread::Sleep(15);
		}
		SAFE_DELETE_ARRAY(output);

		ASSERT(!nativeProc.IsRunning());
#endif

		exitState = nativeProc.WaitForExit();

		if (exitState == FB_CONSOLE_BUILD_OK)
		{
			INFO("Build exit OK\n");
			return;
		}
		else if (exitState == FB_CONSOLE_BUILD_CANCELLED)
		{
			INFO("Build cancelled\n");
			exitState = FB_CONSOLE_BUILD_CANCELLED;
			return;
		}
		else
		{
			FATALERROR("Failed to build, build exit code: %d\n", exitState);
			exitState = FB_CONSOLE_BUILD_FAILED;
			return;
		}
	}
	else
	{
		FATALERROR("Create build process failed!\n");
	}
}

// Cleanup
//------------------------------------------------------------------------------
/*virtual*/ void NDKBuildTask::Cleanup()
{
	for (uint16_t idx = 0; idx < 100; ++idx)
	{
		ClearHookerTools();
		if (IsHooked())
		{
			Thread::Sleep(100);
			continue;
		}
		else
		{
			return;
		}
	}
}
