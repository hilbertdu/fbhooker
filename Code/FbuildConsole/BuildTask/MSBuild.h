//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: MSBuild.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/05
//*******************************************************

// MSBuild.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FCONSOLE_MSBUILD_H
#define FCONSOLE_MSBUILD_H

// Includes
//------------------------------------------------------------------------------
#include "BuildTask.h"
#include <map>

class MSBuildTask : public BuildTask
{
public:
	typedef struct {
		std::string name;
		std::string guid;
		std::string path;
		std::string fullPath;
		std::string replacedPath;
		std::string fullReplacedPath;
		std::string toolsetVer;
		std::vector<std::string> dependGuids;
	} VCProjectInfo;

	typedef std::map<std::string, VCProjectInfo>::iterator ProjectIter;

	MSBuildTask(const std::string &servicePath,
				const std::string &slnPath,
				const std::string &project,
				const std::string &buildConfig,
				const std::string &buildTarget,
				const std::string &args,
				const std::string &fbuildArgs,
				const std::string &brokeragePath,
				bool useMSBuild,
				bool quickMode,
				bool debugMode);
	~MSBuildTask();

	virtual void DoBuild();
	virtual void Cleanup();

	int GetExitState() { return exitState; };

private:
	// --> Rewrite all solution and project file, hook the VCTargetPath, and use MSBuild to build
	// --> In this way the quick mode build can not be supported.

	bool CheckParamValid();
	void ParseSolution();
	void RewriteSolution();
	void RewriteVCProject();
	void StartMSBuild();

	void HookerCppTargets();

	std::string GetToolsetVersion(const std::string &projectContent);
	std::string GetBuildExePath(const std::string &toolsetVer);

	virtual void GetServiceArgs(std::string &args);
	virtual void CopyHookerTools();
	virtual void ClearHookerTools();

	std::string ExecutablePath;
	std::string MSBuildTargetsDir;
	std::string HookerTargetsFileDir;
	std::string HookerTargetsDir;

	std::string configuration;
	std::string platform;
	std::string solutionFile;
	std::string buildProject;
	std::string buildTarget;
	std::string buildArgs;
	std::string solutionContent;
	std::string hookSolutionPath;
	std::map<std::string, VCProjectInfo> projects;

	bool useMSBuild;
};

#endif	// FCONSOLE_MSBUILD_H