//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: BuildTask.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/05
//*******************************************************

// Includes
//------------------------------------------------------------------------------
#include "BuildTask.h"

#if defined( __WINDOWS__ )
#include<windows.h>
#endif

// Static members
/*static*/ std::string BuildTask::totalArgs = "";

// Constructor
//------------------------------------------------------------------------------
BuildTask::BuildTask(const std::string &serviceDir, 
					 const std::string &fbuildArgs,
					 const std::string &brokeragePath,
					 bool quickMode,
					 bool debugMode) :
serviceDir(serviceDir),
fbuildArgs(fbuildArgs),
brokeragePath(brokeragePath),
quickMode(quickMode),
debugMode(debugMode),
exitState(0)
{
	INFO("Quick mode: %d\n", quickMode);
	servicePath = serviceDir + "\\fbservice.exe";
	hooker0Path = serviceDir + "\\fbhooker0.exe";
	hooker1Path = serviceDir + "\\fbhooker1.exe";
}

// Deconstructor
//------------------------------------------------------------------------------
BuildTask::~BuildTask()
{
}

/*static*/ void BuildTask::AbortBuild()
{
	CHECK_CONDITION(IsValid());
	
	INFO("Abort build task\n");


#if defined( __WINDOWS__ )
	// Kill native build process
	if (Get().nativeProc.IsRunning())
	{
		Get().nativeProc.TerminateJob(FB_CONSOLE_BUILD_CANCELLED);
	}

	// Create cleanup process
	HANDLE current_process = ::GetCurrentProcess();
	char filePath[MAX_PATH] = "";
	DWORD size = MAX_PATH;
	QueryFullProcessImageNameA(current_process, 0, filePath, &size);

	Process proc;
	proc.DisableHandleRedirection();
	proc.Spawn(filePath, (totalArgs + " --clear").c_str(), nullptr, nullptr);
	proc.Detach();
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	// TODO
#endif
}

/*static*/ void BuildTask::SetTotalArgs(const std::string &args)
{
	totalArgs = args;
}

/*virtual*/ void BuildTask::PreBuild()
{
	CHECK_CONDITION(ExitStateValid());

	// Initialize intermediate directories
	FileDirHelper::DirectoryCreate(workingDir + "\\.fbuild");
	FileDirHelper::DirectoryCreate(workingDir + "\\.fbuild\\.tmp");
	FileDirHelper::DirectoryCreate(workingDir + "\\.fbuild\\Config");
}

// DoBuild
//------------------------------------------------------------------------------
/*virtual*/ void BuildTask::DoBuild()
{
	CHECK_CONDITION(ExitStateValid());

	INFO("Build task do build\n");
	CopyHookerTools();
	BeginService();
}

// PostBuild
//------------------------------------------------------------------------------
/*virtual*/ void BuildTask::PostBuild()
{
	INFO("Build task post build\n");
	Thread::Sleep(1000);
	EndService();
	ClearHookerTools();
}


void BuildTask::SetWorkingDir(const std::string &workingDir)
{
	DEBUG("Set working dir: %s\n", workingDir.c_str());
	this->workingDir = workingDir;
}

// GetServiceArgs
//------------------------------------------------------------------------------
/*virtual*/ void BuildTask::GetServiceArgs(std::string &args)
{
	args = quickMode ? "-startquick" : "-start";
	args += " /FBOPTS:\"\"\"-dist -nodb " + fbuildArgs + "\"\"\"";
	args += " /BROKERAGEPATH:\"\"\"" + brokeragePath + "\"\"\"";
	args += " /SLNDIR:\"\"\"" + solutionDir + "\"\"\"";
	args += " /SLNNAME:\"\"\"" + solutionName + "\"\"\"";
}

// CopyHookerTools
//------------------------------------------------------------------------------
/*virtual*/ void BuildTask::CopyHookerTools()
{
}

// ClearHookerTools
//------------------------------------------------------------------------------
/*virtual*/ void BuildTask::ClearHookerTools()
{
}

// Cleanup
//------------------------------------------------------------------------------
/*virtual*/ void BuildTask::Cleanup()
{
}

// ExitStateValid
//------------------------------------------------------------------------------
bool BuildTask::ExitStateValid()
{
	return exitState == FB_CONSOLE_BUILD_OK;
}

// Build
//------------------------------------------------------------------------------
void BuildTask::BeginService()
{
	INFO("BeginService\n");

	std::string args;
	GetServiceArgs(args);

#if defined( __WINDOWS__ )
	serviceStartProc.DisableHandleRedirection();
	serviceStartProc.CreateJob();
#ifdef _DEBUG
	serviceStartProc.ShowConsole();
#else
	serviceStartProc.CreateDetached();
#endif
#endif
	int exitState = serviceStartProc.Spawn((serviceDir + "\\fbservice.exe").c_str(), args.c_str(), workingDir.c_str(), nullptr, false);
	if (exitState == PROCESS_CREATE_OK)
	{
		serviceStartProc.Detach();
		INFO("Create start fbservice process OK!\n");
	}
	else
	{
		FATALERROR("Create start fbservice process failed!\n");
	}
}

// EndService
//------------------------------------------------------------------------------
void BuildTask::EndService()
{
	INFO("EndService\n");

	std::string exitArgs;
	if (quickMode)
	{
		exitArgs += "-interbuild";
		exitArgs += " /SLNDIR:\"\"\"" + solutionDir + "\"\"\"";
		exitArgs += " /SLNNAME:\"\"\"" + solutionName + "\"\"\"";
		exitArgs += " /FBFILE:\"\"\"" + solutionDir + "\\.fbuild\\fbuild.bff" + "\"\"\"";
		exitArgs += " /FBOPTS:\"\"\"-dist -nodb " + fbuildArgs + "\"\"\"";
		exitArgs += " /BROKERAGEPATH:\"\"\"" + brokeragePath + "\"\"\"";
	}
	else
	{
		exitArgs += "-exit";
	}
	

#if defined( __WINDOWS__ )
	if (!quickMode)
	{
		serviceEndProc.DisableHandleRedirection();
		serviceEndProc.CreateDetached();
	}
#endif
	int exitState = serviceEndProc.Spawn((serviceDir + "\\fbservice.exe").c_str(), exitArgs.c_str(), workingDir.c_str(), nullptr, quickMode);
	if (exitState == PROCESS_CREATE_OK)
	{
		INFO("Create end fbservice process OK!\n");
		if (!quickMode)
		{
			serviceEndProc.Detach();
		}
		else
		{
			INFO("Waiting for end proc exit\n");
			exitState = serviceEndProc.WaitForExit();
			if (exitState == FB_CONSOLE_BUILD_OK)
			{
				INFO("Build exit OK\n");
				return;
			}
			else if (exitState == FB_CONSOLE_BUILD_CANCELLED)
			{
				INFO("Build cancelled\n");
				exitState = FB_CONSOLE_BUILD_CANCELLED;
				return;
			}
			else
			{
				FATALERROR("Failed to build, build exit code: %d\n", exitState);
				exitState = FB_CONSOLE_BUILD_FAILED;
				return;
			}
		}
	}
	else
	{
		FATALERROR("Create end fbservice process failed!\n");
	}
}
