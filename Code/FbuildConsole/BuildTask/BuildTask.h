//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: BuildTask.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/11
//*******************************************************

// PreBuild.h
//------------------------------------------------------------------------------
#pragma once
#ifndef FCONSOLE_PREBUILD_H
#define FCONSOLE_PREBUILD_H

// Includes
//------------------------------------------------------------------------------
#include <string>
#include <vector>
#include "../Common/Common.h"

class BuildTask : public Singleton<BuildTask>
{
public:
	BuildTask(const std::string &serviceDir, 
			  const std::string &fbuildArgs,
			  const std::string &brokeragePath,
			  bool quickMode,
			  bool debugMode);
	~BuildTask();

	virtual void PreBuild();
	virtual void DoBuild();
	virtual void PostBuild();
	virtual void Cleanup();

	static void AbortBuild();
	static void SetTotalArgs(const std::string &args);

protected:
	void BeginService();
	void EndService();

	void SetWorkingDir(const std::string &workingDir);
	virtual void GetServiceArgs(std::string &args);
	virtual void CopyHookerTools();
	virtual void ClearHookerTools();

	bool ExitStateValid();

	std::string solutionDir;
	std::string solutionName;

	std::string workingDir;
	std::string serviceDir;
	std::string servicePath;
	std::string hooker0Path;
	std::string hooker1Path;

	std::string fbuildArgs;
	std::string brokeragePath;

	Process nativeProc;
	Process serviceStartProc;
	Process serviceEndProc;

	bool quickMode;
	bool debugMode;
	int exitState;

	static std::string totalArgs;
};

#endif	// FCONSOLE_PREBUILD_H