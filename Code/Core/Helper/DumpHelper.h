#pragma once
//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: DumpHelper.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/10
//*******************************************************

// DumpHelper.h
//------------------------------------------------------------------------------

class DumpHelper
{
public:
	enum DUMP_TYPE { FULL_DUMP, MINI_DUMP };

public:
	static void Init(const char *dumpFilename, DUMP_TYPE dtype);
};

//------------------------------------------------------------------------------