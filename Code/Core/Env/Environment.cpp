//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Tracing.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Env.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Environment.h"
#include "../Utils/Macros.h"
#include "../Utils/Utils.h"
#include "../Logger/Tracing.h"

#if defined( __WINDOWS__ )
#include <windows.h>
#include <stdio.h>
#endif

#if defined( __LINUX__ ) || defined( __APPLE__ )
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#endif

// Static member initialization
//------------------------------------------------------------------------------
char * Env::userEnvBlock = nullptr;
std::vector<std::string> Env::userEnvs;

// GetEnvVariable
//------------------------------------------------------------------------------
/*static*/ std::string Env::GetEnvVariable(const char * name)
{
#if defined( __WINDOWS__ )
	int iLength;
	iLength = GetEnvironmentVariableA(name, NULL, 0);
	if (iLength == 0)
	{
		DEBUG("Not found env: %s\n", name);
		return "";
	}
	char *buffer = new char[iLength + 1];
	GetEnvironmentVariableA(name, buffer, iLength + 1);
	std::string ret = buffer;
	delete buffer;
	return ret;

#elif defined( __LINUX__ ) || defined( __APPLE__ )
	const char * envVar = ::getenv(name);
	std::string ret = envVar;
	delete envVar;
	return ret;
#else
	#error Unknown platform
#endif
}

/*static*/ void Env::AddUserEnv(const std::string& name)
{
	userEnvs.push_back(name + "=" + GetEnvVariable(name.c_str()));
}

/*static*/ void Env::AddUserEnv(const std::string& name, const std::string& value)
{
	userEnvs.push_back(name + "=" + value);
}

/*static*/ void Env::ResetUserEnv()
{
	userEnvs.clear();
	SAFE_DELETE(userEnvBlock);
}

/*static*/ void Env::GenerateUserEnvBlock()
{
	size_t envLen = 1;
	for (size_t i = 0; i < userEnvs.size(); ++i)
	{
		DEBUG("[Env] %s\n", userEnvs[i].c_str());
		envLen += userEnvs[i].size() + 1;
	}
	SAFE_DELETE(userEnvBlock);
	userEnvBlock = new char[envLen + 1]();

	int startPos = 0;
	for (size_t i = 0; i < userEnvs.size(); ++i)
	{
		memcpy(userEnvBlock + startPos, userEnvs[i].c_str(), userEnvs[i].length() + 1);
		startPos += userEnvs[i].length() + 1;
	}
	memcpy(userEnvBlock + startPos, "\0", 1);
}

/*static*/ std::string Env::GetEnvVariable(const std::string &name, const std::vector<std::string> &envs)
{
	for (size_t i = 0; i < envs.size(); ++i)
	{
		size_t idx = envs[i].find_first_of(name);
		if (idx == 0 && envs[i].at(name.size()) == '=')
		{
			return envs[i].substr(name.size() + 1);
		}
	}
	return "";
}

/*static*/ std::vector<std::string> Env::GetUserEnvs()
{
	return userEnvs;
}

/*static*/ const char * Env::GetUserEnvBlock()
{
	return userEnvBlock;
}

// GetLastErr
//------------------------------------------------------------------------------
/*static*/ int Env::GetLastErr()
{
#if defined( __WINDOWS__ )
	return ::GetLastError();
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	return errno;
#else
	#error Unknown platform
#endif
}


//------------------------------------------------------------------------------
