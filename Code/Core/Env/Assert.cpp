//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Assert.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/19
//*******************************************************

// Assert.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Assert.h"
#include "../Utils/Macros.h"
#include "../Logger/Tracing.h"

#if defined( __WINDOWS__ )
	#include <windows.h>
#endif

#ifdef ASSERTS_ENABLED
	#include <stdarg.h>
    #include <stdio.h>
#endif

// Static
//------------------------------------------------------------------------------
#ifdef ASSERTS_ENABLED
/*static*/ bool AssertHandler::s_ThrowOnAssert( false );
#endif

// IsDebugerAttached
//------------------------------------------------------------------------------
bool IsDebuggerAttached()
{
	#if defined( __WINDOWS__ )
		return ( IsDebuggerPresent() == 1 );
	#elif defined( __APPLE__ )
		return false; // TODO:MAC Implement IsDebugerAttached
	#elif defined( __LINUX__ )
		return false; // TODO:LINUX Implement IsDebugerAttached
	#else
		#error Unknown platform
	#endif
}

// Failure
//------------------------------------------------------------------------------
#ifdef ASSERTS_ENABLED
/*static*/ bool AssertHandler::Failure( const char * message, 
										const char * file,  
										const int line )
{
	#ifdef DEBUG
		const int BUFFER_SIZE( 4096 );
		char buffer[ BUFFER_SIZE ];
        #if defined( __APPLE__ ) || defined( __LINUX__ )
            sprintf( buffer,
        #else
            sprintf_s( buffer, BUFFER_SIZE,
        #endif
			"\n-------- ASSERTION FAILED --------\n%s(%i): Assert: %s\n-----^^^ ASSERTION FAILED ^^^-----\n",
			file, line, message );

		puts( buffer );
        #if defined( __WINDOWS__ )
            OutputDebugStringA( buffer );
        #endif
    
		if ( s_ThrowOnAssert )
		{
			throw "AssertionFailed";
		}

		if ( IsDebuggerAttached() == false )
		{
			#if defined( __WINDOWS__ )
                // TODO:LINUX Fix MessageBox use
                // TODO:OSX Fix MessageBox use
				int res = MessageBox( nullptr, buffer, "Assertion Failed - Break Execution?", MB_YESNO | MB_ICONERROR );
				return ( res == IDYES );
			#endif
		}
		return true; // break execution
	#else
		return true; // break execution
	#endif
}

// FailureM
//------------------------------------------------------------------------------
/*static*/ bool AssertHandler::FailureM( const char * message, 
										 const char * file,  
										 const int line,
										 const char * fmtString,
										 ... )
{
	#ifdef DEBUG
		size_t bufferSize = 2048;
		char *buffer = new char[bufferSize]();

		va_list args;
		va_start(args, fmtString);

		int len = -1;
		while (len < 0)
		{
			len = vsnprintf_s(buffer, bufferSize, _TRUNCATE, fmtString, args);
			if (len >= 0)
				break;
			DEBUG("Realloc output buffer\n");
			SAFE_DELETE_ARRAY(buffer);
			bufferSize *= 2;
			buffer = new char[bufferSize]();
		}

		va_end(args);

		std::string msg = message;
		msg += buffer;

		bool ret = Failure(msg.c_str(), file, line );
		SAFE_DELETE_ARRAY(buffer);
		return ret;
	#else
		return true; // break execution
	#endif
}

//------------------------------------------------------------------------------
#endif // ASSERTS_ENABLED
