//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Tracing.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Env - Provide host system details.
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_ENV_ENV_H
#define CORE_ENV_ENV_H

// Includes
//------------------------------------------------------------------------------
#include <string>
#include <vector>

// Forward Declarations
//------------------------------------------------------------------------------

// Env
//------------------------------------------------------------------------------
class Env
{
public:
	enum Platform
	{
		WINDOWS,
		IOS,
		OSX,
		LINUX
	};

	static inline Platform GetPlatform();
	static inline const char * GetPlatformName(Platform platform);
	static inline const char * GetPlatformName() { return GetPlatformName(GetPlatform()); }

	static std::string GetEnvVariable(const char * name);

	static void AddUserEnv(const std::string& name);
	static void AddUserEnv(const std::string& name, const std::string& value);
	static void ResetUserEnv();
	static void GenerateUserEnvBlock();
	static std::string GetEnvVariable(const std::string &name, const std::vector<std::string> &envs);
	static std::vector<std::string> GetUserEnvs();
	static const char * GetUserEnvBlock();

	static int GetLastErr();

private:
	static std::vector<std::string> userEnvs;
	static char* userEnvBlock;
};

// GetPlatform
//------------------------------------------------------------------------------
/*static*/ inline Env::Platform Env::GetPlatform()
{
#if defined( __WINDOWS__ )
	return Env::WINDOWS;
#elif defined( __IOS__ )
	return Env::IOS;
#elif defined( __OSX__ )
	return Env::OSX;
#elif defined( __LINUX__ )
	return Env::LINUX;
#endif
}

// GetPlatformName
//------------------------------------------------------------------------------
/*static*/ inline const char * Env::GetPlatformName(Platform platform)
{
	switch (platform)
	{
	case Env::WINDOWS:	return "Windows";
	case Env::IOS:		return "IOS";
	case Env::OSX:		return "OSX";
	case Env::LINUX:	return "Linux";
	}
	return "Unknown";
}

//------------------------------------------------------------------------------
#endif // CORE_ENV_ENV_H
