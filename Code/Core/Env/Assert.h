//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Assert.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/03/19
//*******************************************************

// Assert.h
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_ENV_ASSERT_H
#define CORE_ENV_ASSERT_H

// Includes
//------------------------------------------------------------------------------

// Defines
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
    #define BREAK_IN_DEBUGGER __debugbreak();
#elif defined( __APPLE__ )
    #define BREAK_IN_DEBUGGER __builtin_trap();
#elif defined( __LINUX__ )
    #define BREAK_IN_DEBUGGER __asm__ __volatile__("int $3")
#else
    #error Unknown platform
#endif

// Global functions
//------------------------------------------------------------------------------
bool IsDebuggerAttached();

// DEBUG
//------------------------------------------------------------------------------
#ifdef _DEBUG
	#define ASSERTS_ENABLED

	// standard assertion macro
	#define ASSERT( expression )												\
		do {																	\
			if ( !( expression ) )												\
			{																	\
				if ( AssertHandler::Failure( #expression, __FILE__, __LINE__ ) )\
				{																\
					BREAK_IN_DEBUGGER;												\
				}																\
			}																	\
		} while ( false );														\

	// standard assertion macro with message
	#define ASSERTM( expression, ... )											\
		do {																	\
			if ( !( expression ) )												\
			{																	\
				if ( AssertHandler::FailureM( #expression, __FILE__, __LINE__, __VA_ARGS__ ) )\
				{																\
					BREAK_IN_DEBUGGER;												\
				}																\
			}																	\
		} while ( false );														\

	// assert result of code, but still execute code when asserts are disabled
	#define VERIFY( code ) ASSERT( code )

	// compile time assertion
    #define CTASSERT( expression ) static_assert( expression,                   \
        "\n================ COMPILE TIME ASSERTION FAILURE ================\n"  \
                                   "Expression: " #expression                   \
        "\n================================================================\n" )

	class AssertHandler
	{
	public:
		static void SetThrowOnAssert( bool throwOnAssert )
		{
			s_ThrowOnAssert = throwOnAssert;
		}
		static bool Failure( const char * message, 
							 const char * file,  
							 const int line );
		static bool FailureM( const char * message, 
							 const char * file,  
							 const int line,
							 const char * msgFormat,
							 ... );

		static bool s_ThrowOnAssert;
	};

// RELEASE
//------------------------------------------------------------------------------
#elif defined ( _RELEASE )
	#define ASSERT( expression )			\
		do {								\
		} while ( false );					\

	#define VERIFY( code )					\
		do {								\
			if ( code ) {}					\
		} while ( false );					\

	#define CTASSERT( expression )			\
		do {								\
		} while ( false );					\

#else
	#error neither DEBUG nor RELEASE were defined
#endif

//------------------------------------------------------------------------------
#endif // CORE_ENV_ASSERT_H
