//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Tracing.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Tracing.h
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_TRACING_TRACING_H
#define CORE_TRACING_TRACING_H

// Includes
//------------------------------------------------------------------------------
#include <iostream>

// Macros
//------------------------------------------------------------------------------
#define DEBUG( ... )											\
	do {														\
		if (Tracing::showDebug)									\
		{														\
			Tracing::OutputFormat("[DEBUG]  ", ##__VA_ARGS__);	\
		}														\
	} while(false)												\

#define INFO( ... )												\
	do {														\
		if (Tracing::showInfo)									\
		{														\
			Tracing::OutputFormat("[INFO]   ", ##__VA_ARGS__);	\
		}														\
	} while(false)												\

#define WARNING( ... )          Tracing::OutputFormat( "[WARN]   ", ##__VA_ARGS__ )
#define FATALERROR( ... )       Tracing::OutputFormat( "[ERROR]  ", ##__VA_ARGS__ )

#define OUTPUT_DIRECT( msg )    Tracing::Output( msg )

#define OUTPUT_PREFIX_SIZE      9

// Tracing
//------------------------------------------------------------------------------
class Tracing
{
public:
	static inline void DoNothing() {}
	static void Output(const char * message);
	static void OutputFormat(const char *prefix, const char * fmtString, ...);

	static void Init();
	static void Finalize();

	static bool outputToFile;
	static bool showInfo;
	static bool showDebug;
	static std::string logFilePath;

private:
	static std::ofstream logFile;
};

//------------------------------------------------------------------------------
#endif // CORE_TRACING_TRACING_H
