//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Tracing.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Tracing.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include "Tracing.h"
#include "../Utils/Macros.h"
#include "../Utils/Utils.h"

#if defined( __WINDOWS__ )
    #ifdef DEBUG
        #include <windows.h> // for OutputDebugStringA
    #endif
#endif

// Static Data
//------------------------------------------------------------------------------
/*static*/ std::ofstream Tracing::logFile;
/*static*/ std::string Tracing::logFilePath;
/*static*/ bool Tracing::outputToFile = false;
/*static*/ bool Tracing::showInfo = false;
/*static*/ bool Tracing::showDebug = false;

// Output
//------------------------------------------------------------------------------
/*static*/ void Tracing::Output(const char * message)
{
	// normal output that goes to the TTY
	fputs(message, stdout);

	if (outputToFile && logFile.is_open())
	{
		logFile << message;
		logFile.flush();
	}

	// emit to the debugger as well if possible
#ifdef __WINDOWS__
#ifdef _DEBUG
	OutputDebugStringA(message);
#endif
#endif
}

// OutputFormat
//------------------------------------------------------------------------------
/*static*/ void Tracing::OutputFormat(const char *prefix, const char * fmtString, ...)
{
	size_t bufferSize = 2048;
	char *buffer = new char[bufferSize]();

	va_list args;
	va_start(args, fmtString);

	int len = -1;
	while (len < 0)
	{
#if defined( __WINDOWS__ )
		len = vsnprintf_s(buffer + OUTPUT_PREFIX_SIZE, bufferSize - OUTPUT_PREFIX_SIZE - 1, _TRUNCATE, fmtString, args);
#elif defined( __APPLE__ ) || defined( __LINUX__ )
		len = vsnprintf(buffer + OUTPUT_PREFIX_SIZE, bufferSize - OUTPUT_PREFIX_SIZE - 1, fmtString, args);
#else
		#error Unknown platform
#endif
		if (len >= 0)
			break;
		DEBUG("Realloc output buffer\n");
		SAFE_DELETE_ARRAY(buffer);
		bufferSize *= 2;
		buffer = new char[bufferSize]();
	}

	va_end(args);

	memcpy(buffer, prefix, OUTPUT_PREFIX_SIZE);
	Output(buffer);
	SAFE_DELETE_ARRAY(buffer);
}

// OpenLogFile
//------------------------------------------------------------------------------
/*static*/ void Tracing::Init()
{
	if (outputToFile)
	{
		if (logFilePath.empty())
		{
			logFilePath = "fastbuild.log";
		}
		logFile.open(logFilePath);
	}
}

// CloseLogFile
//------------------------------------------------------------------------------
/*static*/ void Tracing::Finalize()
{
	if (logFile)
	{
		logFile.close();
	}
}

//------------------------------------------------------------------------------
