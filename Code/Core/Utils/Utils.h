//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Utils.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Utils.h
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_UTILS_H
#define CORE_UTILS_H

// Include
//------------------------------------------------------------------------------
#include <vector>
#include <string>


#define CHECK_CONDITION( expression )		\
		do {								\
			if ( !( expression ) )			\
			{								\
				return;						\
			}								\
		} while (false);


namespace Math {
	template <class T>
	static inline T RoundUp(T value, T alignment)
	{
		return (value + alignment - 1) & ~(alignment - 1);
	}
	template <class T>
	static inline T Max(T a, T b)
	{
		return (a > b) ? a : b;
	}
	template <class T>
	static inline T Min(T a, T b)
	{
		return (a < b) ? a : b;
	}
	template <class T>
	static inline T Clamp(T a, T b, T c)
	{
		return Min(Max(a, b), c);
	}
	template <typename T>
	static inline bool IsPowerOf2(T value)
	{
		return (((value - 1) & value) == 0);
	}
}

namespace WCharHelper {
	char *GetCharFromWChar(const wchar_t *wch);
	wchar_t *GetWCharFromChar(const char *ch);
}


namespace StrHelper {
	void StrFormat(std::string &out, const char *fmtString, ...);
	void StrEscape(std::string &out, const std::string &in, const char &escapeCh, const char &prefix);
	void StrJoin(std::string &out, const std::vector<std::string> &list, const char *splitter = " ", const char *wrapper = "");
	void StrJoinEx(std::string &out, const std::vector<std::string> &list, const char *splitter = " ", const char *wrapperLeft = "", const char *wrapperRight = "");
	void StrUpper(std::string &out, const std::string &in);
	void StrLower(std::string &out, const std::string &in);
	void StrTrim(std::string &str, const char trimCh);
	void StrTrim(std::string &str, const char lTrimCh, const char rTrimCh);
	void StrTrimEnd(std::string &str, const char rTrimCh);
	void StrTrims(std::string &str, const char *trimChs);		// TODO to be tested
	bool StrBeginWithI(const std::string &str, const char *s);
	bool StrEndWithI(const std::string &str, const char *s);
	bool StrCompareI(const std::string &str, const std::string &substr);
	bool StrReplace(std::string &out, const std::string &str, const std::string &src, const std::string &des);
	bool StrReplace(std::string &str, const std::string &src, const std::string &des);
	int StrFindI(const std::string &str1, const std::string &str2);
	std::vector<std::string> &StrSplit(const std::string &s, char delim, std::vector<std::string> &elems, char trimCh = '\0');
}


namespace FileDirHelper {

// Defines
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
#define NATIVE_SLASH ( '\\' )
#define NATIVE_SLASH_STR ( "\\" )
#define NATIVE_DOUBLE_SLASH ( "\\\\" )
#define OTHER_SLASH ( '/' )
#define OTHER_SLASH_STR ( "/" )
#elif defined( __LINUX__ ) || defined( __APPLE__ )
#define NATIVE_SLASH ( '/' )
#define NATIVE_SLASH_STR ( "/" )
#define NATIVE_DOUBLE_SLASH ( "//" )
#define OTHER_SLASH ( '\\' )
#endif

// For places that explicitly need slashes a certain way
// use these defines to signify the intent
#define BACK_SLASH ( '\\' )
#define FORWARD_SLASH ( '/' )

// All slash string
#define ALL_SLASH_STR ( "\\/" )

	enum FileMode
	{
		READ = 0x1,
		WRITE = 0x2,
		APPEND = 0x4,
	};

	enum FileAttr
	{
		HIDE = 0x1,
	};

	std::string GetCurrentDir();
	bool SetCurrentDir(const std::string &curDir);
	bool FileExists(const std::string &fileName);
	void TransToFullPath(std::string &file, const std::string &prefix = "");
	void PathJoin(std::string &out, const std::string &path1, const std::string &path2);
	void GetFileNameFromPath(std::string &fileName, const std::string &path, bool withExt = true);
	void GetDirFromPath(std::string &dir, const std::string &path);

	void FixupFolderPath(std::string &path);
	void FixupFilePath(std::string &path);

	void FindFileByExtension(const std::string &targetPath, std::vector<std::string> &fileNames, const std::string &extension, bool isDir = false);
	void FindFileByPrefix(const std::string &targetPath, std::vector<std::string> &fileNames, const std::string &prefix, bool isDir = false);
	bool FileReadAll(const std::string &path, std::string &content);
	bool FileWrite(const std::string &path, std::string &content, FileMode mode = WRITE);
	bool FileCopy(const std::string &src, const std::string &des, bool overwrite = true);
	bool FileDelete(const std::string &path);
	bool DirectoryCreate(const std::string &path);
	bool DirectoryCopy(const std::string &src, const std::string &des);
	bool DirectoryDelete(const std::string &path, bool deleteSubdirs = true);
	bool DirectoryExist(const std::string &path);
}


namespace ErrorHelper {
	int GetLastError();
	void ShowMsgBox(const char * msg);
	void ShowLastErrorInfo();
}


#endif	// CORE_UTILS_H
