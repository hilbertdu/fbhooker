//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Utils.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************


// Include
//------------------------------------------------------------------------------
#include "Utils.h"
#include "Macros.h"
#include "../Logger/Tracing.h"
#include "../Env/Assert.h"

#include <algorithm>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sstream>
#include <fstream>

#if defined( __WINDOWS__ )
	#include <windows.h>
#endif
#if defined( __LINUX__ ) || defined( __APPLE__ )
	#include <dirent.h>
	#include <errno.h>
	#include <limits.h>
	#include <stdio.h>
	#include <sys/stat.h>
	#include <unistd.h>
#endif
#if defined( __LINUX__ )
	#include <fcntl.h>
	#include <sys/sendfile.h>
#endif
#if defined( __APPLE__ )
	#include <copyfile.h>
	#include <sys/time.h>
#endif


namespace StrHelper {
	// StrFormat
	//------------------------------------------------------------------------------
	void StrFormat(std::string &out, const char *fmtString, ...)
	{
		out = "";
		size_t bufferSize = 1024;
		char *buffer = new char[bufferSize]();

		va_list args;
		va_start(args, fmtString);

		int len = -1;
		while (len < 0)
		{
#if defined( __WINDOWS__ ) 
			len = vsnprintf_s(buffer, bufferSize - 1, _TRUNCATE, fmtString, args);
#elif defined( __APPLE__ ) || defined( __LINUX__ )
			len = vsnprintf(buffer, bufferSize - 1, fmtString, args);
#else
#error Unknown platform
#endif
			if (len >= 0)
				break;
			DEBUG("Realloc output buffer\n");
			SAFE_DELETE_ARRAY(buffer);
			bufferSize *= 2;
			buffer = new char[bufferSize]();
		}

		va_end(args);
		out = buffer;
		SAFE_DELETE_ARRAY(buffer);
	}

	// StrJoin
	//------------------------------------------------------------------------------
	void StrJoin(std::string &out,
		const std::vector<std::string> &list,
		const char *splitter,
		const char *wrapper)
	{
		out = "";
		std::vector<std::string>::const_iterator iter = list.begin();
		for (; iter != list.end(); ++iter)
		{
			out += wrapper;
			out += (*iter);
			out += wrapper;
			if (iter + 1 != list.end())
			{
				out += splitter;
			}
		}
	}

	void StrJoinEx(std::string &out,
		const std::vector<std::string> &list,
		const char *splitter,
		const char *wrapperLeft,
		const char *wrapperRight)
	{
		out = "";
		std::vector<std::string>::const_iterator iter = list.begin();
		for (; iter != list.end(); ++iter)
		{
			out += wrapperLeft;
			out += (*iter);
			out += wrapperRight;
			if (iter + 1 != list.end())
			{
				out += splitter;
			}
		}
	}

	// StrEscape
	//------------------------------------------------------------------------------
	void StrEscape(std::string &out, const std::string &in, const char &escapeCh, const char &prefix)
	{
		out = "";
		for (size_t i = 0; i < in.length(); ++i)
		{
			if (in[i] == escapeCh)
			{
				out.push_back(prefix);
			}
			out.push_back(in[i]);
		}
	}

	// StrUpper and StrLower
	//------------------------------------------------------------------------------
	void StrUpper(std::string &out, const std::string &in)
	{
		out.resize(in.size());
		for (size_t i = 0; i < in.size(); ++i)
		{
			out[i] = toupper(in[i]);
		}
	}

	void StrLower(std::string &out, const std::string &in)
	{
		out.resize(in.size());
		for (size_t i = 0; i < in.size(); ++i)
		{
			out[i] = tolower(in[i]);
		}
	}

	// StrTrim
	//------------------------------------------------------------------------------
	void StrTrim(std::string &str, const char trimCh)
	{
		size_t posStart = str.find_first_not_of(trimCh);
		size_t posEnd = str.find_last_not_of(trimCh);
		str = str.substr(posStart == std::string::npos ? str.size() : posStart,
			posEnd == std::string::npos ? 0 : posEnd - posStart + 1);
	}

	void StrTrim(std::string &str, const char lTrimCh, const char rTrimCh)
	{
		size_t posStart = str.find_first_not_of(lTrimCh);
		size_t posEnd = str.find_last_not_of(rTrimCh);
		str = str.substr(posStart == std::string::npos ? str.size() : posStart,
			posEnd == std::string::npos ? 0 : posEnd - posStart + 1);
	}

	void StrTrimEnd(std::string &str, const char rTrimCh)
	{
		size_t posEnd = str.find_last_not_of(rTrimCh);
		str = str.substr(0, posEnd == std::string::npos ? 0 : posEnd + 1);
	}

	void StrTrims(std::string &str, const char *trimChs)
	{
		if (str.size() == 0)
		{
			return;
		}

		size_t startIdx = 0;
		size_t endIdx = str.size();
		size_t trimLen = strlen(trimChs);
		while (startIdx < str.size())
		{
			bool trimed = false;
			for (size_t idx = 0; idx < trimLen; ++idx)
			{
				if (str.at(startIdx) == *(trimChs + idx))
				{
					trimed = true;
				}
			}
			if (trimed)
			{
				startIdx++;
				continue;
			}
			break;
		}
		while (endIdx > 0)
		{
			bool trimed = false;
			for (size_t idx = 0; idx < trimLen; ++idx)
			{
				if (str.at(endIdx) == *(trimChs + idx))
				{
					trimed = true;
				}
			}
			if (trimed)
			{
				endIdx--;
				continue;
			}
			break;
		}
		str = str.substr(startIdx, endIdx > startIdx ? endIdx - startIdx + 1 : 0);
	}

	// StrBeginWithI
	//------------------------------------------------------------------------------
	bool StrBeginWithI(const std::string &str, const char *s)
	{
		size_t len = strlen(s);
		size_t idx = 0;
		if (len == 0 || str.length() < len)
		{
			return false;
		}
		while (idx < len)
		{
			if (str.at(idx) != toupper(s[idx]) && str.at(idx) != tolower(s[idx]))
			{
				return false;
			}
			idx++;
		}
		return true;
	}

	// StrEndWithI
	//------------------------------------------------------------------------------
	bool StrEndWithI(const std::string &str, const char *s)
	{
		size_t len = strlen(s);
		size_t strLen = str.length();
		size_t idx = 0;
		if (len == 0 || strLen < len)
		{
			return false;
		}
		while (idx < len)
		{
			int pos1 = strLen - idx - 1;
			int pos2 = len - idx - 1;
			if (str.at(pos1) != toupper(s[pos2]) && str.at(pos1) != tolower(s[pos2]))
			{
				return false;
			}
			idx++;
		}
		return true;
	}

	// StrCompareI
	//------------------------------------------------------------------------------
	bool StrCompareI(const std::string &str1, const std::string &str2)
	{
		if (str1.size() != str2.size())
		{
			return false;
		}
		for (size_t i = 0; i < str1.size(); ++i)
		{
			if (tolower(str1[i]) != tolower(str2[i]))
			{
				return false;
			}
		}
		return true;
	}

	// StrReplace
	//------------------------------------------------------------------------------
	bool StrReplace(std::string &out, const std::string &str, const std::string &src, const std::string &des)
	{
		size_t idx1 = 0;
		size_t idx2 = 0;
		size_t oriLen = str.size();
		size_t srcLen = src.size();

		out.clear();
		out.reserve(oriLen);

		if (oriLen == 0 || srcLen == 0)
		{
			return false;
		}

		bool replaced = false;

		while (idx1 < oriLen)
		{
			for (; idx1 < oriLen && idx2 < srcLen && str[idx1] == src[idx2]; ++idx1, ++idx2);
			if (idx2 == srcLen)
			{
				out += des;
				replaced = true;
			}
			else
			{
				out += str.substr(idx1 - idx2, idx2 + 1);
				idx1++;
			}
			idx2 = 0;
		}
		return replaced;
	}

	// StrReplace ( in place sort )
	//------------------------------------------------------------------------------
	bool StrReplace(std::string &str, const std::string &src, const std::string &des)
	{
		std::string out;
		bool replaced = StrReplace(out, str, src, des);
		if (replaced)
		{
			str = out;
		}
		return replaced;
	}

	// StrFindI
	//------------------------------------------------------------------------------
	int StrFindI(const std::string &str, const std::string &substr)
	{
		std::string::const_iterator fPos = std::search(str.begin(), str.end(), substr.begin(), substr.end(),
			[](char l, char r)
		{
			return tolower(l) == tolower(r);
		});

		if (fPos != str.end())
		{
			return std::distance(str.begin(), fPos);
		}
		return -1;
	}

	// StrSplit
	//------------------------------------------------------------------------------
	std::vector<std::string> &StrSplit(const std::string &s, char delim, std::vector<std::string> &elems, char trimCh) {
		/*std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			if (trimCh != '\0')
			{
				StrTrim(item, trimCh);
			}
			elems.push_back(item);
		}
		return elems;*/


		int totalSize = s.size();
		int curPos = 0;
		int curTokenStart = 0;
		int curTokenEnd = 0;
		bool lookingForStart = true;
		bool hasQuotation = false;

		for (int idx = 0; idx < totalSize; ++idx)
		{
			if (lookingForStart)
			{
				if (s[idx] == delim || s[idx] == '\n' || s[idx] == '\r')
				{
					curTokenStart++;
				}
				else
				{
					if (s[idx] == '"' || s[idx] == '\'')
					{
						hasQuotation = true;
					}
					curTokenStart = idx;
					lookingForStart = false;
				}
			}
			else
			{
				if (s[idx] == delim || s[idx] == '\n' || s[idx] == '\r')
				{
					if (hasQuotation)
					{
						continue;
					}
					hasQuotation = false;
					lookingForStart = true;
					curTokenEnd = idx;
					std::string token = std::string(s, curTokenStart, curTokenEnd - curTokenStart);
					StrTrim(token, trimCh);
					elems.push_back(token);
				}
				else if (s[idx] == '"' || s[idx] == '\'')
				{
					hasQuotation = !hasQuotation;
				}
			}
		}

		// check the last one token if has
		if (!lookingForStart)
		{
			std::string token = std::string(s, curTokenStart, s.size() - curTokenStart);
			StrTrim(token, trimCh);
			elems.push_back(token);
		}
		return elems;
	}
}

namespace FileDirHelper {
	// GetCurrentDir
	//------------------------------------------------------------------------------
	std::string GetCurrentDir()
	{
		std::string curDir;
#if defined( __WINDOWS__ )
		char buffer[MAX_PATH] = { 0 };
		DWORD len = GetCurrentDirectoryA(MAX_PATH, buffer);
		if (len != 0)
		{
			curDir = buffer;
		}
		else
		{
			FATALERROR("GetCurrentDirectory error: %d!\n", GetLastError());
		}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		const size_t bufferSize(PATH_MAX);
		char buffer[bufferSize];
		if (getcwd(buffer, bufferSize))
		{
			curDir = buffer;
		}
#else
#error Unknown platform
#endif
		return curDir;
	}

	bool SetCurrentDir(const std::string &curDir)
	{
		return false;
	}

	// FileExists
	//------------------------------------------------------------------------------
	bool FileExists(const std::string &fileName)
	{
#if defined( __WINDOWS__ )
		// see if we can get attributes
		DWORD attributes = GetFileAttributes(fileName.c_str());
		if (attributes == INVALID_FILE_ATTRIBUTES)
		{
			return false;
		}
		return true; // note this might not be file!
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		struct stat st;
		if (stat(fileName.c_str(), &st) == 0)
		{
			if ((st.st_mode & S_IFDIR) != S_IFDIR)
			{
				return true; // exists and is NOT a folder
			}
		}
		return false;
#else
#error Unknown platform
#endif
	}

	// GetFullPathName
	//------------------------------------------------------------------------------
	void TransToFullPath(std::string &file, const std::string &prefix)
	{
		FixupFilePath(file);
		char fullPath[MAX_FILE_PATH] = { 0 };
#if defined( __WINDOWS__ )
		if (file.find(':') == std::string::npos && !prefix.empty())
		{
			::GetFullPathNameA((prefix + NATIVE_SLASH_STR + file).c_str(), MAX_FILE_PATH, fullPath, NULL);
		}
		else
		{
			::GetFullPathNameA(file.c_str(), MAX_FILE_PATH, fullPath, NULL);
		}
		file = fullPath;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO fixedup to unique path name
		std::string tmpPrefix = prefix;
		while (file.front() != NATIVE_SLASH)
		{
			if (tmpPrefix.empty())
			{
				file = GetCurrentDir() + file;
				break;
			}
			else
			{
				file = tmpPrefix + NATIVE_SLASH_STR + file;
				tmpPrefix.clear();
			}
		}
#else
#error Unknown platform
#endif
		StrHelper::StrTrimEnd(file, NATIVE_SLASH);
	}

	// FindFile
	//------------------------------------------------------------------------------
	void PathJoin(std::string &out, const std::string &path1, const std::string &path2)
	{
		out = path1;
		if (!StrHelper::StrEndWithI(out, NATIVE_SLASH_STR))
		{
			out += NATIVE_SLASH_STR + path2;
		}
		else
		{
			out += path2;
		}
	}

	void FixupFolderPath(std::string &path)
	{
		// Normalize slashes
		std::string tmpPath;
		StrHelper::StrReplace(tmpPath, path, OTHER_SLASH_STR, NATIVE_SLASH_STR);
#if defined( __WINDOWS__ )
		bool isUNCPath = StrHelper::StrBeginWithI(tmpPath, NATIVE_DOUBLE_SLASH);
#endif
		while (StrHelper::StrReplace(tmpPath, NATIVE_DOUBLE_SLASH, NATIVE_SLASH_STR)) {}

#if defined( __WINDOWS__ )
		if (isUNCPath)
		{
			tmpPath = NATIVE_SLASH + tmpPath; // Restore double slash by adding one back
		}
#endif

		// ensure slash termination
		if (!StrHelper::StrEndWithI(path, NATIVE_SLASH_STR))
		{
			tmpPath += NATIVE_SLASH;
		}
		path = tmpPath;
	}

	void FixupFilePath(std::string &path)
	{
		// Normalize slashes
		std::string tmpPath;
		StrHelper::StrReplace(tmpPath, path, OTHER_SLASH_STR, NATIVE_SLASH_STR);
		while (StrHelper::StrReplace(tmpPath, NATIVE_DOUBLE_SLASH, NATIVE_SLASH_STR)) {}

		// Sanity check - calling this function on a folder path is an error
		//ASSERT(StrHelper::StrEndWithI(tmpPath, NATIVE_SLASH_STR) == false);
		path = tmpPath;
	}

	// FindFile
	//------------------------------------------------------------------------------
	void FindFileByExtension(const std::string &targetPath, std::vector<std::string> &fileNames, const std::string &extension, bool isDir)
	{
#if defined( __WINDOWS__ )
		WIN32_FIND_DATAA ffd;
		HANDLE hFind = FindFirstFileA((targetPath + NATIVE_SLASH_STR + "*").c_str(), &ffd);

		fileNames.clear();
		if (INVALID_HANDLE_VALUE == hFind)
		{
			DEBUG("Can not find dir %s\n", targetPath.c_str());
		}
		do
		{
			if (isDir && (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ||
				(!isDir && !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)))
			{
				std::string tmpfileName = ffd.cFileName;
				if (extension.empty() || tmpfileName.rfind(extension) != std::string::npos)
				{
					fileNames.push_back(tmpfileName);
				}
			}
		} while (FindNextFileA(hFind, &ffd) != 0);
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO: LINUX Implement
#else
#error Unknown platform
#endif
	}

	// GetFileNameFromPath
	//------------------------------------------------------------------------------
	void GetFileNameFromPath(std::string &fileName, const std::string &path, bool withExt)
	{
		std::string fixedPath = path;
		FixupFilePath(fixedPath);

		size_t pos = fixedPath.find_last_of(NATIVE_SLASH_STR);
		fileName = (pos == std::string::npos) ? fixedPath : fixedPath.substr(pos + 1, fixedPath.size() - pos);

		if (!withExt)
		{
			size_t extPos = fileName.find_last_of(".");
			fileName = fileName.substr(0, extPos);
		}
	}

	// GetFileNameFromPath
	//------------------------------------------------------------------------------
	void GetDirFromPath(std::string &dir, const std::string &path)
	{
		size_t pos = path.find_last_of(NATIVE_SLASH_STR);
		if (pos == std::string::npos)
		{
			pos = 0;
		}
		dir = path.substr(0, pos);
	}

	// FindFile
	//------------------------------------------------------------------------------
	void FindFileByPrefix(const std::string &targetPath, std::vector<std::string> &fileNames, const std::string &prefix, bool isDir)
	{
#if defined( __WINDOWS__ )
		WIN32_FIND_DATAA ffd;
		HANDLE hFind = FindFirstFileA((targetPath + NATIVE_SLASH_STR + "*").c_str(), &ffd);

		std::string upperPrefix;
		StrHelper::StrUpper(upperPrefix, prefix);

		fileNames.clear();
		if (INVALID_HANDLE_VALUE == hFind)
		{
			DEBUG("Can not find dir %s\n", targetPath.c_str());
		}
		do
		{
			if (isDir && (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ||
				(!isDir && !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)))
			{
				std::string tmpfileName;
				StrHelper::StrUpper(tmpfileName, ffd.cFileName);
				if (tmpfileName.find(upperPrefix) != std::string::npos)
				{
					fileNames.push_back(ffd.cFileName);
				}
			}
		} while (FindNextFileA(hFind, &ffd) != 0);
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO: LINUX Implement
#else
#error Unknown platform
#endif
	}

	bool FileReadAll(const std::string &path, std::string &content)
	{
		std::ifstream inFile(path);
		if (!inFile)
		{
			content.clear();
			return false;
		}

		std::stringstream strStream;
		strStream << inFile.rdbuf();
		content = strStream.str();
		inFile.close();
		return true;
	}

	bool FileWrite(const std::string &path, std::string &content, FileMode mode)
	{
		std::ofstream::openmode openMode = std::ofstream::out;
		if ((mode & APPEND) != 0)
		{
			openMode |= std::ofstream::app;
		}

		std::ofstream outFile(path, openMode);
		if (!outFile)
		{
			content.clear();
			FATALERROR("Can't write file: %s\n", path.c_str());
			return false;
		}

		outFile << content;
		outFile.close();
		return true;
	}

	bool FileCopy(const std::string &src, const std::string &des, bool overwrite)
	{
		const char* srcFileName = src.c_str();
		const char* dstFileName = des.c_str();

#if defined( __WINDOWS__ )
		BOOL failIfDestExists = (overwrite ? FALSE : TRUE);
		BOOL result = CopyFile(srcFileName, dstFileName, failIfDestExists);
		if (result == FALSE)
		{
			// even if we allow overwrites, Windows will fail if the dest file
			// was read only, so we have to un-mark the read only status and try again
			if ((GetLastError() == ERROR_ACCESS_DENIED) && (overwrite))
			{
				// see if dst file is read-only
				DWORD dwAttrs = GetFileAttributes(dstFileName);
				if (dwAttrs == INVALID_FILE_ATTRIBUTES)
				{
					return false; // can't even get the attributes, nothing more we can do
				}
				if (0 == (dwAttrs & FILE_ATTRIBUTE_READONLY))
				{
					return false; // file is not read only, so we don't know what the problem is
				}

				// try to remove read-only flag on dst file
				dwAttrs = (dwAttrs & ~FILE_ATTRIBUTE_READONLY);
				if (FALSE == SetFileAttributes(dstFileName, dwAttrs))
				{
					return false; // failed to remove read-only flag
				}

				// try to copy again
				result = CopyFile(srcFileName, dstFileName, failIfDestExists);
				return (result == TRUE);
			}
		}

		return (result == TRUE);
#elif defined( __APPLE__ )
		if (overwrite == false)
		{
			if (FileExists(dstFileName))
			{
				return false;
			}
		}
		copyfile_state_t s;
		s = copyfile_state_alloc();
		bool result = (copyfile(srcFileName, dstFileName, s, COPYFILE_DATA | COPYFILE_XATTR) == 0);
		copyfile_state_free(s);
		return result;
#elif defined( __LINUX__ )
		if (overwrite == false)
		{
			if (FileExists(dstFileName))
			{
				return false;
			}
		}

		int source = open(srcFileName, O_RDONLY, 0);
		if (source < 0)
		{
			return false;
		}

		int dest = open(dstFileName, O_WRONLY | O_CREAT | O_TRUNC, 0644); // TODO:LINUX Check args for FileCopy dst
		if (dest < 0)
		{
			close(source);
			return false;
		}

		struct stat stat_source;
		//VERIFY(fstat(source, &stat_source) == 0);

		ssize_t bytesCopied = sendfile(dest, source, 0, stat_source.st_size);

		close(source);
		close(dest);

		return (bytesCopied == stat_source.st_size);
#else
#error Unknown platform
#endif
	}

	bool FileDelete(const std::string &path)
	{
#if defined( __WINDOWS__ )
		SetFileAttributes(path.c_str(), FILE_ATTRIBUTE_NORMAL);
		BOOL result = DeleteFile(path.c_str());
		if (result == FALSE)
		{
			DEBUG("Can not delete file: %s, error code: %d\n", path.c_str(), ::GetLastError());
			return false; // failed to delete
		}
		return true; // delete ok
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		/*if (GetReadOnly(path))
		{
			return false;
		}*/
		return (remove(path.c_str()) == 0);
#else
#error Unknown platform
#endif
	}

	bool DirectoryCreate(const std::string &path)
	{
#if defined( __WINDOWS__ )
		if (CreateDirectory(path.c_str(), nullptr))
		{
			return true;
		}

		// it failed - is it because it exists already?
		if (GetLastError() == ERROR_ALREADY_EXISTS)
		{
			return true;
		}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		umask( 0 ); // disable default creation mask
		mode_t mode = S_IRWXU | S_IRWXG | S_IRWXO; // TODO:LINUX TODO:MAC Check these permissions
		if ( mkdir( path.c_str(), mode ) == 0 )
		{
			return true; // created ok
		}

		// failed to create - already exists?
		if ( errno == EEXIST )
		{
			return true;
		}
#else
#error Unknown platform
#endif
		// failed, probably missing intermediate folders or an invalid name
		return false;
	}

	bool DirectoryCopy(const std::string &src, const std::string &des)
	{
#if defined( __WINDOWS__ )
		WIN32_FIND_DATAA ffd;
		HANDLE hFind = FindFirstFileA((src + "\\*").c_str(), &ffd);

		if (INVALID_HANDLE_VALUE == hFind)
		{
			DEBUG("Can not find dir %s\n", src.c_str());
			return false;
		}
		do
		{
			if (StrHelper::StrCompareI(ffd.cFileName, ".") || StrHelper::StrCompareI(ffd.cFileName, ".."))
			{
				continue;
			}
			if ((ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				DirectoryCreate(des + "\\" + ffd.cFileName);
				if (!DirectoryCopy(src + "\\" + ffd.cFileName, des + "\\" + ffd.cFileName))
				{
					return false;
				}
			}
			else {
				if (!FileCopy(src + "\\" + ffd.cFileName, des + "\\" + ffd.cFileName))
				{
					return false;
				}
			}
		} while (FindNextFileA(hFind, &ffd) != 0);
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO: LINUX Implement
#else
#error Unknown platform
#endif
		return true;
	}

	bool DirectoryDelete(const std::string &path, bool deleteSubdirs)
	{
		// To be implement
#if defined( __WINDOWS__ )
		bool            bSubdirectory = false;       // Flag, indicating whether
		// subdirectories have been found
		HANDLE          hFile;                       // Handle to directory
		std::string     strFilePath;                 // Filepath
		std::string     strPattern;                  // Pattern
		WIN32_FIND_DATA ffd;             // File information


		strPattern = path + "\\*.*";
		hFile = ::FindFirstFileA(strPattern.c_str(), &ffd);
		if (INVALID_HANDLE_VALUE == hFile)
		{
			DEBUG("Can not find dir %s\n", path.c_str());
			return false;
		}
		do
		{
			if (StrHelper::StrCompareI(ffd.cFileName, ".") || StrHelper::StrCompareI(ffd.cFileName, ".."))
			{
				continue;
			}
			strFilePath.erase();
			strFilePath = path + "\\" + ffd.cFileName;

			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (deleteSubdirs)
				{
					// Delete subdirectory
					bool ret = DirectoryDelete(strFilePath, deleteSubdirs);
					if (!ret)
					{
						return ret;
					}
				}
				else
					bSubdirectory = true;
			}
			else
			{
				bool ret = FileDelete(strFilePath);
				if (!ret)
				{
					return ret;
				}
			}
		} while (::FindNextFile(hFile, &ffd) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			DEBUG("Can not delete dir %s, error code: %d\n", path.c_str(), dwError);
			return false;
		}
		else
		{
			if (!bSubdirectory)
			{
				::SetFileAttributes(path.c_str(), FILE_ATTRIBUTE_NORMAL);
				if (::RemoveDirectory(path.c_str()) == FALSE)
				{
					DEBUG("Can not remove directory: %s\n", path.c_str());
					return false;
				}
			}
		}
#elif defined( __LINUX__) || defined( __APPLE__)
		// TODO: LINUX Implement
#endif
		return true;
	}

	bool DirectoryExist(const std::string &path)
	{
#if defined( __WINDOWS__ )
		DWORD res = GetFileAttributes(path.c_str());
		if ((res != INVALID_FILE_ATTRIBUTES) &&
			((res & FILE_ATTRIBUTE_DIRECTORY) != 0))
		{
			return true; // exists and is a folder
		}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		struct stat st;
		if (stat(path.c_str(), &st) == 0)
		{
			if ((st.st_mode & S_IFDIR) != 0)
			{
				return true; // exists and is folder
			}
		}
#else
#error Unknown platform
#endif
		return false; // doesn't exist, isn't a folder or some other problem
	}
}

namespace WCharHelper {
	// Char and WChar exchange
	//------------------------------------------------------------------------------
	char *GetCharFromWChar(const wchar_t *wch)
	{
#if defined( __WINDOWS__ )
		int size = WideCharToMultiByte(CP_ACP, 0, wch, -1, NULL, 0, NULL, NULL);
		char *ch = new char[size + 1];
		if (WideCharToMultiByte(CP_ACP, 0, wch, -1, ch, size, NULL, NULL))
		{
			return ch;
		}
#else
		// Not implement
#endif
		return nullptr;
	}

	wchar_t *GetWCharFromChar(const char *ch)
	{
#if defined( __WINDOWS__ )
		size_t size = MultiByteToWideChar(CP_ACP, 0, ch, -1, NULL, 0);
		wchar_t *wch = new wchar_t[size + 1];
		if (MultiByteToWideChar(CP_ACP, 0, ch, -1, wch, size))
		{
			return wch;
		}
#else
		// Not implement
#endif
		return nullptr;
	}
}


namespace ErrorHelper {
	// ShowMsgBox
	//------------------------------------------------------------------------------
	void ShowMsgBox(const char * msg)
	{
#if defined( __WINDOWS__ )
		MessageBoxA(nullptr, msg, "FBHooker", MB_OK);
#else
		// Not implement
#endif
	}

	int GetLastError()
	{
#if defined( __WINDOWS__ )
		return ::GetLastError();
#elif defined( __APPLE__ ) || defined( __LINUX__ )
		return errno;
#else
#error Unknown platform
#endif
	}

	// ShowErrorInfo
	//------------------------------------------------------------------------------
	void ShowLastErrorInfo()
	{
#if defined( __WINDOWS__ )
		int errorCode = GetLastError();
		if (errorCode == 0)
		{
			WARNING("No error occur\n");
			return;
		}
		LPVOID lpMsgBuf;
		::FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
		ShowMsgBox((char *)lpMsgBuf);
		LocalFree(lpMsgBuf);
#else
		// Not implement
#endif
	}
}
