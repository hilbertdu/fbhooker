//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Macros.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Macros.h
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_MACROS_H
#define CORE_MACROS_H

// Macro
//------------------------------------------------------------------------------
#define SAFE_DELETE(p)			do { if(p) { delete (p); (p) = nullptr; } } while(0)
#define SAFE_DELETE_ARRAY(p)	do { if(p) { delete[] (p); (p) = nullptr; } } while(0)

#define MAX_FILE_PATH	1024

#define KILOBYTE (1024)
#define MEGABYTE (1024 * 1024)

// ErrorCode
//------------------------------------------------------------------------------
enum
{
	PROCESS_CREATE_OK = 0x0010,
	PROCESS_CREATE_EXIST = 0x0011,
	PROCESS_CREATE_ERROR = 0x0012,
	PROCESS_START_EXIT = 0x0013,
};

enum
{
	SHARED_MEM_CANNOT_CREATE = 0x013,
	SHARED_MEM_CANNOT_REMAP = 0x014,
};

#endif // CORE_MACROS_H
