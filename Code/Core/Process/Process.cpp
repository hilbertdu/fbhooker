//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: Process.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// Process.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "Process.h"
#include "../Utils/Macros.h"
#include "../Utils/Utils.h"
#include "../Logger/Tracing.h"
#include "../Env/Assert.h"
#include "Thread.h"
#include <string>

#if defined( __WINDOWS__ )
	#include <windows.h>
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	#include <errno.h>
	#include <fcntl.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <sys/wait.h>
	#include <unistd.h>
#endif

// CONSTRUCTOR
//------------------------------------------------------------------------------
Process::Process() :
m_Started(false)
#if defined( __WINDOWS__ )
,m_SharingHandles(false)
,m_RedirectHandles(true)
,m_ShowConsole(false)
,m_CreateDetached(false)
,m_StdOutRead(nullptr)
,m_StdOutWrite(nullptr)
,m_StdErrRead(nullptr)
,m_StdErrWrite(nullptr)
,m_Job(nullptr)
#endif
#if defined( __LINUX__ ) || defined( __APPLE__ )
,m_ChildPID(-1)
,m_HasAlreadyWaitTerminated(false)
#endif
{
#if defined( __WINDOWS__ )
	CTASSERT(sizeof(m_ProcessInfo) == sizeof(PROCESS_INFORMATION));
#endif
}

// DESTRUCTOR
//------------------------------------------------------------------------------
Process::~Process()
{
	if (m_Started)
	{
		WaitForExit();
	}

#if defined( __WINDOWS__ )
	if (m_Job)
	{
		TerminateJobObject(m_Job, 0);
	}
#endif
}

#if defined( __WINDOWS__ )
bool Process::CreateJob()
{
	ASSERT(!m_Job);
	m_Job = CreateJobObjectA(NULL, NULL); // GLOBAL
	if (m_Job == NULL)
	{
		FATALERROR("Could not create job object\n");
		return false;
	}
	else
	{
		JOBOBJECT_EXTENDED_LIMIT_INFORMATION jeli = { 0 };

		// Configure all child processes associated with the job to terminate when the
		jeli.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
		if (0 == SetInformationJobObject(m_Job, JobObjectExtendedLimitInformation, &jeli, sizeof(jeli)))
		{
			FATALERROR("Could not SetInformationJobObject\n");
			return false;
		}
		return true;
	}
}

bool Process::TerminateJob(uint32_t exitCode)
{
	if (m_Job)
	{
		INFO("Try to terminate job\n");
		if (!TerminateJobObject(m_Job, exitCode))
		{
			FATALERROR("Can not terminate job: %d\n", GetLastError());
			return false;
		}
		m_Job = nullptr;
	}
	else
	{
		WARNING("No job can be terminated\n");
	}
	return true;
}
#endif

// Spawn
//------------------------------------------------------------------------------
int Process::Spawn(const char * executable,
	const char * args,
	const char * workingDir,
	const char * environment,
	bool shareHandles)
{
	ASSERT(!m_Started);
	ASSERT(executable);

#if defined( __WINDOWS__ )
	// Set up the start up info struct.
	STARTUPINFO si;
	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);

	SECURITY_ATTRIBUTES sa;
	ZeroMemory(&sa, sizeof(SECURITY_ATTRIBUTES));
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = nullptr;

	m_SharingHandles = shareHandles;

	if (m_RedirectHandles)
	{
		// create the pipes
		if (shareHandles)
		{
			si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
			si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
			si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
		}
		else
		{
			if (!CreatePipe(&m_StdOutRead, &m_StdOutWrite, &sa, MEGABYTE))
			{
				return false;
			}
			SetHandleInformation(m_StdOutRead, HANDLE_FLAG_INHERIT, 0);

			if (!CreatePipe(&m_StdErrRead, &m_StdErrWrite, &sa, MEGABYTE))
			{
				VERIFY(CloseHandle(m_StdOutRead));
				VERIFY(CloseHandle(m_StdOutWrite));
				return false;
			}
			SetHandleInformation(m_StdErrRead, HANDLE_FLAG_INHERIT, 0);

			si.hStdOutput = m_StdOutWrite;
			si.hStdError = m_StdErrWrite;
			si.hStdInput = GetStdHandle(STD_INPUT_HANDLE); // m_StdInRead;
		}
		si.dwFlags |= STARTF_USESTDHANDLES;
	}

	std::string fullArgs = "";
	fullArgs += '\"';
	fullArgs += executable;
	fullArgs += '\"';
	if (args)
	{
		fullArgs += ' ';
		fullArgs += args;
	}

	DEBUG("Create process: %s, working dir: %s\n", fullArgs.c_str(), workingDir);

	ASSERT(!(m_ShowConsole && m_CreateDetached));
	DWORD creationFlag = 0;
	if (m_ShowConsole)
	{
		creationFlag |= CREATE_NEW_CONSOLE;
	}
	if (m_CreateDetached)
	{
		creationFlag |= DETACHED_PROCESS;
	}

	if (!CreateProcessA(nullptr, //executable,
		(LPSTR)fullArgs.c_str(),
		nullptr,
		nullptr,
		(BOOL)m_RedirectHandles,
		creationFlag,
		(void *)environment,
		workingDir,
		(LPSTARTUPINFOA)&si,
		(LPPROCESS_INFORMATION)&m_ProcessInfo))
	{
		return PROCESS_CREATE_ERROR;
	}

	if (m_Job)
	{
		if (0 == AssignProcessToJobObject(m_Job, GetProcessInfo().hProcess))
		{
			FATALERROR("Could not AssignProcessToObject\n");
		}
	}

	m_Started = true;
	return PROCESS_CREATE_OK;

#elif defined( __LINUX__ ) || defined( __APPLE__ )
	// create StdOut and StdErr pipes to capture output of spawned process
	int stdOutPipeFDs[2];
	int stdErrPipeFDs[2];
	VERIFY(pipe(stdOutPipeFDs) == 0);
	VERIFY(pipe(stdErrPipeFDs) == 0);

	// fork the process
	const pid_t childProcessPid = fork();
	if (childProcessPid == -1)
	{
		// cleanup pipes
		VERIFY(close(stdOutPipeFDs[0]) == 0);
		VERIFY(close(stdOutPipeFDs[1]) == 0);
		VERIFY(close(stdErrPipeFDs[0]) == 0);
		VERIFY(close(stdErrPipeFDs[1]) == 0);

		ASSERT(false); // fork failed - should not happen in normal operation
		return false;
	}

	const bool isChild = (childProcessPid == 0);
	if (isChild)
	{
		VERIFY(dup2(stdOutPipeFDs[1], STDOUT_FILENO) != -1);
		VERIFY(dup2(stdErrPipeFDs[1], STDERR_FILENO) != -1);

		VERIFY(close(stdOutPipeFDs[0]) == 0);
		VERIFY(close(stdOutPipeFDs[1]) == 0);
		VERIFY(close(stdErrPipeFDs[0]) == 0);
		VERIFY(close(stdErrPipeFDs[1]) == 0);

		if (workingDir)
		{
			FileDirHelper::SetCurrentDir(workingDir);
		}

		// split args
		std::vector<std::string> argVec;
		StrHelper::StrSplit(args, ' ', argVec, '"');

		// prepare args
		const size_t numArgs = argVec.size();
		char ** argv = new char *[numArgs + 2];
		argv[0] = const_cast< char * >(executable);
		for (size_t i = 0; i<numArgs; ++i)
		{
			argv[i + 1] = const_cast< char * >(argVec[i].c_str()); // leave arg as-is
		}
		argv[numArgs + 1] = nullptr;

		// transfer execution to new executable
		execv(executable, argv);

		exit(-1); // only get here if execv fails
	}
	else
	{
		// close write pipes (we never write anything)
		VERIFY(close(stdOutPipeFDs[1]) == 0);
		VERIFY(close(stdErrPipeFDs[1]) == 0);

		// keep pipes for reading child process
		m_StdOutRead = stdOutPipeFDs[0];
		m_StdErrRead = stdErrPipeFDs[0];
		m_ChildPID = (int)childProcessPid;

		// TODO: How can we tell if child spawn failed?
		m_Started = true;
		m_HasAlreadyWaitTerminated = false;
		return true;
	}
#else
#error Unknown platform
#endif
}

// IsRunning
//----------------------------------------------------------
bool Process::IsRunning() const
{
	if (!m_Started)
	{
		return false;
	}

#if defined( __WINDOWS__ )
	switch (WaitForSingleObject(GetProcessInfo().hProcess, 0))
	{
	case WAIT_OBJECT_0:
		return false;

	case WAIT_TIMEOUT:
		return true;
	}
	ASSERT(false); // we should never get here
	return false;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	// already waited?
	if (m_HasAlreadyWaitTerminated)
	{
		return false;
	}

	// non-blocking "wait"
	int status(-1);
	pid_t result = waitpid(m_ChildPID, &status, WNOHANG);
	ASSERT(result != -1); // usage error
	if (result == 0)
	{
		return true; // Still running
	}

	// store wait result: can't call again if we just cleaned up process
	ASSERT(result == m_ChildPID);
	m_ReturnStatus = status;
	m_HasAlreadyWaitTerminated = true;
	return false; // no longer running
#else
#error Unknown platform
#endif
}

// WaitForExit
//------------------------------------------------------------------------------
int Process::WaitForExit()
{
	ASSERT(m_Started);
	m_Started = false;

#if defined( __WINDOWS__ )
	// wait for it to finish
	VERIFY(WaitForSingleObject(GetProcessInfo().hProcess, INFINITE) == WAIT_OBJECT_0);

	// get the result code
	DWORD exitCode = 0;
	VERIFY(GetExitCodeProcess(GetProcessInfo().hProcess, (LPDWORD)&exitCode));

	// cleanup
	VERIFY(CloseHandle(GetProcessInfo().hProcess));
	VERIFY(CloseHandle(GetProcessInfo().hThread));

	if (!m_SharingHandles && m_RedirectHandles)
	{
		VERIFY(CloseHandle(m_StdOutRead));
		VERIFY(CloseHandle(m_StdOutWrite));
		VERIFY(CloseHandle(m_StdErrRead));
		VERIFY(CloseHandle(m_StdErrWrite));
	}

	return exitCode;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	VERIFY(close(m_StdOutRead) == 0);
	VERIFY(close(m_StdErrRead) == 0);
	if (m_HasAlreadyWaitTerminated == false)
	{
		VERIFY(waitpid(m_ChildPID, &m_ReturnStatus, 0) == m_ChildPID);
	}
	return m_ReturnStatus;
#else
#error Unknown platform
#endif
}

// Detach
//------------------------------------------------------------------------------
void Process::Detach()
{
	ASSERT(m_Started);
	m_Started = false;

#if defined( __WINDOWS__ )
	// cleanup
	VERIFY(CloseHandle(GetProcessInfo().hProcess));
	VERIFY(CloseHandle(GetProcessInfo().hThread));

	if (!m_SharingHandles && m_RedirectHandles)
	{
		VERIFY(CloseHandle(m_StdOutRead));
		VERIFY(CloseHandle(m_StdOutWrite));
		VERIFY(CloseHandle(m_StdErrRead));
		VERIFY(CloseHandle(m_StdErrWrite));
	}
#elif defined( __APPLE__ )
	// TODO:MAC Implement Process
#elif defined( __LINUX__ )
	ASSERT(false); // TODO:LINUX Implement Process
#else
#error Unknown platform
#endif
}

// ReadAllData
//------------------------------------------------------------------------------
void Process::ReadAllData(std::string & outStr, std::string & errStr)
{
	bool processExited = false;
	for (;;)
	{
		uint32_t prevOutSize = outStr.size();
		uint32_t prevErrSize = errStr.size();
		Read(m_StdOutRead, outStr);
		Read(m_StdErrRead, errStr);

		// did we get some data?
		if ((prevOutSize != outStr.size()) || (prevErrSize != errStr.size()))
		{
			continue; // try reading again right away incase there is more
		}

		// nothing to read right now
		if (IsRunning())
		{
			// no data available, but process is still going, so wait
			// TODO:C Replace this sleep with event-based wait
			// (tricky to do on Windows since unnamed pipes are not waitable :(
			Thread::Sleep(15);
			continue;
		}

		// process exited - is this the first time to this point?
		if (processExited == false)
		{
			processExited = true;
			continue; // get remaining output
		}

		break; // all done
	}
}

// Read
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
void Process::Read(HANDLE handle, std::string & str)
{
	// anything available?
	DWORD bytesAvail(0);
	if (!::PeekNamedPipe(handle, nullptr, 0, nullptr, (LPDWORD)&bytesAvail, nullptr))
	{
		return;
	}
	if (bytesAvail == 0)
	{
		return;
	}

	// read the new data
	DWORD bytesReadNow = 0;
	char *buffer = new char[bytesAvail + 1]();
	if (!::ReadFile(handle, buffer, bytesAvail, (LPDWORD)&bytesReadNow, 0))
	{
		return;
	}
	ASSERT(bytesReadNow == bytesAvail);
	str += buffer;
}
#endif


// Read
//------------------------------------------------------------------------------
#if defined( __LINUX__ ) || defined( __APPLE__ )
void Process::Read(int handle, std::string & str)
{
	// any data available?
	timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	fd_set fdSet;
	FD_ZERO(&fdSet);
	FD_SET(handle, &fdSet);
	int ret = select(handle + 1, &fdSet, nullptr, nullptr, &timeout);
	if (ret == -1)
	{
		ASSERT(false); // usage error?
		return;
	}
	if (ret == 0)
	{
		return; // no data available
	}

	size_t spaceInBuffer = 16 * MEGABYTE - 1;
	char *buffer = new char[spaceInBuffer + 1]();
	// read the new data
	ssize_t result = read(handle, buffer, spaceInBuffer);
	if (result == -1)
	{
		ASSERT(false); // error!
		return;
	}
	str += buffer;
}
#endif

// ReadStdOut
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
char * Process::ReadStdOut(uint32_t * bytesRead)
{
	return Read(m_StdOutRead, bytesRead);
}
#endif

// ReadStdOut
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
char * Process::ReadStdErr(uint32_t * bytesRead)
{
	return Read(m_StdErrRead, bytesRead);
}
#endif

// ReadStdOut
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
uint32_t Process::ReadStdOut(char * outputBuffer, uint32_t outputBufferSize)
{
	return Read(m_StdOutRead, outputBuffer, outputBufferSize);
}
#endif

// ReadStdErr
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
uint32_t Process::ReadStdErr(char * outputBuffer, uint32_t outputBufferSize)
{
	return Read(m_StdErrRead, outputBuffer, outputBufferSize);
}
#endif

// Read
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
char * Process::Read(HANDLE handle, uint32_t * bytesRead)
{
	// see if there's anything in the pipe
	DWORD bytesAvail = 0;
	VERIFY(PeekNamedPipe(handle, nullptr, 0, nullptr, (LPDWORD)&bytesAvail, nullptr));
	if (bytesAvail == 0)
	{
		if (bytesRead)
		{
			*bytesRead = 0;
		}
		return nullptr;
	}

	// allocate output buffer
	char * mem = new char[bytesAvail + 1](); // null terminate for convenience
	mem[bytesAvail] = 0;

	// read the data
	DWORD bytesReadNow = 0;
	VERIFY(ReadFile(handle, mem, bytesAvail, (LPDWORD)&bytesReadNow, 0));
	ASSERT(bytesReadNow == bytesAvail);
	if (bytesRead)
	{
		*bytesRead = bytesReadNow;
	}
	return mem;
}
#endif

// Read
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
uint32_t Process::Read(HANDLE handle, char * outputBuffer, uint32_t outputBufferSize)
{
	// see if there's anything in the pipe
	DWORD bytesAvail = 0;
	VERIFY(PeekNamedPipe(handle, nullptr, 0, 0, (LPDWORD)&bytesAvail, 0));
	if (bytesAvail == 0)
	{
		return 0;
	}

	// if there is more available than we have space for, just read as much as we can
	uint32_t bytesToRead = outputBufferSize < bytesAvail ? outputBufferSize : bytesAvail;

	// read the data
	DWORD bytesReadNow = 0;
	VERIFY(ReadFile(handle, outputBuffer, bytesToRead, (LPDWORD)&bytesReadNow, 0));
	ASSERT(bytesReadNow == bytesToRead);
	return bytesToRead;
}
#endif

// GetCurrentId
//------------------------------------------------------------------------------
/*static*/ uint32_t Process::GetCurrentId()
{
#if defined( __WINDOWS__ )
	return ::GetCurrentProcessId();
#elif defined( __LINUX__ )
	return 0; // TODO: Implement GetCurrentId()
#elif defined( __OSX__ )
	return 0; // TODO: Implement GetCurrentId()
#endif
}

//------------------------------------------------------------------------------
