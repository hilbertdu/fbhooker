//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: SharedMemory.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// SharedMemory.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "SharedMemory.h"
#include "../Logger/Tracing.h"

#if defined( __WINDOWS__ )
	#include <windows.h>
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	#include <sys/types.h>
	#include <sys/ipc.h>
	#include <sys/shm.h>
	#include <stdio.h>
#endif

// CONSTRUCTOR
//------------------------------------------------------------------------------
#if defined( __WINDOWS__ )
SharedMemory::SharedMemory(const char * name, uint32_t size) :
m_Name(name),
#elif defined( __LINUX__ ) || defined( __APPLE__ )
SharedMemory::SharedMemory(int key, uint32_t size) :
m_Key(key),
#endif
m_Memory(nullptr),
m_MapFile(nullptr),
m_MaxSize(size)
{
}

// DESTRUCTOR
//------------------------------------------------------------------------------
SharedMemory::~SharedMemory()
{
	Close();
}

// Create
//------------------------------------------------------------------------------
bool SharedMemory::Create()
{
#if defined( __WINDOWS__ )
	m_MapFile = CreateFileMappingA(INVALID_HANDLE_VALUE,	// use paging file
		nullptr,				// default security
		PAGE_READWRITE,			// read/write access
		0,						// maximum object size (high-order DWORD)
		m_MaxSize,				// maximum object size (low-order DWORD)
		m_Name);               // name of mapping object
	if (m_MapFile)
	{
		m_Memory = MapViewOfFile(m_MapFile,				// handle to map object
			FILE_MAP_ALL_ACCESS,	// read/write permission
			0,						// DWORD dwFileOffsetHigh
			0,						// DWORD dwFileOffsetLow
			m_MaxSize);
		if (!m_Memory)
		{
			DEBUG("Can't map share mem %s %d\n", m_Name, m_MaxSize);
			return false;
		}
		memset(m_Memory, 0, m_MaxSize);
		return true;
	}
	DEBUG("Can't create file mapping %s %d\n", m_Name, m_MaxSize);
	return false;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
#else
#error Unknown platform
#endif
}

// Open
//------------------------------------------------------------------------------
bool SharedMemory::Open()
{
	Close();
#if defined( __WINDOWS__ )
	m_MapFile = OpenFileMappingA(FILE_MAP_ALL_ACCESS,		// read/write access
		FALSE,					// do not inherit the name
		m_Name);					// name of mapping object
	if (m_MapFile)
	{
		m_Memory = MapViewOfFile(m_MapFile,				// handle to map object
			FILE_MAP_ALL_ACCESS,	// read/write permission
			0,						// DWORD dwFileOffsetHigh
			0,						// DWORD dwFileOffsetLow
			m_MaxSize);
		if (!m_Memory)
		{
			return false;
		}
		return true;
	}
	return false;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
#else
#error Unknown platform
#endif
}

// Resize
//------------------------------------------------------------------------------
bool SharedMemory::ReCreate(size_t size)
{
	m_MaxSize = size;

	Close();
	if (Create())
	{
		return true;
	}
	return false;
}

// ReOpen
//------------------------------------------------------------------------------
bool SharedMemory::ReOpen(size_t size)
{
	m_MaxSize = size;

	Close();
	if (Open())
	{
		return true;
	}
	return false;
}

// Close
//------------------------------------------------------------------------------
void SharedMemory::Close()
{
#if defined( __WINDOWS__ )
	if (m_Memory)
	{
		UnmapViewOfFile(m_Memory);
		m_Memory = nullptr;
	}
	if (m_MapFile)
	{
		CloseHandle(m_MapFile);
		m_MapFile = nullptr;
	}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
#else
#error Unknown platform
#endif
}

//------------------------------------------------------------------------------
