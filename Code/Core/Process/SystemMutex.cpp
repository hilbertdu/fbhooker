//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: SystemMutex.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// SystemMutex
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "SystemMutex.h"
#include "../Logger/Tracing.h"
#include "../Utils/Utils.h"
#include "../Env/Assert.h"

#if defined( __WINDOWS__ )
	#include <windows.h>
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	#include <errno.h>
    #include <sys/file.h>
#endif

// CONSTRUCTOR
//------------------------------------------------------------------------------
SystemMutex::SystemMutex(const char * name, MUTEX_TYPE type) :
#if defined( __WINDOWS__ )
m_Handle(INVALID_HANDLE_VALUE),
#elif defined( __LINUX__ ) || defined( __APPLE__ )
m_Handle(-1),
#endif
m_Name(name),
m_Usage(type),
m_IsOwnLock(false)
{
}

// DESTRUCTOR
//------------------------------------------------------------------------------
SystemMutex::~SystemMutex()
{
#if defined( __WINDOWS__ )
	if (m_Handle == INVALID_HANDLE_VALUE)
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	if (m_Handle == -1)
#endif
	{
		return;
	}
	if (IsOwnLocked())
	{
		Unlock();
	}
}

// TryLock
//------------------------------------------------------------------------------
bool SystemMutex::TryLock()
{
	if (m_Usage == FOR_ONE_PROCESS)
	{
#if defined( __WINDOWS__ )
		void * handle = (void *)CreateMutexA(nullptr, FALSE, m_Name.c_str());
		if (GetLastError() == ERROR_ALREADY_EXISTS)
		{
			if ((handle != INVALID_HANDLE_VALUE) && (handle != 0))
			{
				CloseHandle(handle);
			}
			return false;
		}
		DEBUG("Create a mutex for one process %s\n", m_Name.c_str());
		m_Handle = handle;
		m_IsOwnLock = true;
		return true;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		std::string tempFileName;
		StrHelper::StrFormat(tempFileName, "/var/run/%s.lock", m_Name.c_str());
		m_Handle = open(tempFileName.c_str(), O_CREAT | O_RDWR, 0666);
		int rc = flock(m_Handle, LOCK_EX | LOCK_NB);
		if (rc)
		{
			if (errno == EWOULDBLOCK)
			{
				return false; // locked by another process
			}
		}
		m_IsOwnLock = true;
		return true; // we own it now
#else
#error Unknown platform
#endif
	}
	else if (m_Usage == FOR_SYNC_PROCESS)
	{
#if defined( __WINDOWS__ )
		return InitAndWait(IGNORE);
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		return InitAndWait(0);
#else
#error Unknown platform
#endif
	}
	return false;
}

// Lock
//------------------------------------------------------------------------------
void SystemMutex::Lock()
{
	if (m_Usage == FOR_ONE_PROCESS)
	{
		// Not implement
		return;
	}
	else if (m_Usage == FOR_SYNC_PROCESS)
	{
#if defined( __WINDOWS__ )
		InitAndWait(INFINITE);
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		InitAndWait(0);
#else
#error Unknown platform
#endif
	}
}

// IsLocked
//------------------------------------------------------------------------------
bool SystemMutex::IsLocked() const
{
	if (m_Usage == FOR_ONE_PROCESS)
	{
#if defined( __WINDOWS__ )
		if (OpenMutexA(MUTEX_ALL_ACCESS, FALSE, m_Name.c_str()))
		{
			return true;
		}
		return false;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO
		return true;
#else
#error Unknown platform
#endif
	}
	else if (m_Usage == FOR_SYNC_PROCESS)
	{
		return m_IsOwnLock;
	}
	return false;
}

bool SystemMutex::IsOwnLocked() const
{
	return m_IsOwnLock;
}

// Unlock
//------------------------------------------------------------------------------
void SystemMutex::Unlock()
{
	if (m_Usage == FOR_ONE_PROCESS)
	{
#if defined( __WINDOWS__ )
		//DEBUG("Unlock mutex for one process %s\n", m_Name.c_str());
		ASSERT(m_Handle != INVALID_HANDLE_VALUE);
		CloseHandle(m_Handle);
		m_Handle = INVALID_HANDLE_VALUE;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		ASSERT( m_Handle != -1 );
        VERIFY( flock( m_Handle, LOCK_UN ) == 0 );
		m_Handle = -1;
#else
#error Unknown platform
#endif
	}
	else if (m_Usage == FOR_SYNC_PROCESS)
	{
#if defined( __WINDOWS__ )
		if (m_Handle != INVALID_HANDLE_VALUE && IsOwnLocked())
		{
			//DEBUG("Unlock for sync process %s\n", m_Name.c_str());
			::ReleaseMutex(m_Handle);
		}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO
#else
#error Unknown platform
#endif
	}
	m_IsOwnLock = false;
}

// Wait
//------------------------------------------------------------------------------
bool SystemMutex::InitAndWait(unsigned long dwMilliseconds)
{
#if defined( __WINDOWS__ )
	if (m_Handle == INVALID_HANDLE_VALUE)
	{
		void *handle = (void *)CreateMutexA(nullptr, TRUE, m_Name.c_str());
		if (handle && handle != INVALID_HANDLE_VALUE)
		{
			if (GetLastError() == ERROR_ALREADY_EXISTS)
			{
				m_Handle = handle;
				return InitAndWait(INFINITE);
			}
			DEBUG("Creat and locked for sync process %s\n", m_Name.c_str());
			m_Handle = handle;
			m_IsOwnLock = true;
			return true;
		}
		return false;
	}
	else
	{
		if (::WaitForSingleObject(m_Handle, dwMilliseconds) == WAIT_OBJECT_0)
		{
			//DEBUG("Locked for sync process %s\n", m_Name.c_str());
			m_IsOwnLock = true;
			return true;
		}
		return false;
	}
#elif defined( __LINUX__ ) || defined( __APPLE__ )
		// TODO
#else
#error Unknown platform
#endif
}

//------------------------------------------------------------------------------
