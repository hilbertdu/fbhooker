//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: SystemMutex.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// SystemMutex.h
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_PROCESS_SYSTEMMUTEX_H
#define CORE_PROCESS_SYSTEMMUTEX_H

// Includes
//------------------------------------------------------------------------------
#include <string>
#include <stdint.h>

enum MUTEX_TYPE {
	FOR_SYNC_PROCESS,
	FOR_ONE_PROCESS
};

// SystemMutex
//------------------------------------------------------------------------------
class SystemMutex
{
public:
	SystemMutex(const char *, MUTEX_TYPE);
	~SystemMutex();

	bool TryLock();
	void Lock();
	void Unlock();
	bool IsLocked() const;
	bool IsOwnLocked() const;

	inline void ResetName(const std::string &name) { m_Name = name; };

private:
	bool InitAndWait(unsigned long  dwMilliseconds);

#if defined( __WINDOWS__ )
	void * m_Handle;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	int m_Handle;
#endif
	std::string m_Name;
	int m_Usage;
	bool m_IsOwnLock;
};

//------------------------------------------------------------------------------
#endif // CORE_PROCESS_SYSTEMMUTEX_H
