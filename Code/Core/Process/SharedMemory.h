//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: SharedMemory.h
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// SharedMemory.h
//------------------------------------------------------------------------------
#pragma once
#ifndef CORE_SHAREDMEMORY_H
#define CORE_SHAREDMEMORY_H

#include <stdint.h>
#include <stddef.h>

// SharedMemory
//------------------------------------------------------------------------------
class SharedMemory
{
public:
#if defined( __WINDOWS__ )
	SharedMemory(const char * name, uint32_t size=2048);
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	SharedMemory(int key, uint32_t size=2048);
#else
#error Unknown platform
#endif
	~SharedMemory();

	bool Create();
	bool Open();
	void Close();

	bool ReCreate(size_t size);
	bool ReOpen(size_t size);

	inline void SetMaxSize(size_t size);
	inline size_t GetMaxSize() const { return m_MaxSize; };

	void * GetPtr() const { return m_Memory; }
private:
#if defined( __WINDOWS__ )
	const char * m_Name;
#elif defined( __LINUX__ ) || defined( __APPLE__ )
	int m_Key;
#else
#error Unknown platform
#endif
	void * m_Memory;
	void * m_MapFile;
	size_t m_MaxSize;
};

//------------------------------------------------------------------------------
#endif // CORE_SHAREDMEMORY_H
