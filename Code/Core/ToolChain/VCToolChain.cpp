//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: VCToolChain.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2015/11/01
//*******************************************************

// VCToolChain.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "ToolChain.h"
#include "../Env/Environment.h"
#include "../Logger/Tracing.h"
#include "../Utils/Utils.h"
#include "../Process/Process.h"
#include "../Utils/Macros.h"
#include <vector>


// Static Data
//------------------------------------------------------------------------------
/*static*/ std::map<std::string, std::string> VCToolChain::sToolsetPathMap;
/*static*/ std::string VCToolChain::sVSWherePath;


// Init
//------------------------------------------------------------------------------
/*static*/ void VCToolChain::Init()
{
	// TODO: use vswhere to get vs install path for all version
	Process proc;
	int ret = proc.Spawn(sVSWherePath.c_str(), "-all -legacy", nullptr, nullptr);

	if (ret == PROCESS_CREATE_OK)
	{
		INFO("Create build process OK!\n");
		std::string outStr, errStr;
		proc.ReadAllData(outStr, errStr);

		int result = proc.WaitForExit();

		INFO("VSWhere output: %s\n", outStr.c_str());

		if (result != 0)
		{
			FATALERROR("VSWhere process error:  %s\n", errStr.c_str());
			return;
		}

		std::vector<std::string> lines;
		StrHelper::StrSplit(outStr, '\n', lines);

		for (size_t idx = 0; idx < lines.size(); ++idx)
		{
			if (StrHelper::StrBeginWithI(lines[idx], "installationPath:"))
			{
				size_t installVerIdx = idx + 1;
				if (installVerIdx < lines.size() && StrHelper::StrBeginWithI(lines[installVerIdx], "installationVersion:"))
				{
					std::string installPath = lines[idx].substr(strlen("installationPath: "));
					std::string versionRaw = lines[installVerIdx].substr(strlen("installationVersion: "));
					std::string version = versionRaw.substr(0, 2) + "0";

					sToolsetPathMap[version] = installPath;
					++idx;

					INFO("Find a install path: %s -> %s\n", version.c_str(), installPath.c_str());
				}
			}
		}
	}
}

// Init
//------------------------------------------------------------------------------
/*static*/ void VCToolChain::SetVSWherePath(const std::string &path)
{
	sVSWherePath = path;
}

// GetVSInstallDir
//------------------------------------------------------------------------------
/*static*/ std::string VCToolChain::GetVSInstallDir(const std::string &toolsetVer)
{
	if (sToolsetPathMap.find(toolsetVer) != sToolsetPathMap.end())
	{
		return sToolsetPathMap[toolsetVer];
	}

	std::string envKey = "VS" + toolsetVer + "COMNTOOLS";
	std::string envValue = Env::GetEnvVariable(envKey.c_str());

	INFO("GetVSInstallDir :%s\n", envValue.c_str());

	while (envValue.size() > 0 && envValue.back() == NATIVE_SLASH)
	{
		envValue.pop_back();
	}

	if (StrHelper::StrEndWithI(envValue, "\\Common7\\Tools"))
	{
		return envValue.substr(0, envValue.size() - 14);
	}
	else
	{
		FATALERROR("Can not found vs install path: %s!\n", toolsetVer.c_str());
	}
	return "";
}

// GetVSLangVersion
//------------------------------------------------------------------------------
/*static*/ std::string VCToolChain::GetVSLangVersion(const std::string &toolsetVer)
{
	return "1033";
}

// GetToolsetVersionFromPath
//------------------------------------------------------------------------------
/*static*/ std::string VCToolChain::GetToolsetVersionFromPath(const std::string &path)
{
	if (StrHelper::StrBeginWithI(path, GetVSInstallDir("100").c_str()))
	{
		return "100";
	}
	else if (StrHelper::StrBeginWithI(path, GetVSInstallDir("110").c_str()))
	{
		return "110";
	}
	else if (StrHelper::StrBeginWithI(path, GetVSInstallDir("120").c_str()))
	{
		return "120";
	}
	else if (StrHelper::StrBeginWithI(path, GetVSInstallDir("140").c_str()))
	{
		return "140";
	}
	else if (StrHelper::StrBeginWithI(path, GetVSInstallDir("150").c_str()))
	{
		return "150";
	}
	else
	{
		FATALERROR("Can't get toolset version from path: %s\n", path.c_str());
		return "";
	}
}

// GetRedistVersionFromPath
//------------------------------------------------------------------------------
/*static*/ std::string VCToolChain::GetRedistVersionFromPath(const std::string &path)
{
	// only useful for vs2017
	std::string installDir17 = GetVSInstallDir("150");
	if (StrHelper::StrBeginWithI(path, installDir17.c_str()))
	{
		std::string start = installDir17 + "\\VC\\Tools\\MSVC\\";
		FileDirHelper::FixupFolderPath(start);
		std::string suffix = path.substr(start.size());
		size_t versionLastIdx = suffix.find_first_of(NATIVE_SLASH);
		std::string redistVer = suffix.substr(0, versionLastIdx);

		// redistVer may not the same as vc toolset version!
		// if not, get biggest version
		std::string redistDir = installDir17 + "\\VC\\Redist\\MSVC\\" + redistVer;
		if (!FileDirHelper::DirectoryExist(redistDir))
		{
			WARNING("Redist version is not the same as vc toolset version: %s !\n", redistVer.c_str());
			redistDir = installDir17 + "\\VC\\Redist\\MSVC\\";
			std::vector<std::string> dirList;
			FileDirHelper::FindFileByExtension(redistDir, dirList, "", true);
			// FIXME: just get the last version
			if (dirList.empty()) 
			{
				FATALERROR("Cant not get redist directory !\n");
				return "";
			}
			INFO("Get redist version: %s\n", dirList.back().c_str());
			return dirList.back();
		}
	}
	return "";
}


// GetRedistVersionFromPath
//------------------------------------------------------------------------------
/*static*/ std::string VCToolChain::GetRedistPathFromBinPath(const std::string &path)
{
	// only useful for vs2017
	std::string installDir17 = GetVSInstallDir("150");
	if (StrHelper::StrBeginWithI(path, installDir17.c_str()))
	{
		std::string start = installDir17 + "\\VC\\Tools\\MSVC\\";
		FileDirHelper::FixupFolderPath(start);
		std::string suffix = path.substr(start.size());
		size_t versionLastIdx = suffix.find_first_of(NATIVE_SLASH);
		std::string redistVer = suffix.substr(0, versionLastIdx);

		// redistVer may not the same as vc toolset version!
		// if not, get biggest version
		std::string redistDir = installDir17 + "\\VC\\Redist\\MSVC\\" + redistVer;
		if (!FileDirHelper::DirectoryExist(redistDir))
		{
			WARNING("Redist version is not the same as vc toolset version: %s !\n", redistVer.c_str());
			redistDir = installDir17 + "\\VC\\Redist\\MSVC\\";
			std::vector<std::string> dirList;
			FileDirHelper::FindFileByExtension(redistDir, dirList, "", true);
			// FIXME: just get the last version
			if (dirList.empty())
			{
				FATALERROR("Cant not get redist directory !\n");
				return "";
			}
			INFO("Get redist version: %s\n", dirList.back().c_str());
			std::string crtDir = dirList.back() + "\\onecore\\x64";
			if (!FileDirHelper::DirectoryExist(redistDir + crtDir))
			{
				crtDir = dirList.back() + "\\x64";
			}

			std::vector<std::string> crtDirList;
			FileDirHelper::FindFileByExtension(redistDir + crtDir, crtDirList, ".CRT", true);
			if (crtDirList.empty())
			{
				FATALERROR("Cant not get redist crt directory !\n");
				return "";
			}
			return crtDir + "\\" + crtDirList.front();
		}
	}
	return "";
}


// GetToolsetVersionFromPaths
//------------------------------------------------------------------------------
/*static*/ std::string VCToolChain::GetToolsetPath(const std::string &paths)
{
	std::vector<std::string> pathVec;
	StrHelper::StrSplit(paths, ';', pathVec, '"');
	for (size_t idx = 0; idx < pathVec.size(); ++idx)
	{
		std::string path;
		FileDirHelper::PathJoin(path, pathVec[idx], "cl.exe");
		if (FileDirHelper::FileExists(path))
		{
			INFO("Found build tool path: %s\n", path.c_str());
			return pathVec[idx];
		}
	}
	FATALERROR("Can't found build tool path\n");
	return "";
}

//------------------------------------------------------------------------------
