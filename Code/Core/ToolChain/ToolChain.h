//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: ToolChain.h
// Contact : hilbertdu@corp.netease.com
// Date: 2016/04/05
//*******************************************************

// Provide toolchain info.
//------------------------------------------------------------------------------
#pragma once
#ifndef TOOL_CHAIN_H
#define TOOL_CHAIN_H

// Includes
//------------------------------------------------------------------------------
#include <string>
#include <map>

// Forward Declarations
//------------------------------------------------------------------------------

// ToolChain
//------------------------------------------------------------------------------
class ToolChain
{
public:
	enum TOOLCHAIN_TYPE
	{
		MSVC,
		GNU,
	};

	enum BUILD_TYPE
	{
		COMPILE				= 0,
		STATIC_LIBRARY		= 1,
		DYNAMIC_LIBRARY		= 2,
	};

	static std::string GetFBCompilerOption(const std::string &compiler);
	static std::string GetFBLibrarianOption(const std::string &librarian);
	static std::string GetFBLinkerOption(const std::string &linker);
};

// VCToolChain
//------------------------------------------------------------------------------
class VCToolChain : public ToolChain
{
public:
	static void Init();

	// To support vs2017, currently we use vswhere tool to find vs install path
	// please set vswhere path before Init.
	static void SetVSWherePath(const std::string &path);

	static std::string GetToolsetPath(const std::string &paths);
	static std::string GetToolsetVersionFromPath(const std::string &path);
	static std::string GetRedistVersionFromPath(const std::string &path);		// for vs 2017
	static std::string GetRedistPathFromBinPath(const std::string &path);		// for vs 2017
	static std::string GetVSInstallDir(const std::string &toolsetVer);
	static std::string GetVSLangVersion(const std::string &toolsetVer);

private:
	static std::map<std::string, std::string> sToolsetPathMap;		// version -> install dir
	static std::string sVSWherePath;
};


// NDKToolChain
//------------------------------------------------------------------------------
class NDKToolChain : public ToolChain
{
public:
	static void Init();

	static std::string GetToolsetVersionFromPath(const std::string &path);
};


//------------------------------------------------------------------------------
#endif // TOOL_CHAIN_H
