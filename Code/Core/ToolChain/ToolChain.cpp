//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: ToolChain.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/04/14
//*******************************************************

// ToolChain.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "ToolChain.h"
#include "../Env/Environment.h"
#include "../Logger/Tracing.h"
#include "../Utils/Utils.h"
#include <vector>

// GetFBCompilerOption
//------------------------------------------------------------------------------
/*static*/ std::string ToolChain::GetFBCompilerOption(const std::string &compiler)
{
	// TODO Get fastbuild compiler option
	return "";
}

// GetFBLibrarianOption
//------------------------------------------------------------------------------
/*static*/ std::string ToolChain::GetFBLibrarianOption(const std::string &librarian)
{
	// TODO Get fastbuild librarian option
	return "";
}

// GetFBCompilerOption
//------------------------------------------------------------------------------
/*static*/ std::string ToolChain::GetFBLinkerOption(const std::string &linker)
{
	// TODO Get fastbuild linker option
	return "";
}


//------------------------------------------------------------------------------
