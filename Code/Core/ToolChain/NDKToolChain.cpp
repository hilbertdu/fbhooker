//*******************************************************
// Copyright (c) 2015/11/01 Netease. All Right Reserved
// Author: hilbertdu
// Filename: NDKToolChain.cpp
// Contact : hilbertdu@corp.netease.com
// Date: 2016/04/23
//*******************************************************

// NDKToolChain.cpp
//------------------------------------------------------------------------------

// Includes
//------------------------------------------------------------------------------
#include "ToolChain.h"
#include "../Logger/Tracing.h"
#include "../Utils/Utils.h"

// Init
//------------------------------------------------------------------------------
/*static*/ void NDKToolChain::Init()
{
}

// GetToolsetVersionFromPath
//------------------------------------------------------------------------------
/*static*/ std::string NDKToolChain::GetToolsetVersionFromPath(const std::string &path)
{
	std::string dir, rootDir;
	FileDirHelper::GetDirFromPath(dir, path);
	if (StrHelper::StrEndWithI(dir, "\\prebuilt\\windows-x86_64\\bin"))
	{
		rootDir = dir.substr(0, dir.size() - 28);
	}
	else
	{
		FATALERROR("Can not found ndk toolchain root dir: %s!\n", path.c_str());
		return "";
	}

	if (StrHelper::StrEndWithI(rootDir, "4.6"))
	{
		return "4.6";
	}
	else if (StrHelper::StrEndWithI(rootDir, "4.8"))
	{
		return "4.8";
	}
	else
	{
		FATALERROR("Unknown NDK toolchain version: %s!\n", dir.c_str());
		return "";
	}
}


//------------------------------------------------------------------------------
