@echo off

set SOLUTION_PATH=..
set CONSOLE_PATH=%SOLUTION_PATH%\Build\Bin\Release\FbuildConsole.exe

set Test_PROJECT_2012="E:\TestProjects\zguide\vs2012\zguide.sln"
set Test_PROJECT_2013="E:\GitProjects\fastbuild\VS2013\FastBuild.sln"
set Test_PROJECT_2015="E:\TestProjects\C++\Test2015\Test2015.sln"

set Test_MESSIAH_2013="E:\HexProjects\Messiah\trunk\src\Messiah.2013.sln"

::%CONSOLE_PATH% -native devenv -sln "%Test_MESSIAH_2013%" -proj "Python.2013" -config "Hybrid|X64" -target rebuild -bkpath "\\SKY-20151102YYY\FbShared"

%CONSOLE_PATH% -native devenv -sln "%Test_PROJECT_2013%" -config "Debug|Win32" -target rebuild -bkpath "\\SKY-20151102YYY\FbShared"

pause & exit