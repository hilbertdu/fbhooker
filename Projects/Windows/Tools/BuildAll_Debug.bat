@echo off

set MS_BUILD_PATH="C:\Program Files (x86)\MSBuild\12.0\Bin\MSBuild.exe"
set SOLUTION_PATH=..
set SOLUTION_NAME=FbuildTools.sln
set OUTPUT_NAME=FbuildHooker.exe
set PUBLISH_PATH=..\..\..\Publish\InstallNew
set TOOLCHAIN_PATH=%PUBLISH_PATH%\Toolchain\FbuildService
set TOOL_CONSOLE_PATH=%PUBLISH_PATH%\Toolchain\FbuildService

if not exist %PUBLISH_PATH% (
	md %PUBLISH_PATH%
)

if not exist %TOOLCHAIN_PATH% (
	md %TOOLCHAIN_PATH%
)

%MS_BUILD_PATH% %SOLUTION_PATH%\%SOLUTION_NAME% /t:Build /p:HookerType=FASTBUILD_HOOKER0;configuration=Debug;platform=Win32 /v:m /nologo
copy %SOLUTION_PATH%\Build\Bin\Debug\%OUTPUT_NAME% %TOOLCHAIN_PATH%\FBHooker0.exe

%MS_BUILD_PATH% %SOLUTION_PATH%\%SOLUTION_NAME% /t:Build /p:HookerType=FASTBUILD_HOOKER1;configuration=Debug;platform=Win32 /v:m /nologo
copy %SOLUTION_PATH%\Build\Bin\Debug\%OUTPUT_NAME% %TOOLCHAIN_PATH%\FBHooker1.exe

%MS_BUILD_PATH% %SOLUTION_PATH%\%SOLUTION_NAME% /t:Build /p:HookerType=FASTBUILD_SERVICE;configuration=Debug;platform=Win32 /v:m /nologo
copy %SOLUTION_PATH%\Build\Bin\Debug\%OUTPUT_NAME% %TOOLCHAIN_PATH%\FBService.exe

%MS_BUILD_PATH% %SOLUTION_PATH%\%SOLUTION_NAME% /t:"FbuildConsole" /p:HookerType=FASTBUILD_SERVICE;configuration=Debug;platform=Win32 /v:m /nologo
copy %SOLUTION_PATH%\Build\Bin\Debug\FbuildConsole.exe %TOOL_CONSOLE_PATH%\FbuildConsole.exe

copy %TOOLCHAIN_PATH%\FBHooker0.exe c:\fastbuild\fbhooker0.exe
copy %TOOLCHAIN_PATH%\FBHooker1.exe c:\fastbuild\fbhooker1.exe
copy %TOOLCHAIN_PATH%\FBService.exe c:\fastbuild\fbservice.exe
copy %TOOL_CONSOLE_PATH%\FbuildConsole.exe c:\fastbuild\bin\FbuildConsole.exe

pause & exit