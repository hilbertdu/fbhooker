#!/bin/bash

cp configure.ac ../../configure.ac

cp Makefile.am ../../Makefile.am
cp Makefile.Core.am ../../Code/Core/Makefile.am
cp Makefile.Hooker.am ../../Code/FbuildHooker/Makefile.am
cp Makefile.Console.am ../../Code/FbuildConsole/Makefile.am

mkdir -p Build
cd ../../
aclocal
autoconf
automake -a
cd -
cd Build
../../../configure
make

