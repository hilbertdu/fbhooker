#FASTBuildVSHooker

This is a tool to make fastbuild hook VS build. Use this tool you don't need to write bff file for each vs project, Just build in VS!

##Install in VS

1.Run Project/Windows/Tools BuildAll_Release.bat

2.Run Publish/InstallNew/install_vsxx.bat corresponding to your vs version

3.Start vs and open any project, run FastBuild/Build at toolbar